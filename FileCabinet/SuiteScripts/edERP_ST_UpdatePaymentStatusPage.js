/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */
/***************************************************************************************  
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                
 **                       
 **@Author      :  Anand Yadav
 **@Dated       :  23 Sep 2019
 **@Version     :  2.0
 **@Description :  To create a suitelet page with update button for updating payment status.
 ***************************************************************************************/

define(['N/ui/serverWidget','N/task'],

function(ui,task) {
   

    function onRequest(context) {
    	
    	if(context.request.method === 'GET'){
    		
    		var form = ui.createForm({
                title: 'Update Payment Status'
            	});
        	
        	form.addSubmitButton({
                label: 'Update'
            });	

            context.response.writePage(form);
    	}else{
    		
    		var scheduledScriptTask = task.create({
                taskType: task.TaskType.SCHEDULED_SCRIPT
            });
            scheduledScriptTask.scriptId = 'customscript_updatepaymentstatus';
            scheduledScriptTask.deploymentId = 'customdeploy_updatepaymentstatus';
            var scheduledScriptTaskId=scheduledScriptTask.submit();
           
            log.debug('scheduledScriptTaskId',scheduledScriptTaskId);
    		var resForm = ui.createForm({
                title: 'Update Payment Status'
            	});
    		
    		var updateResponseField = resForm.addField({
                id: 'update_response',
                type: ui.FieldType.INLINEHTML,
                label:'respone'
               
            });
    		updateResponseField.defaultValue = '<p style="font-size:12pt;font-weight:bold">Payment Status for your account is updated. Please check after few minutes.</p>';
    		
    		context.response.writePage(resForm);
    	}
    		
    }

    return {
        onRequest: onRequest
    };
    
});
