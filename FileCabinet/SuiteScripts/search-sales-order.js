

function scheduledScript(){
   
   var col = new Array();
   col[0] = new nlobjSearchColumn('tranid');
   col[1] = new nlobjSearchColumn('status');
   col[2] = new nlobjSearchColumn('internalid');

   var filter = new Array();
   //This filter prevents multiple output in the result 
   filter[0] = new nlobjSearchFilter('mainline',null,'is',true);
   //filter[0] = new nlobjSearchFilter('type',null,'is','Sales Order');
   //filter[0] = new nlobjSearchFilter('status',null,'is','pendingBilling');

   var searchResults = nlapiSearchRecord( 'salesorder', null, filter , col);

   //Get search result length for debugging
   nlapiLogExecution('DEBUG', 'searchResults.length',searchResults.length);

     for ( var i = 0; searchResults != null && i < searchResults.length; i++)
     {
     		//Locate the field in the status column with a pending billing status	
     		if(searchResults[i].getValue(col[1]) == "pendingBilling"){

	   			var tranID = searchResults[i].getValue(col[0]);
	   			var status = searchResults[i].getValue(col[1]);
	   			var internalId = searchResults[i].getValue(col[2]);
	   															   //(error) need to get the internal ID
	   			var itemfulfill = nlapiTransformRecord('salesorder', internalId , 'invoice');
	   			var fullfilledId = nlapiSubmitRecord(itemfulfill, true);


	   			//Log
	   			nlapiLogExecution('DEBUG', 'tranID = ', tranID);
	   			nlapiLogExecution('DEBUG', 'status = ', status);
	   			nlapiLogExecution('DEBUG', 'itemfulfill = ', itemfulfill);
	   			nlapiLogExecution('DEBUG', 'fullfilledId = ', fullfilledId);
	   			nlapiLogExecution('DEBUG', 'internalId = ', internalId);

   			}
   	} 	
   	
}



