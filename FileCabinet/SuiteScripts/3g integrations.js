/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/https','N/encode'],

function(https,encode) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {


    	var user = 'N3tSu1t3';
        var password = 'woer9Ew$1l012Fk;';
        var token = user + ":" + password;
      	  var base64EncodedString = encode.convert({
            string: token,
            inputEncoding: encode.Encoding.UTF_8,
            outputEncoding: encode.Encoding.BASE_64
        });

		log.debug('hash',base64EncodedString);
        var auth = "Basic " + base64EncodedString;
        var header = {
            'Authorization': auth,
            'System': 'Billing'

        }


        var response = https.get({
            url: 'https://www.sidpayment.com/services/api/v30/billing/start/2018-02-01/end/2018-02-05/skip/0',
            headers: header
        });
    	log.debug('hashes',JSON.stringify(response)); 
		
    }
	

	

    return {
        execute: execute
    };
    
});


