/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */
define(['N/record','N/email','N/log' ],
    function(record, email, log) {
        function afterSubmit(context) {
            if (context.type !== context.UserEventType.CREATE)
                return;

          var record = context.newRecord;
          var type = record.type;
          log.debug('DEBUG','type = ' + type);


          var subTotal = record.getValue({fieldId:'subtotal'});
          var discountItem = record.getValue({fieldId:'discounttotal'});
          var tax = record.getValue({fieldId:'taxtotal'});
          var shippingCost = record.getValue({fieldId:'altshippingcost'});
          var handlingCost = record.getValue({fieldId:'althandlingcost'});
          log.debug('DEBUG','subTotal = ' + subTotal);
          log.debug('DEBUG','discountItem = ' + discountItem);
          log.debug('DEBUG','tax = ' + tax);
          log.debug('DEBUG','shippingCost = ' + shippingCost);
          log.debug('DEBUG','handlingCost = ' + handlingCost);


          var body = "SUBTOTAL: "+subTotal+"\nDISCOUNT ITEM: "+discountItem+"\nTAX: "+tax+"\nSHIPPING COST: "+shippingCost+"\nHANDLING COST: "+handlingCost;

          var senderId = 313;
          var recipient = 312;

            email.send({
                    author: senderId,
                    recipients: recipient,
                    subject: "Order Details",
                    body: body
          });

           log.debug('DEBUG','type = ' + type);

        }

        return {
            afterSubmit: afterSubmit
        };
    });