/**
 *@NApiVersion 2.x
 *@NScriptType ScheduledScript
 */
define(['N/search', 'N/record', 'N/runtime', 'N/log'],
    function(search, record, runtime, log) {
        function execute(context) {

        //create a search object
        var filter = [["mainline", "is", "T"]];
        var searchSalesOrder = search.create({
        type: search.Type.SALES_ORDER,
        columns: ['internalid', 'tranid', 'status'], //columns to display
        filters: filter
        });

        //start-> run a search
        searchSalesOrder.run().each(function(result) {

        //get the internal id of a specific record
        var internalId = result.getValue({
            name: 'internalid'
        });

        //get the value of the field status
        var status = result.getValue({
            name: 'status'
        });

        //choose the data with a pending billing only
        if(status=="pendingBilling"){
        
        //transform the record into invoice
        var transformToInvoice = record.transform({
        fromType: record.Type.SALES_ORDER,
        fromId: internalId,
        toType: record.Type.INVOICE,
        isDynamic: false
        });
        var transformNow = transformToInvoice.save({
             ignoreMandatoryFields: true
        });

        //logs
        log.debug(internalId);
        log.debug(status); 
        log.debug(transformNow);
        }                

        return true;
        });
        //end search <-
    }
        return {
            execute: execute
        };
    });