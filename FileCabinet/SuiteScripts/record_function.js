/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
 
define(['N/record', 'N/file'],

function(record, file) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */

	var recordId = 113;
    function execute(scriptContext) {
    	var arr = new Array();
    	arr.push('ENR00001');
    	var tranId = getSOValue('memo');
    	log.debug('tran id',tranId);
    	
    	var objRecord = record.create({
            type: record.Type.SALES_ORDER,
            isDynamic: true
        });
    	objRecord.setValue({
    		fieldId: 'Memo',
    		value: 'This is a memo'
    	})
    	
    	  /**
    	 * Creates a file
    	 * @param {string} name - file name
    	 * @param {string} fileType - file type
    	 * @param {string} contents - file content
    	 * @returns {Object} file.File
    	 */
    		
    }
 
 function getSOValue(fieldId){
	 var loadSales = record.load({
 		type: 'salesorder',
 		id: recordId
 	});
 	return loadSales.getValue( {fieldId: fieldId} );
 }
 function getSOText(fieldId){
	 var loadSales = record.load({
 		type: 'salesorder',
 		id: recordId
 	});
 	return loadSales.getText( {fieldId: fieldId} );
 }
    return {
        execute: execute
    };
    
});
