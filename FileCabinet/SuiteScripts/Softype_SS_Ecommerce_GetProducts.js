/**
 *@NApiVersion 2.x
 *@NScriptType ScheduledScript
 */
define(['N/https', 'N/http', 'N/record', 'N/search', 'N/log', 'N/runtime', 'N/task'],
    function(https, http, record, search, log, runtime, task) {
        function execute(scriptContext) {
            var headers = new Array();
            headers["Content-Type"] = "application/json";
			headers['Accept'] = 'application/json';
			
        //  url = 'https://softypetestdata.000webhostapp.com/testDataProducts.php';
		//	url = 'http://megasale.18.180.216.54.xip.io/wp-json/wc/v2/products'
		
		   // url = 'https://softypetestdata.000webhostapp.com/products.json';
		   
			var ckey = runtime.getCurrentScript().getParameter('custscript_products_consumer_key');
			var csecret = runtime.getCurrentScript().getParameter('custscript_products_consumer_secret');
			var host_url = runtime.getCurrentScript().getParameter('custscript_products_host_url');
		 
			var stdUrl = 'https://softypetestdata.000webhostapp.com/WooCommerce-REST-API/API/';
			var url = stdUrl + 'GetProducts.php?url=' + host_url + '&ckey=' + ckey + '&csecret=' + csecret;

            var response = https.get({
                url: url,
                headers: headers
            });
            log.debug('response-->', response);

           var dataBody = JSON.parse(response.body);
		   dataBody = dataBody.products;
            log.debug('dataBody', dataBody);

            for (var i = 0; i < dataBody.length; i++) {

                var productId = dataBody[i].id;
                log.audit('productId', productId);

                var date_created = dataBody[i].created_date;
                log.debug('date_created', date_created);

                var name = dataBody[i].title;
                log.debug('name', name);

                var searchId = search.create({
                    type: search.Type.ITEM,
                    columns: [{
                        name: 'internalid'
                    }],
                    filters: [{
                        name: 'custitem_woocom_id',
                        operator: 'is',
                        values: productId
                    }]

                }).run().getRange(0, 999);

                if (searchId.length != 0)
                    continue;

                var price = dataBody[i].price;
                log.debug('price', price);

                var sku = dataBody[i].sku;
                log.debug('sku', sku);

				
                var newRecord = record.create({
                    type: 'inventoryitem',
                    isDynamic: false
                });

				newRecord.setValue({
                    fieldId: "cogsaccount",
                    value: 115
                });
				
				newRecord.setValue({
                    fieldId: "assetaccount",
                    value: 316
                });
				
				// newRecord.setValue({
                    // fieldId: "taxschedule",
                    // value: 1
                // });
				
                newRecord.setValue({
                    fieldId: "itemid",
                    value: name
                });

                newRecord.setValue({
                    fieldId: "subsidiary",
                    value: 1
                });

                newRecord.setValue({
                    fieldId: "custitem_woocom_id",
                    value: productId
                });

                // newRecord.setValue({
                // fieldId: "price",
                // value: price
                // });

                var recordID = newRecord.save();
                log.emergency("record ID", recordID);
            }

        }

        return {
            execute: execute
        };
    });