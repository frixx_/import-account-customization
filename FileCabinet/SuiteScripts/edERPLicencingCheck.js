function edERPLicencingCheck(){
	
	nlapiLogExecution('debug','Library File Called');
	var paymentStatus="";
	
	var column=[];
	column.push(new nlobjSearchColumn('custrecord_payment_status'));
	var searchResult=nlapiSearchRecord('customrecord_payment_status',null,null,column);
	if(searchResult){
		
		var recId=searchResult[0].getId();
		nlapiLogExecution('debug','recId',recId);
		
		paymentStatus=searchResult[0].getValue('custrecord_payment_status');
		nlapiLogExecution('debug','paymentStatus',paymentStatus);
	}
	return paymentStatus;
}
