/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author      : Divya Goswami
 **@Dated       : 1st April 2019
 **@Version     : 2.0
 **@Description : UserEvent Script to display on Check. (Standard Record)
 ***************************************************************************************/
define(['N/ui/serverWidget', 'N/log', 'N/error', 'N/runtime', 'N/record', 'N/url', 'N/search'],
    function(serverWidget, log, error, runtime, record, url, search) {

        function beforeLoad_createButton(context) {
			var currentrec = context.newRecord;
            var recordId = context.newRecord.id;
log.debug('recordId', recordId);

            if (context.type === context.UserEventType.DELETE) return;

            try {
                if (context.type === context.UserEventType.VIEW) {

                    var outputUrl = url.resolveScript({
						scriptId: 'customscript_softype_ns_st_if_print',
						deploymentId: 'customdeploy_softype_ns_st_if_print',
						returnExternalUrl: false
					});

                    outputUrl += '&recordId=' + recordId;
                    var stringScript = "window.open('" + outputUrl + "','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";

                    var printButton = context.form.addButton({ id: 'custpage_print', label: 'Print', functionName: stringScript });
                }
            } catch (e) {
                log.error('Error Details', e.toString());
                throw e;
            }
        }

        return {
            beforeLoad: beforeLoad_createButton
        };
    }
);