/***************************************************************************************
 ** Copyright (c) 1998-2020 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.
 **
 **@Author      :  Divya Goswami
 **@Dated       :  1st April 2019
 **@Version     :  1.x
 **@Description :  Print format for item fullfillment
 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function printReceivingReciept(request, response) {
    var RecId = request.getParameter('recordId');
    nlapiLogExecution('Debug', 'RecId', RecId);

    var loadRec = nlapiLoadRecord('itemfulfillment', RecId);
    var DeliveryId = loadRec.getFieldValue('tranid');
    var receiveDate = loadRec.getFieldValue('trandate');
    var Name = loadRec.getFieldText('entity');
    var SOTO_No = loadRec.getFieldValue('createdfrom');
    var SOTO = nlapiLookupField('transaction', SOTO_No, 'type');
    var to_location = loadRec.getFieldText('transferlocation');
    var createdFromId_Text = loadRec.getFieldText('createdfrom');

    var act_subsidiary = loadRec.getFieldText('subsidiary');
    var recSubs = nlapiLoadRecord('subsidiary', loadRec.getFieldValue('subsidiary'));
    var subs_address = recSubs.getFieldValue('mainaddress_text');

    SOTO_No = nlapiEscapeXML(SOTO_No);

    if (SOTO_No == null)
        SOTO_No = '';

    SOTO = nlapiEscapeXML(SOTO);
    if (SOTO == null)
        SOTO = '';

    DeliveryId = nlapiEscapeXML(DeliveryId);
    if (DeliveryId == null)
        DeliveryId = '';

    receiveDate = nlapiEscapeXML(receiveDate);
    if (receiveDate == null)
        receiveDate = '';

    Name = nlapiEscapeXML(Name);
    if (Name == null)
        Name = '';

    var remarkMemo = nlapiEscapeXML(remarkMemo);
    if (remarkMemo == null)
        remarkMemo = '';

    var form = nlapiCreateForm('From Creation', true); //creating form with title
    var fldHtml = form.addField('custpage_fldhtml', 'inlinehtml', '');

    if (SOTO == 'SalesOrd') {
        var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
        htmlvar += '<pdfset>\n';

        // PDF for collection=========================================
        htmlvar += '<pdf>\n';
        htmlvar += '<head>';
        htmlvar += '<macrolist>';

        htmlvar += '<macro id="nlheader">';
        htmlvar += '<table border="0" class="header" style="width:100%;">';
        htmlvar += '<tr>';
        htmlvar += '<td align="left" style="width:489px;" valign="top"><span style="font-size:14px;"><strong><span class="nameandaddress" style="align:center;">' + act_subsidiary + '</span></strong></span><br /><span style="font-size:10px;"><span class="nameandaddress" style="align:center;">' + subs_address + '</span></span></td>';
        htmlvar += '</tr></table>';
        htmlvar += '</macro>';

        htmlvar += '<macro id="nlfooter">';

        htmlvar += '<table>';
        htmlvar += '<tr>';
        htmlvar += '<td><p><b>Received the Above goods in good order and condition.</b></p></td>';
        htmlvar += '</tr>';

        htmlvar += '<tr>';
        htmlvar += '<td><p>1. This Delivery Receipt, if signed will serve as confirmation that the above goods have been received in good order and condition. I acknowledge that the above items are complete and further certify that this is not a sales Invoice which is issued Seperately. </p></td>';
        htmlvar += '</tr>';
        htmlvar += '<tr >';
        htmlvar += '<td><p>2. If not paid with the payment terms approved, will carry interest at the rate of 3% per month until settled. </p></td>';
        htmlvar += '</tr>';
        htmlvar += '</table>';
        htmlvar += '</macro>';
        htmlvar += '</macrolist>';
        htmlvar += '</head>';
        htmlvar += '<body header="nlheader" header-height="20pt" footer="nlfooter" footer-height="50pt" padding="1cm" font-size="12px" size="8.5in 11in">';

        htmlvar += '<table border="0" cellpadding="1" cellspacing="1" style="width: 671px;padding-top: 30px;"><tr>';
        htmlvar += '<td><p align="center" font-size="14"><b><center>DELIVERY REPORT</center></b></p>';
        htmlvar += '</td></tr></table>';

        htmlvar += '<table style="height:auto;width:100%; border-collapse:collapse;table-layout:fixed;margin-top: 10px;"><tr style="border:1px solid black;border-left:none;border-right:none">';
        htmlvar += '<td><b>Delivery ID#:</b></td>';
        htmlvar += '<td>' + DeliveryId + '</td>';
        htmlvar += '<td><b>Date Fullfilled:</b></td>';
        htmlvar += '<td>' + receiveDate + '</td>';
        htmlvar += '</tr>';
        htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;border-top:none">';
        htmlvar += '<td><b>SO #:</b></td>';
        htmlvar += '<td>' + createdFromId_Text + '</td>';
        htmlvar += '<td><b>Customer:</b></td>';
        htmlvar += '<td><p style="align:left;">' + Name + '</p></td>';
        htmlvar += '</tr>';
        htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;border-top:none">';
        htmlvar += '<td><b>Remarks:</b></td>';
        htmlvar += '<td>' + remarkMemo + '</td>';
        htmlvar += '</tr>';

        htmlvar += '<tr style="border:0px solid black;border-left:none;border-right:none;border-top:none">';
        htmlvar += '<td></td>';
        htmlvar += '</tr>';

        htmlvar += '</table>';
        htmlvar += '<table style="height:auto;width:100%; border-collapse:collapse;table-layout:fixed;padding-top:1cm">';

        var id = loadRec.getLineItemValue('item', 'item', 1);
        htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;border-bottom:1px">';

        htmlvar += '<td style="align:center;width:10%;"><b>Item ID</b></td>';
        htmlvar += '<td style="align:left;width:68%;padding-left:5px"><b>Item Description</b></td>';

        htmlvar += '<td style="align:left;width:11%;"><b>UOM</b></td>';
        htmlvar += '<td style="align:left;width:11%;"><b>Fullfilled Qty</b></td>';

        htmlvar += '</tr>';

        var lineCount = loadRec.getLineItemCount('item'); //get the line item count

        var tot_qty = 0;
        for (var i = 1; i <= lineCount; i++) {
            tot_qty += parseFloat(loadRec.getLineItemValue('item', 'quantity', i)); //NDEV

            var Quantity = loadRec.getLineItemValue('item', 'quantity', i);
            var Uoms = loadRec.getLineItemValue('item', 'unitsdisplay', i);
            var ItemCode = loadRec.getLineItemText('item', 'item', i);

            var Description = loadRec.getLineItemValue('item', 'description', i);
            var Location = loadRec.getLineItemText('item', 'location', i);
            var subrecord = loadRec.viewLineItemSubrecord('item', 'inventorydetail', i);
            nlapiLogExecution('DEBUG', 'subrecord===>', subrecord);

            if (subrecord) {
                htmlvar += '<tr style="border-bottom:1px solid black">';
                htmlvar += '<td style="align:center;">' + nlapiEscapeXML(ItemCode) + '</td>';
                htmlvar += '<td> <p style="align:left;padding-left:5px">' + nlapiEscapeXML(Description) + '</p></td>';


                htmlvar += '<td> <p style="align:left;padding:0px 0px 0px 0px;">' + nlapiEscapeXML(Uoms) + '</p></td>';
                htmlvar += '<td> <p style="align:center;">' + nlapiEscapeXML(Quantity) + '</p></td>';
            } else {
                htmlvar += '<tr style="border-bottom:1px solid black">';
                htmlvar += '<td style="align:center;">' + nlapiEscapeXML(ItemCode) + '</td>';

                htmlvar += '<td> <p style="align:left;padding-left:5px">' + nlapiEscapeXML(Description) + '</p></td>';

                htmlvar += '<td> <p style="align:left;padding:0px 0px 0px 0px">' + nlapiEscapeXML(Uoms) + '</p></td>';
                htmlvar += '<td> <p style="align:center;">' + nlapiEscapeXML(Quantity) + '</p></td>';
            }

            htmlvar += '</tr>';
        }

        //b: NDEV
        htmlvar += '<tr>';

        htmlvar += '<td colspan="4" style="padding-top:5px;align:right"><b>Total Quantity: ' + tot_qty + '</b></td>';
        htmlvar += '<td>&nbsp;</td>';
        htmlvar += '</tr>';
        //e:NDEV

        htmlvar += '</table>';
nlapiLogExecution('DEBUG', 'Vishal', 'Vishal');
        htmlvar += '<table style="padding-top:10px;border:0px solid black;width:100%;page-break-inside:avoid">';
        htmlvar += '<tr>';
        htmlvar += '<td><b>Delivered By:</b></td>';
        htmlvar += '<td> </td>';
        htmlvar += '</tr>';

        htmlvar += "  <tr style='padding-top:10px'>";
        htmlvar += "  <td><b>Delivery Date:</b></td>";
        htmlvar += '  <td> </td>';
        htmlvar += "  <td></td>";
        htmlvar += "  </tr>";
        htmlvar += "  <tr style='padding-top:10px'>";
        htmlvar += "  <td ><b>Prepared By:</b></td>";
        htmlvar += "  <td ><b>Checked By:</b> </td>";
        htmlvar += "  <td > <b>Approved By:</b></td>";
        htmlvar += "  <td > <b>Received By:</b></td>";
        htmlvar += "  </tr>";
nlapiLogExecution('DEBUG', 'Vishal2', 'Vishal2');
        htmlvar += '</table>';
        htmlvar += '</body>\n</pdf>';

        //Pdf for Logistics===================================================

        //Pdf for IF-1===================================================
        htmlvar += '</pdfset>';
        var file = nlapiXMLToPDF(htmlvar);
        response.setContentType('PDF', 'Print.pdf', 'inline');
        response.write(file.getValue());
    }

    if (SOTO == 'TrnfrOrd') {
        var recLocation = nlapiLoadRecord('location', loadRec.getFieldValue('transferlocation'));
        var to_location_addr = recLocation.getFieldValue('mainaddress_text');
        to_location_addr = nlapiEscapeXML(to_location_addr);

        var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';

        htmlvar += '<pdfset>'

        htmlvar += '<pdf>\n';
        htmlvar += '<head>';
        htmlvar += '<macrolist>';
        htmlvar += '<macro id="nlheader">';
        htmlvar += '<table class="header" style="width: 100%;">';
        htmlvar += '<tr>';
        htmlvar += '<td align="left" style="width: 489px;" valign="top"><span style="font-size:14px;"><strong><span class="nameandaddress" style="align:center;">' + act_subsidiary + '</span></strong></span><br /><span style="font-size:10px;"><span class="nameandaddress" style="align:center;">' + subs_address + '</span></span></td>';
        htmlvar += '</tr></table>';
        htmlvar += '</macro>';

        htmlvar += '<macro id="nlfooter">';

        htmlvar += " </macro>";
        htmlvar += " </macrolist>";
        htmlvar += '</head>';
        htmlvar += '<body header="nlheader" header-height="3%" footer="nlfooter" footer-height="1%" padding="0.5in 0.5in 0.5in 0.5in" font-size="12px" size="A4">';

        htmlvar += '<table border="0" cellpadding="1" cellspacing="1" style="width: 671px;padding-top: 30px;"><tr>';
        htmlvar += '<td><p align="center" font-size="14"><b><center>DELIVERY REPORT</center></b></p>';
        htmlvar += '</td></tr></table>';
        htmlvar += '<table style="height:auto;width:100%;border-collapse:collapse; table-layout:fixed;margin-top: 10px;"><tr style="border:1px solid black;border-left:none;border-right:none">';
        htmlvar += '<td><b>To Location:</b></td>';
        htmlvar += '<td>' + to_location + '</td>';
        htmlvar += '<td><b>Date:</b></td>';
        htmlvar += '<td>' + receiveDate + '</td>';
        htmlvar += "</tr>";
        htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;border-top:none">';
        htmlvar += '<td><b>Order</b></td>';
        htmlvar += '<td>' + createdFromId_Text + ' </td>';
        htmlvar += '<td><b>Memo:</b></td>';
        htmlvar += '<td> </td>';
        htmlvar += "  </tr>";
        htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;border-top:none">';
        htmlvar += '<td><b>Document No#:</b></td>';
        htmlvar += '<td>' + DeliveryId + '</td>';
        htmlvar += "  </tr>";

        if (false) {
            htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;border-top:none">';
            htmlvar += '<td style="border: 0px #fab; width:300px; word-wrap:break-word;"><b>Consignment Location:</b></td>';
            htmlvar += '<td style="border: 0px #fab; width:300px; word-wrap:break-word;"><b>Consignment Address:</b></td>';
            htmlvar += '<td style="border: 0px #fab; width:300px; word-wrap:break-word;"><p style="align:left;">' + nlapiEscapeXML(to_location_addr) + '</p></td>';
            htmlvar += "  </tr>";
        } else {
            htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;border-top:none">';
            htmlvar += '<td style="border: 0px #fab; width:300px; word-wrap:break-word;"><b>Location Address:</b></td>';
            htmlvar += '<td style="border: 0px #fab; width:300px; word-wrap:break-word;"><p style="align:left;">' + nlapiEscapeXML(to_location_addr) + '</p></td>';
            htmlvar += "  </tr>";
        }
        htmlvar += '</table><br/>';
        htmlvar += '<table style="width: 100%;padding-top:4px;height:auto;width:100%; border-collapse:collapse;table-layout:fixed;">'
        htmlvar += '<tr style="border:1px solid black;border-left:none;border-right:none;">';

        htmlvar += '<td style="align:center;width:10%"><b>Item ID</b></td>';
        htmlvar += '<td style="padding-left:20px;width:68%"><b>Item Description</b></td>';

        htmlvar += '<td style="padding-left:20px;width:11%"><b>UOM</b></td>';
        htmlvar += '<td style="width:11%;"><p style="align:left"><b>Transfered Qty</b></p></td>';
        htmlvar += "  </tr>";

        var tot_qty = 0;
        var lineCount = loadRec.getLineItemCount('item'); //get the line item count
        for (var i = 1; i <= lineCount; i++) {
            var Quantity = loadRec.getLineItemValue('item', 'quantity', i);
            tot_qty += parseInt(Quantity);
            var Uoms = loadRec.getLineItemValue('item', 'unitsdisplay', i);
            var ItemCode = loadRec.getLineItemText('item', 'item', i);
            var Description = loadRec.getLineItemValue('item', 'description', i);
            var Location = loadRec.getLineItemText('item', 'location', i);
            var subrecord = loadRec.viewLineItemSubrecord('item', 'inventorydetail', i);

            nlapiLogExecution('DEBUG', 'subrecord===>', subrecord);

            if (subrecord) {
                htmlvar += '<tr style="border-bottom:1px solid black">';

                htmlvar += '<td> <p style="align:center;">' + nlapiEscapeXML(ItemCode) + '</p></td>';
                htmlvar += '<td> <p style="align:left;padding-left:20px">' + nlapiEscapeXML(Description) + '</p></td>';

                htmlvar += '<td  style="padding-left:20px"> <p style="align:left;">' + nlapiEscapeXML(Uoms) + '</p></td>';
                htmlvar += '<td> <p style="align:center;">' + nlapiEscapeXML(Quantity) + '</p></td>';
            } else {
                htmlvar += '<tr style="border-bottom:1px solid black">';

                htmlvar += '<td> <p style="align:center;">' + nlapiEscapeXML(ItemCode) + '</p></td>';
                htmlvar += '<td> <p style="align:left;padding-left:20px">' + nlapiEscapeXML(Description) + '</p></td>';

                htmlvar += '<td style="padding-left:20px"> <p style="align:left;">' + nlapiEscapeXML(Uoms) + '</p></td>';
                htmlvar += '<td> <p style="align:center;">' + nlapiEscapeXML(Quantity) + '</p></td>';
            }
            htmlvar += "  </tr>";
        }

        //b: NDEV
        htmlvar += '<tr>';
        htmlvar += '<td colspan="4" style="padding-top:5px;align:right"><b>Total Quantity: ' + tot_qty + '</b></td>';
        htmlvar += '<td>&nbsp;</td>';
        htmlvar += "  </tr>";
        //e:NDEV

        htmlvar += '</table>';

        htmlvar += '<table style="padding-top:10px;width:100%">';
        htmlvar += "  <tr>";
        htmlvar += "  <td><b>Transfered by:</b></td>";
        htmlvar += '<td> </td>';
        htmlvar += "  </tr>";

        htmlvar += "  <tr style='padding-top:10px'>";
        htmlvar += "  <td><b>Transfer Date:</b></td>";
        htmlvar += '  <td> </td>';
        htmlvar += "  <td></td>";
        htmlvar += "  </tr>";
        htmlvar += "  <tr style='padding-top:10px'>";
        htmlvar += "  <td><b>Prepared By:</b></td>";
        htmlvar += "  <td><b>Checked By:</b> </td>";
        htmlvar += "  <td> <b>Approved By:</b></td>";
        htmlvar += "  <td> <b>Received By:</b></td>";
        htmlvar += "  </tr>";
        htmlvar += '</table>';

        htmlvar += '</body>\n</pdf>';
        /*END IF 1*/

        htmlvar += '</pdfset>'
        var file = nlapiXMLToPDF(htmlvar);
        response.setContentType('PDF', 'Print.pdf', 'inline');
        response.write(file.getValue());
    }
}