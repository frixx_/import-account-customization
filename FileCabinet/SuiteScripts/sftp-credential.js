/**
*@NApiVersion 2.x
*@NScriptType Suitelet
*/
define(['N/log','N/ui/serverWidget','N/runtime'], function(log, ui, runtime) {
    function onRequest(context) {

if(context.request.method === 'GET'){
    var form = ui.createForm({title: 'Enter SFTP Credentials'});
    var credField = form.addCredentialField({
        id: 'custfield_sftp_password_token',
        label: 'SFTP Password',
        restrictToScriptIds: [runtime.getCurrentScript().id,'customscript_sftp_test'],
        restrictToDomains: ['127.0.0.1'],
        //restrictToCurrentUser: false
    });
    credField.maxLength = 64;
    form.addSubmitButton();
    context.response.writePage(form);
}

var request = context.request;
if(context.request.method === 'POST'){
    // Read the request parameter matching the field ID we specified in the form
    var passwordToken = request.parameters.custfield_sftp_password_token;
    log.debug({
        title: 'New password token', 
        details: passwordToken
    });
  
    // In a real-world script, "passwordToken" is saved into a custom field here...
}
}
return {
    onRequest: onRequest
};

});