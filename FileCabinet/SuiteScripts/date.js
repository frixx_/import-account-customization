function onSave(){
	if( confirm("Please review all fields. Are you sure you want to submit?")){
        return true;
	}else{
		return false;
	}

}
function validateField(type, name){
 
 	//Initialize system date
	var todaysDate = new Date();
	var tDay = todaysDate.getDate();
	var tMonth = todaysDate.getMonth()+1;

	//get the value of end date field
	var endDate = nlapiGetFieldValue('enddate');
	endDate.toString();

	//get month and day by string
	var eDay = endDate.charAt(3) + endDate.charAt(4);
	var eMonth = endDate.charAt(0) + endDate.charAt(1);

	//add the day with 30
	todaysDate.setDate(todaysDate.getDate()+30);

	//New date with added days +30
	var nDay = todaysDate.getDate();
	var nMonth = todaysDate.getMonth()+1;

	//Display values for sample
	//alert("end day:"+eDay+"\nend month:"+eMonth+"\nnew day:"+nDay+"\nnew month:"+nMonth);

	if(name==='enddate'){
    //compare input end date with new date which is +30 days

	if(eDay!=nDay||eMonth!=nMonth){
        alert("Should be 30 days from today"); 
        return false;
	}
}  
return true;

}