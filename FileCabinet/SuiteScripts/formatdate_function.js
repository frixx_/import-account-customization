/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/config'],

function(format, config) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *u
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	
    	var todaysDate = new Date();
    	var formattedDate = formatDate(todaysDate);
    	
    	log.debug('formattedDate',formattedDate);
    }
    
   function formatDate(todaysDate){
	   //format date to netsuite settings date
 	   var actualDate = format.format({
            value: todaysDate,
            type: format.Type.DATETIME,
            timezone: format.Timezone.timezone
        });
 	   //log.debug('system date',actualDate);
 	   
 	   //convert date to javascript format date
 	   actualDate = new Date(actualDate);
 	   
 	   //get day
 	   var day = actualDate.getDate();
 	   //get month
 	   var month = actualDate.getMonth() + 1;
 	   //get year
        var year = actualDate.getFullYear();
        
        var hour = actualDate.getHours();
        var minute = actualDate.getMinutes();
        var second = actualDate.getSeconds();
        //log.debug('hour', hour);
        
        //add zero if less than 10 function
        function addZero(n) {
            return (n < 10) ? '0' + n : n;
       }
        
        //identify if AM or PM
        var m = (addZero(hour) > 11) ? 'PM' : 'AM'; 
        
        //convert hour into 12 hour format
        hour = (actualDate.getHours() + 24) % 12 || 12;
        
        
        
 	   var formattedDate = "D"+ addZero(day) + addZero(month) + year +"_T"+ addZero(hour) +""+ addZero(minute) +""+ addZero(second) + m;
 	   return formattedDate;
   }
   
   
   
    return {
        execute: execute
    };
    
});
