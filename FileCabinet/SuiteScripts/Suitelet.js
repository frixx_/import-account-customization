/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/xml', 'N/render', 'N/http', 'N/ui/serverWidget', 'N/record', 'N/search'],

function(xml, render, http, ui, record, search) {
   
   
    /*function onRequest(context) {   
    	 var html = `<!DOCTYPE html>
        	 <head>
        		 <meta charset="UTF-8"/>
        		 <title>Demo</title>
        		 <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="crossorigin="anonymous"></script>
        		 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        		
        	 </head>
        		 <body>
        		 	 <div id="error"></div>
        		 	 <div id="credentials"></div>
						<form id="form" action="/" method="GET">
						  <div>
						    <label for="name">Name</label>
						    <input id="name" name="name" type="text" required>
						  </div>
						  <div>
						    <label for="password">Password</label>
						    <input id="password" name="password" type="password">
						  </div>
						  <button type="submit">Submit</button>
						</form>
        		 	 <h1>form 2!<h1> 
			       <form>
			       <fieldset>
			          <legend>Selecting elements</legend>
			          <p>
			             <label>Select list</label>
			             <select id = "myList">
			               <option value = "1">one</option>
			               <option value = "2">two</option>
			               <option value = "3">three</option>
			               <option value = "4">four</option>
			             </select>
			          </p>
			       </fieldset>
			    </form>
        		 	 <script>
        		 	 	const name = document.getElementById('name')
			const password = document.getElementById('password')
			const form = document.getElementById('form')
			const errorElement = document.getElementById('error')
			const creds = document.getElementById('credentials')
			
			form.addEventListener('submit', (e) => {
			  let messages = []
			  if (name.value === '' || name.value == null) {
			    messages.push('Name is required')
			  }
			
			  if (password.value.length <= 6) {
			    messages.push('Password must be longer than 6 characters')
			  }
			
			  if (password.value.length >= 20) {
			    messages.push('Password must be less than 20 characters')
			  }
			
			  if (password.value === 'password') {
			    messages.push('Password cannot be password')
			  }
			
			  if (messages.length > 0) {
			    e.preventDefault()
			    errorElement.innerText = messages.join(', ')
			  }
			  e.preventDefault()
			  errorElement.innerText = password.value;
			})
        		 	 </script>
        		 </body>
        	 `;
        	 context.response.write({ output: html });
    }
    */
	
	/*//======================WORKING SAMPLE===================================
	function onRequest(context) {   
		  var method = context.request.method;
	       var something = "";
	       if (method == "POST") {
	              something = context.request.parameters["something"];
	              log.debug('something', something);
	       }
	       
	       var html = `<html>
	    	   		   		<body>something: ${something}<br/>
	                     		<form method="post">
	                     			Input  <input type="text" name="something" id="something" value=""/> 
	                     			<input type="submit"/> 
	                     		</form>
	                     	</body>
	                   </html>`;
	       
	       context.response.write(html);
	}
	//========================================================================*/
	
	function onRequest(context) {   
		 var method = context.request.method;
	       let yourname = "";
	       
	       if (method == "POST") {
	    	   yourname = context.request.parameters["yourname"];
	                log.debug('context.request', context.request.parameters);
	                log.debug('yourname', yourname);
	           
	           
	           let pendingOrder = record.create({
	               type: 'customrecord_pending_orders',                     
	           });
	           
	           for(let i=1;i<6;i++){
	        	   let foodId = "food"+i;
	        	   let food = context.request.parameters[foodId];
	        	   let name = context.request.parameters["yourname"];
	        	   if(food!==""){	
	        		   		pendingOrder.setValue({
	         		            fieldId: 'custrecord_pending_orderer_name',
	         		            value: name
	         		    	  });
	        		   		pendingOrder.setValue({
	         		            fieldId: 'custrecord_pending_orderer_food',
	         		            value: food
	         		    	  });
	        	   			let saveId = pendingOrder.save({
	            		        enableSourcing: false,  
	            		        ignoreMandatoryFields: true
	            		    });
	        	   		
	        	   }
	        	 
	           }
	           
	       }
	       
	       	   let foodsArray = new Array();
	    	   const makeSearch = search.load({
	               id: 'customsearch827'
	           });
	    	   const result = makeSearch.run().getRange(0, 100);
	      	   const resultLength = result.length;
	      	   for(let i=0; i<resultLength; i++){
	      		 let food = result[i].getValue('custrecord_food_name');
	      		 let price = result[i].getValue('custrecord_price');
	      		 //log.debug('food', food);
	      		 foodsArray.push(food);
	      	   }
	      	   log.debug('foodsArray',foodsArray );
	      	
	       
	       
	       let backgroundImage = `https://tstdrv1393316.app.netsuite.com/core/media/media.nl?id=5328&c=TSTDRV1393316&h=3cbf6158d6ebbb56a467&fcts=20200325222632&whence=`;
	       var html = `<!DOCTYPE html>
	    	  	<html>
	        	 	<head>
	        		 <meta charset="UTF-8"/>
	        		 <title>Demo</title>
	        		 <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="crossorigin="anonymous"></script>
	        		 
		        		
		        		 <style>
	    	   				html
	    	   					{
								  background: url("${backgroundImage}") no-repeat center center fixed; 
								  -webkit-background-size: cover;
								  -moz-background-size: cover;
								  -o-background-size: cover;
								  background-size: cover;
	    	   					 }
	    	   				body
	    	   					{
	    	   					  color: white;
	    	   					}
	    	   				.container{
	    	   						
	    	   					}
	    	   				input 
	    	   					 {
	    	   					  background-color: yellow;
	    	   					 }
	    	   				.orderTable{
	    	   					
	    	   				}
	    	   				table.orderTable td{
	    	   					font-size: 20px;
	    	   				}
	    	   				.qty{
	    	   					background-color: white;
	    	   					width: 20px;
	    	   				}
	    	   				.food{
	    	   					width: 400px;
	    	   				}
	    	   				.remove{
	    	   					background-color: red;
	    	   				}
	    	   				.placeOrder{
	    	   					background-color: #33ff33;
	    	   				}
		        		 </style>
	    	   		</head>
	        	 		<body background-image="${backgroundImage}">
	        	 			<div class="container" align="center">
	        	 				<div class="header">
	        	 					<h3>Softype Food Ordering</h3>
	        	 				</div>
		        	 			<form onSubmit="return validateForm(this)" method="post">
			        	 			Name <input type="text" name="yourname" id="yourname" value=""/>
			        	 			<br/><br/>
			        	 			Menu <select name="selectFood" id="selectFood">
									  `;
	       								for(let i=0; i<resultLength; i++)
	       									{
	       									let food = foodsArray[i];
	       									html+=`
	    									    <option value="${food}">${food}</option>
	    									    `;
	       									}
									    
									html+=`</select>
								    <select name="selectQty" id="selectQty">
									    <option value="1" selected="selected">1</option>
									    <option value="2">2</option>
									    <option value="3">3</option>
									    <option value="4">4</option>
								    </select><br/><br/>
								    <input class="placeOrder" type="button" id="placeOrderBtn" value="Place Order" onClick="addOrder()"/><br/><br/>
								    <table class="orderTable" id="orderedTable">
								    	<tr>
								    		<td>Order</td>
								    		<td>Qty</td>
								    		<td></td>
								    	</tr>
								    	<tr>
								    		<td><input class="food" type="text" name="food1" id="food1" value=""/  readonly="readonly"></td>
								    		<td><input class="qty" type="text" name="qty1" id="qty1" value=""/  readonly="readonly"></td>
								    		<td><input class="remove" type="button" id="remove1" value="x" onclick="removeRow(id)"></td>
								    	</tr>
								    	<tr>
								    		<td><input class="food" type="text" name="food2" id="food2" value=""  readonly="readonly"/></td>
								    		<td><input class="qty" type="text" name="qty2" id="qty2" value=""  readonly="readonly"/></td>
								    		<td><input class="remove" type="button" id="remove2"  value="x" onclick="removeRow(id)"></td>
								    		
								    	</tr>
								    	<tr>
								    		<td><input class="food" type="text" name="food3" id="food3" value=""  readonly="readonly"/></td>
								    		<td><input class="qty" type="text" name="qty3" id="qty3" value=""  readonly="readonly"/></td>
								    		<td><input class="remove" type="button" id="remove3" value="x" onclick="removeRow(id)"></td>
								    	</tr>
								    	<tr>
								    		<td><input class="food" type="text" name="food4" id="food4" value=""  readonly="readonly"/></td>
								    		<td><input class="qty" type="text" name="qty4" id="qty4" value=""  readonly="readonly"/></td>
								    		<td><input class="remove" type="button" id="remove4" value="x" onclick="removeRow(id)"></td>
								    	</tr>
								    	<tr>
								    		<td><input class="food" type="text" name="food5" id="food5" value=""  readonly="readonly"/></td>
								    		<td><input class="qty" type="text" name="qty5" id="qty5" value=""  readonly="readonly"/></td>
								    		<td><input class="remove" type="button" id="remove5" value="x" onclick="removeRow(id)"></td>
								    	</tr>
								    	<tr>
								    		<td><input class="food" type="text" name="food6" id="food6" value=""  readonly="readonly"/></td>
								    		<td><input class="qty" type="text" name="qty6" id="qty6" value=""  readonly="readonly"/></td>
								    		<td><input class="remove" type="button" id="remove6" value="x" onclick="removeRow(id)"></td>
								    	</tr>
								    </table>
								    Your name is: ${yourname}<br/><br/>
								   
								    <script>
								   
								    function addOrder(){
								    	const selectedFood = document.getElementById("selectFood").value;
								    	const selectedQty = document.getElementById("selectQty").value;
								    	console.log(selectedFood);
								    	console.log(selectedQty);
								    	for(let i=1; i<=6; i++){
									    	let foodId = "food"+i;
									    	let qtyId = "qty"+i;
									    	const food = document.getElementById(foodId).value;
									    	const qty = document.getElementById(foodId).value;
									    	if(food==="" && qty===""){
									    	document.getElementById(foodId).value = selectedFood;
									    	document.getElementById(qtyId).value = selectedQty;
									    	break;
								    		}
								    	}
								    }
								    function removeRow(id) {
									        for(let i=1; i<=6; i++)
									        {
									        let foodId = "food"+i;
									        let qtyId = "qty"+i;
									        let removeId = "remove"+i;
									  			switch(id)
									  			{
									  				case removeId:
									  				document.getElementById(foodId).value = "";
									    			document.getElementById(qtyId).value = "";
									  				break;
									  			}
									  		}
									    }
									function validateForm(form){
											let valid = true;
											
									         if(document.getElementById("food1").value === ""&&
									         document.getElementById("food2").value === ""&&
									         document.getElementById("food3").value === ""&&
									         document.getElementById("food4").value === ""&&
									         document.getElementById("food5").value === ""&&
									         document.getElementById("food6").value === ""){
									         	alert("Please make atleast one order")
									         valid=false;
									         }
									  		if(document.getElementById("yourname").value === ""){
									  			alert("Please enter your name");
									  			valid = false;
									  		}	
											 if(!valid) {
										        return false;
										    }
										    else {
										        return confirm('Are you sure you want to submit?');
										    }
									}
								    </script>
								    <input type="submit" value="Submit"/>
								    
								     <div>
										<h4>Current Orders</h4?
										<table>
											<tr>
												<td>Orderer Name</td>
												<td>Food</td>
												<td>Price</td>
												<td>Total</td>
											<tr>
											<tr>
												<td>Ren</td>
												<td>Paksiw</td>
												<td>35</td>
												<td>35</td>
											<tr>
											<tr>
												<td> </td>
												<td>linat an</td>
												<td>30</td>
												<td>30</td>
											<tr>
											<tr>
												<td> </td>
												<td>bihon</td>
												<td>15</td>
												<td>15</td>
											<tr>
										</table>
								    </div>
		        	 			</form>
	        	 			</div>
	        	 		</body>
	        	</html>`;
	       
	       context.response.write(html);
	}
	
    return {
        onRequest: onRequest
    };
    
});
