/**
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/search','N/record'],

function(search, record) {
  	 //sample comment
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {
    	
    	let arr=[111,222,333,444];
    	//for(let i=0; i<10;i++){
    		//arr.push(i);
    	//}
    	return arr;
    	
    }

    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */
    function map(context) {
    	log.debug('map', context);
    	
    	//var searchResult = JSON.parse(context.value);
        //var invoiceId = searchResult.id;
        //var entityId = searchResult.values.entity.value;
        
        //log.debug('searchResult', searchResult);
        //log.debug('invoiceId', invoiceId);
        //log.debug('invoiceId', invoiceId);
        
       
        	log.debug('context', context.value);
        	 context.write({
                 key: context.value,
                value: 12345
            });
        
    }

    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
     * @since 2015.1
     */
    function reduce(context) {
    	 context.write({
             key: context.value,
            value: 12345
        });
     }
   


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
     * @since 2015.1
     */
    function summarize(summary) {
    	let mapRecords=0;
    	let reduceRecords=0;
    	let mapValues =[];
    	//Summary of processed records during the Map stage:

    	summary.mapSummary.keys.iterator().each(function (key, value) {

    		mapRecords += 1;
    		mapValues.push(value);
    		return true;

    	});
    	
    	
    	//Summary of processed records during the Reduce stage:
    	
    	summary.reduceSummary.keys.iterator().each(function (key) {

    		reduceRecords += 1;

    		return true;

    	});
    	
    	//Log all the processed records:
    	log.debug('mapRecords',mapRecords);
    	log.debug('mapValues', mapValues);
    	log.debug('reduceRecords',reduceRecords);
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
