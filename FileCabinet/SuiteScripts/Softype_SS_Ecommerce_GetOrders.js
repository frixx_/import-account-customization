/**
	*@NApiVersion 2.x
	*@NScriptType ScheduledScript
*/
/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Latika 
	**@Dated : 24 March 2020
	**@Version : 2.0
	**@ScriptID : 652
	**@Description : Call WooCommerce API to get the Sales Order
***************************************************************************************/
define(['N/https', 'N/record', 'N/search', 'N/log', 'N/format', 'N/runtime', 'N/task', 'N/url', 'N/error', 'N/email', 'N/render'],
	function (https, record, search, log, format, runtime, task, url, error, email, render) {
		function execute(scriptContext) {
			// var timeStamp = +new Date()
			try {
				var headers = new Array();
				headers["Content-Type"] = "application/json";
				headers["Authorization"] = "Basic dXNlcjpMdW9mdjIyNWdPMks=";

				var website = runtime.getCurrentScript().getParameter('custscript_w00_website');
				var GetOrdersAPI = runtime.getCurrentScript().getParameter('custscript_get_orders_api_v3');
				var LOCATION = runtime.getCurrentScript().getParameter('custscript_getorders_location');
				//var customform = runtime.getCurrentScript().getParameter('custscript_customform_nbyci_get_orders');
				//var subsidiaryStandalone = runtime.getCurrentScript().getParameter('custscript_subsi_standalone_getorders');
				var defaultVat = runtime.getCurrentScript().getParameter('custscript_default_vat_get_orders');

				var websiteDetails = (search.lookupFields({
					type: "customrecord_woocommerce_websites",
					id: website,
					columns: [
						'custrecord_woocommerce_url',
						'custrecord_woocommerce_ck',
						'custrecord_woocommerce_cs',
						'custrecord_woocommerce_logo',
						'custrecord_woocommerce_logo_2',
						'custrecord_display_uom',
						'custrecord_orders_link',
						'custrecord_customer_vat_check',
						'custrecord_primary_status_so',
						'custrecord_set_shipping_address'
					]
				}));
				var middleware_app = runtime.getCurrentScript().getParameter('custscript_middleware_app');

				var host = websiteDetails.custrecord_woocommerce_url;
				var cKey = websiteDetails.custrecord_woocommerce_ck;
				var cSecret = websiteDetails.custrecord_woocommerce_cs;
				var website_logo = websiteDetails.custrecord_woocommerce_logo;
				var website_logo_2 = websiteDetails.custrecord_woocommerce_logo_2;
				var displayUOMonPrint = websiteDetails.custrecord_display_uom;
				var webstoreOrderLink = websiteDetails.custrecord_orders_link;
				var customerVatCheck = websiteDetails.custrecord_customer_vat_check;
				var primarySOStatus = websiteDetails.custrecord_primary_status_so;
				var setShippingAddress = websiteDetails.custrecord_set_shipping_address;
				//var defaultDivision = websiteDetails.custrecord_default_division_webstore[0].value;

				var afterDate = new Date();
				afterDate.setDate(afterDate.getDate() - 10);

				var format_after_date = afterDate.toISOString();
				log.debug('format_after_date', format_after_date);

				//	var stdUrl = 'https://softypetestdata.000webhostapp.com/WooCommerce-REST-API/API/';
				var url = host + GetOrdersAPI + "?after=" + format_after_date;
				//	var my_account  = 'my-account/my-account-view-order/'+orderId
				//url+='url='+host+'&cKey='+cKey+'&csecret'+cSecret;

				log.debug('url', url);
				var response = https.get({
					url: url,
					headers: headers
				});
				log.debug('response-->', response);
				var dataBody = JSON.parse(response.body);
				//dataBody = dataBody.orders;
				log.debug('dataBody', dataBody);


				var x = typeof (dataBody);
				log.debug('x', x);

				var date = new Date();
				date.setDate(date.getDate() + 7);
				var expectedDate = JSON.stringify(date);
				var str = expectedDate.substring(1, 11).split("-");
				var expected_date = (str[1] + '/' + str[2] + '/' + str[0]);

				var format_date = parseAndFormatDateString(expected_date);
				log.debug('format_date', format_date);

				for (var i = 0; i < dataBody.length; i++) {

					var orderId = dataBody[i].id;
					log.audit('orderId', orderId);

					var orderIdDisplay = dataBody[i].number;
					log.audit('orderIdDisplay', orderIdDisplay);

					var memo = dataBody[i].customer_note;
					log.audit('memo', memo);

					var taxTotal = dataBody[i].total_tax; //added on 27 morning after demo
					log.debug('taxTotal', taxTotal);

					var date_created = dataBody[i].created_at;
					log.debug('date_created', date_created);

					var status = dataBody[i].status;
					log.debug('status', status);

					if (status === "shipped" || status === "completed")
						continue;

					var searchId = search.create({
						type: search.Type.SALES_ORDER,
						columns: [{
							name: 'internalid'
						}],
						filters: [{
							name: 'custbody_order_id',
							operator: 'is',
							values: orderId
						}]

					}).run().getRange(0, 999);

					if (searchId.length != 0) {
						if (status === "cancelled") {

							var SalesOrderRecord = record.load({
								type: record.Type.SALES_ORDER,
								id: searchId[0].getValue("internalid")
							});

							var salesOrderStatus = SalesOrderRecord.getValue("status");

							if (salesOrderStatus === "Closed") {
								continue;
							}

							var numLines = SalesOrderRecord.getLineCount({
								sublistId: 'item'
							});

							for (var j = 0; j < numLines; j++) {
								SalesOrderRecord.setSublistValue({
									sublistId: 'item',
									fieldId: 'isclosed',
									line: j,
									value: true
								});
							}

							SalesOrderRecord.save();
							continue;
						} else {
							continue;
						}
					}

					// var emailId = dataBody[i].billing_address.email;
					// log.debug('emailId', emailId);

					var emailId = dataBody[i].billing.email; //customer
					log.debug('emailId', emailId);

					var firstN = dataBody[i].billing.first_name;
					var lastN = dataBody[i].billing.last_name;
					var salesRep = "";

					var customer = 0;
					var contactId = 0;
					//var customerVatApplicable = false;
					var terms;
					var searchSalesRep = search.create({
						type: search.Type.EMPLOYEE,
						columns: [{ name: 'internalid' }],
						filters: [{
							name: 'firstname',
							operator: 'is',
							values: firstN
						}, {
							name: 'lastname',
							operator: 'is',
							values: lastN
						}]

					}).run().getRange(0, 999);

					if (searchSalesRep.length != 0) {
						log.audit('Order placed by Sales Rep', JSON.stringify(searchSalesRep));
						firstN = dataBody[i].shipping.first_name;
						lastN = dataBody[i].shipping.last_name;
						salesRep = searchSalesRep[0].id;
					}

					log.debug('firstN', firstN);
					log.debug('lastN', lastN);
					var searchCustomer = search.create({
						type: search.Type.CUSTOMER,
						columns: [{
							name: 'internalid'
						}, //{ name: 'custentity_vat_applicable' },
						 { name: 'terms' }],
						filters: [{
							name: 'firstname',
							operator: 'is',
							values: firstN
						}, {
							name: 'lastname',
							operator: 'is',
							values: lastN
						}]

					}).run().getRange(0, 999);

					log.debug('searchCustomer', searchCustomer);

					if (searchCustomer.length != 0) {
						//customerVatApplicable = searchCustomer[0].getValue('custentity_vat_applicable')
						customer = searchCustomer[0].id;
						terms = searchCustomer[0].getText('terms');
					} else {
						return;
					}


					var firstNameBill = dataBody[i].shipping.first_name;
					log.debug('firstName', firstNameBill);

					var lastNameBill = dataBody[i].shipping.last_name;
					log.debug('lastNameBill', lastNameBill);

					var addressBill = dataBody[i].shipping.address_1;
					log.debug('addressBill', addressBill);

					var cityBill = dataBody[i].shipping.city;
					log.debug('cityBill', cityBill);

					var countryBill = dataBody[i].shipping.country;
					log.debug('countryBill', countryBill);

					var entityName = firstNameBill.concat(lastNameBill);

					var firstName = dataBody[i].shipping.first_name;
					log.debug('firstName', firstName);

					var lastName = dataBody[i].shipping.last_name;
					log.debug('lastName', lastName);

					var address = dataBody[i].shipping.address_1;
					log.debug('address', address);

					var city = dataBody[i].shipping.city;
					log.debug('city', city);

					var country = dataBody[i].shipping.country;
					log.debug('country', country);


					var data = dataBody[i].meta_data;
					log.debug('data', data);

					var metaData = {};
					log.debug('data length' + typeof (data) + " " + data.length, JSON.stringify(data));
					for (var k = 0; k < data.length; k++) {

						metaData[data[k].key] = data[k].value;
					}
					log.debug('metaData', metaData);


					var newRecord = record.create({
						type: record.Type.SALES_ORDER,
						isDynamic: false
					});

					/*newRecord.setValue({
						fieldId: "customform",
						value: customform// Trading SO Form
					});*/

					if (!(metaData.hasOwnProperty("for_customer") && metaData.hasOwnProperty("zone"))) {

						newRecord.setValue({
							fieldId: "entity",
							value: customer //436// Kel Custodio
						});
					} else {

						var searchCustomer = search.create({
							type: search.Type.CUSTOMER,
							columns: [{
								name: 'internalid'
							}, { name: 'custentity_vat_applicable' }, { name: 'terms' }],
							filters: [{
								name: 'email',
								operator: 'is',
								values: metaData["for_customer"]
							}]

						}).run().getRange(0, 999);

						terms = searchCustomer[0].getText('terms');
						newRecord.setValue({
							fieldId: "entity",
							value: searchCustomer[0].id
						});

						newRecord.setValue({
							fieldId: "custbody_ecomm_sales_rep",
							value: customer
						});

					}

					if (metaData.hasOwnProperty("sales_order_type")) {
						newRecord.setValue({
							fieldId: "custbody_so_ship_type",
							value: metaData["sales_order_type"]
						});
						var PICK_UP = 1;
						if (metaData["sales_order_type"] == PICK_UP) {
							newRecord.setValue({
								fieldId: "custbody_customer_pickup_shipment",
								value: true
							});
						}
					}

					var SUBSIDIARY_STANDALONE = 2;

					/*newRecord.setValue({
						fieldId: "subsidiary",
						value: subsidiaryStandalone
					});*/

					newRecord.setValue({
						fieldId: "location",
						value: LOCATION
					});

					/*newRecord.setValue({
						fieldId: "class",
						value: defaultDivision
					});*/

					newRecord.setValue({
						fieldId: "custbody_customer_payment_term",
						value: terms
					});

					newRecord.setValue({
						fieldId: "custbody_order_id",
						value: orderId
					});

					newRecord.setValue({
						fieldId: "custbody_ecom_webstore_id",
						value: orderIdDisplay
					});
					if (!!memo) {
						newRecord.setValue({
							fieldId: "memo",
							value: memo
						});
					}

					newRecord.setValue({
						fieldId: "orderstatus",
						value: 'A' 	// Pending Approval
					});
					newRecord.setValue({
						fieldId: "custbody_woocommerce_website",
						value: website
					});

					newRecord.setValue({
						fieldId: "custbody_expected_delivery_date",
						value: format_date
					});

					if (!!host) {
						log.debug("host", host);
						if (host.length != 0) {
							newRecord.setValue({
								fieldId: "custbody_host_url",
								value: host
							});
							newRecord.setValue({
								fieldId: "custbody_ecom_order_link",
								value: webstoreOrderLink
							});
						}
					}

					if (!!website_logo) {
						log.debug("website_logo", website_logo);
						if (website_logo.length != 0) {
							newRecord.setValue({
								fieldId: "custbody_woocommerce_logo",
								value: Number(website_logo[0].value)
							});
						}
					}

					if (!!website_logo_2) {
						log.debug("website_logo_2", website_logo_2);
						if (website_logo_2.length != 0) {
							newRecord.setValue({
								fieldId: "custbody_website_logo_2",
								value: Number(website_logo_2[0].value)
							});
						}
					}

					if (displayUOMonPrint) {
						newRecord.setValue({
							fieldId: "custbody_ecom_display_uom_print",
							value: true
						});
					}

					if (contactId != 0) {
						newRecord.setValue({
							fieldId: "custbody_ecomm_sales_rep",
							value: contactId
						});
					}
					if (setShippingAddress) {
						newRecord.setValue({
							fieldId: "custbody_set_shipping_address",
							value: true
						});
					}

					if (!!salesRep) {
						newRecord.setValue({
							fieldId: "custbody_preparedby",
							value: salesRep
						});
					}
					//line items data
					var lineItems = dataBody[i].line_items;
					log.debug('lineItems', lineItems);

					for (var j = 0; j < lineItems.length; j++) {

						log.debug('product_id', lineItems[j].product_id);

						var itemID = search.create({
							type: search.Type.ITEM,
							columns: [{
								name: 'internalid'
							}],
							filters: [{
								name: 'custitem_woocom_id',
								operator: 'is',
								values: lineItems[j].product_id
							}]

						}).run().getRange(0, 999);
						log.debug('itemID', JSON.stringify(itemID));

						var quantity = lineItems[j].quantity;
						log.debug('quantity', quantity);

						var rate = lineItems[j].total;
						log.debug('rate', rate);

						var totalRate = Number(rate) / Number(quantity)
						log.debug('totalRate', totalRate);

						var totalTax = lineItems[j].total_tax;
						log.debug('totalTax', totalTax);

						var internalIDItem = itemID[0].getValue({
							name: 'internalid'
						});
						log.debug('internalIDItem', internalIDItem);

						// var subRec = newRecord.getSubrecord({
						// 	fieldId: 'shippingaddress'
						// });
						// subRec.setValue({
						// 	fieldId: 'country',
						// 	value: country
						// });

						// subRec.setValue({
						// 	fieldId: 'city',
						// 	value: city
						// });

						// subRec.setValue({
						// 	fieldId: 'addr1',
						// 	value: address
						// });

						//shipping address
						var customerRecord = record.load({
							type: "customer",
							id: customer
						});

						var shipAddressLines = customerRecord.getLineCount({
							sublistId: 'addressbook'
						});

						for (var ship_i = 0; ship_i < shipAddressLines; ship_i++) {
							var ship_address_1 = customerRecord.getSublistValue({
								sublistId: 'addressbook',
								fieldId: "addr1_initialvalue",
								line: ship_i
							});
							var ship_city = customerRecord.getSublistValue({
								sublistId: 'addressbook',
								fieldId: "city_initialvalue",
								line: ship_i
							});

							log.audit("shipping address 1", ship_address_1);
							log.audit("shipping address ", address);
							log.audit("shipping ship_city", ship_city);
							log.audit("shipping city", city);
							log.audit("shipping address condition", ship_address_1 == address && ship_city == city);

							if (ship_address_1 == address && ship_city == city) {
								var ship_internalid = customerRecord.getSublistValue({
									sublistId: 'addressbook',
									fieldId: "id",
									line: ship_i
								});

								log.audit("shipping ship_internalid", ship_internalid);

								newRecord.setValue({
									fieldId: 'shipaddresslist',
									value: ship_internalid
								});

								break;
							}

						}

						// var shipaddresslist = newRecord.getField({
						// 	fieldId: 'shipaddresslist'
						// });
						// log.debug('shipaddresslist',shipaddresslist);
						// var addressOptions = shipaddresslist.getSelectOptions({
						// 	filter : city,
						// 	operator : 'contains'
						// });
						// log.debug('addressOptions',JSON.stringify(addressOptions));


						//for billing address
						var subRec = newRecord.getSubrecord({
							fieldId: 'billingaddress'
						});

						subRec.setValue({
							fieldId: 'country',
							value: countryBill
						});
						subRec.setValue({
							fieldId: 'city',
							value: cityBill
						});
						subRec.setValue({
							fieldId: 'addr1',
							value: addressBill
						});

						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'item',
							value: internalIDItem,
							line: j
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'units',
							value: GetBaseUnit(internalIDItem),
							line: j
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'location',
							value: LOCATION,
							line: j
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'inventorylocation',
							value: LOCATION,
							line: j
						});

						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'quantity',
							value: quantity,
							line: j
						});
						// newRecord.setSublistValue({
						// 	sublistId: 'item',
						// 	fieldId: 'pricelevels',
						// 	value: -1,
						// 	line: j
						// });
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'rate',
							value: totalRate,
							line: j
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'amount',
							value: rate,
							line: j
						});

						// Getting Taxcode
						var DEFAULT0VAT = 20887;

						var taxSchedule = (search.lookupFields({
							type: "item",
							id: internalIDItem,
							columns: ['taxschedule']
						})).taxschedule;
                      
                      	log.debug("taxSchedule", JSON.stringify(taxSchedule));

						if (taxSchedule.length != 0) {

							var taxScheduleRecord = record.load({
								type: 'taxschedule',
								id: taxSchedule[0].value
							});

							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'taxcode',
								value: taxScheduleRecord.getSublistValue({
									sublistId: 'nexuses',
									fieldId: 'salestaxcode',
									line: 1
								}),
								line: j
							});
						} else {
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'taxcode',
								value: defaultVat,  // VAT 0
								line: j
							});
						}


						var data = lineItems[j].meta_data;
						log.debug('data', data);

						var metaData = {};
						log.debug('data length' + typeof (data) + " " + data.length, JSON.stringify(data));
						for (var k = 0; k < data.length; k++) {

							metaData[data[k].key] = data[k].value;
						}
						log.debug('metaData', metaData);

						if (metaData.hasOwnProperty("color")) {

							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_item_color',
								value: metaData["color"],
								line: j
							});
						}

						if (metaData.hasOwnProperty("months")) {

							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_item_months',
								value: metaData["months"],
								line: j
							});
						}

					}

					// newRecord.setValue({ fieldId: "custbody_zone_so", value: "NO" });
					// newRecord.setValue({ fieldId: "custbody_area_so", value: 6 });


					var recordID = newRecord.save({
						enableSourcing: false,
						ignoreMandatoryFields: true
					});
					log.emergency("record ID", recordID);

				}

			} catch (err) {
				log.debug("Exception", err);
			}
		}

		function GetBaseUnit(internalIDItem) {
			var itemRecord = record.load({
				type: "inventoryitem",
				id: internalIDItem
			});
			return itemRecord.getValue("baseunit");
		}

		function parseAndFormatDateString(myDate) {
			var initialFormattedDateString = myDate;
			var parsedDateStringAsRawDateObject = format.parse({
				value: initialFormattedDateString,
				type: format.Type.DATE,
			});
			return parsedDateStringAsRawDateObject;
		}

		function formatDate(userDate) {
			userDate = new Date(userDate);
			y = userDate.getFullYear().toString();
			m = (userDate.getMonth() + 1).toString();
			d = userDate.getDate().toString();
			if (m.length == 1) m = '0' + m;
			if (d.length == 1) d = '0' + d;
			return m + d + y;
		}

		return {
			execute: execute
		};
	});		