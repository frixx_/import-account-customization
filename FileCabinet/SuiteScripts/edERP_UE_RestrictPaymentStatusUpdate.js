/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                
 **                       
 **@Author      :  Anand Yadav
 **@Dated       :  23 Sep 2019
 **@Version     :  2.0
 **@Description :  To Prevent user from accessing Payment Status custom record.
 ***************************************************************************************/

define(['N/record','N/runtime','N/error'],

function(record,runtime,error,message,serverWidget) {
   
    
    function beforeLoad(context) {

    }

    function beforeSubmit(context) {
    	
		//Execution Context of the Script
    	var exeContext=runtime.executionContext;
    	log.debug("exeContext",exeContext);
    	
    	var exeType=context.type;
    	log.debug("exeType",exeType);
    	
    	if(exeContext=='USERINTERFACE'){
    		
    		log.debug("exeContext",exeContext);
    		
    		 var errorObj = error.create({
                 name: 'UNAUTHORISED_ACCESS',
                 message: 'You are not Authorised to access this record.Please contact your Softype Account Manager to update your Billing Status',
                 notifyOff: true
             });
    		 
    		throw errorObj.message;
    		
    		return false;
    	}
        		
    }

    function afterSubmit(context) {
    	
    }

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
