/**

 * @NModuleScope public

 */
define(['N/search'],

function(search) {
   
	function edERPLicencingCheck(){
		log.debug('Library File Called');
		
		var paymentStatus="";
		var Columns=[];
		Columns.push(search.createColumn({name:'custrecord_payment_status'}));
		               
	    var searchResult = search.create({
	        type: 'customrecord_payment_status',
	        filters: null,
	        columns: Columns
	    }).run().getRange({start:0,end:1000});
		
	    if(searchResult.length>0){
			
			var recId=searchResult[0].id;
			log.debug('recId',recId);
			
			paymentStatus=searchResult[0].getValue('custrecord_payment_status');
			log.debug('paymentStatus',paymentStatus);
		}
		return paymentStatus;
	}
	
    return {
        
    	edERPLicencingCheck:edERPLicencingCheck
    };
    
});
