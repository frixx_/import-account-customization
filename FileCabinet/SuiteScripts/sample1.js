// Client side script on sales order, on save
// Load the 1st item and mark it inactive
function onSave()
{
   var id = nlapiGetLineItemValue('item', 'item', 1);
   var record = nlapiLoadRecord('inventoryitem', id);
   record.setFieldValue('isinactive', 'T');
   nlapiSubmitRecord(record);

   return true;
}

function onAlert(){
	alert("This is an alert");
}