function credentials(){
    this.email = "renatoc@softype.com";
    this.account = "TSTDRV1393316";
    this.role = "1138";
    this.password = "renSoftype100";
}
 
function replacer(key, value){
    if (typeof value == "number" && !isFinite(value)){
        return String(value);
    }
    return value;
}
 
//Setting up URL              
var url = "https://tstdrv1393316.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=469&deploy=1";

//Calling credential function
var cred = new credentials();



//Setting up Headers 
var headers = {"User-Agent-x": "SuiteScript-Call",
               "Authorization": "NLAuth nlauth_account=" + cred.account + ", nlauth_email=" + cred.email + 
                                ", nlauth_signature= " + cred.password + ", nlauth_role=" + cred.role,
               "Content-Type": "application/json"};
                
//Setting up dataput
var jsonobj = [{ "recordtype":"salesorder",
                 "entity":"Renato A Test",
                 "date": "11/7/2019",
                 "status": "Pending Approval"
                }];

//Stringifying JSON
var myJSONText = JSON.stringify(jsonobj, replacer);
var getHeaders = JSON.stringify(headers, replacer);
 
var response = nlapiRequestURL(url, headers);
var body = response.body;
   
//Below is being used to put a breakpoint in the debugger
var i=0;
 
 nlapiLogExecution('DEBUG', 'headers = ', getHeaders);
 nlapiLogExecution('DEBUG', 'jsonObject = ', myJSONText);
 nlapiLogExecution('DEBUG', 'body = ', body);


//**************RESTLET Code****************

// Get a standard NetSuite record
function getRecord(data)
{
    var obj = nlapiLoadRecord(data.recordtype, data.id);
    /*
    var result = {
        "tranid": obj.getFieldValue('tranid'),
        "status": obj.getFieldValue('status'),
        "date": obj.getFieldValue('trandate')
    };
    */
    return obj;
}
