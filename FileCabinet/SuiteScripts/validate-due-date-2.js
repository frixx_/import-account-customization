 /**
 *@NApiVersion 2.x
 *@NScriptType ClientScript
 */

define(['N/error','N/format','N/log'],
    function(error, format, log) {

    function validateField(context) {
   
    var currentDate = new Date();

    //Add days
	currentDate.setDate(currentDate.getDate()+30);

	//format current date with a specified timezone: Asia/Mania
    var newDate = format.format({
        value: currentDate, 
        type: format.Type.DATE, 
        timezone: format.Timezone.ASIA_MANILA   
    });   
    log.debug('currentDateAdded', '' +newDate);

    //get field value
    var currentRecord = context.currentRecord;
    var initEndDate = currentRecord.getValue({
    	fieldId: 'enddate'
    });

    //convert timezone of the field value into Asia/Manila
     var endDate = format.format({
        value: initEndDate, 
        type: format.Type.DATE, 
        timezone: format.Timezone.ASIA_MANILA   
    });
	log.debug('endDate', '' + endDate);    
	

	var fieldId = context.fieldId;
	if(fieldId==='enddate'){
    
	if(newDate!=endDate){
       alert("Should be 30 days from today"); 
       return false;
	}
}  
return true;
    
}  
    
        function saveRecord(context) {
            if( confirm("Please review all fields. Are you sure you want to submit?")){
        return true;
	}else{
		return false;
	}

        }
        return {
            validateField: validateField,
            saveRecord: saveRecord
        	   };
    });
    


    