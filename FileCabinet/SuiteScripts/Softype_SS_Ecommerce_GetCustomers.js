/**
 *@NApiVersion 2.x
 *@NScriptType ScheduledScript
 */
define(['N/https', 'N/record', 'N/search', 'N/log', 'N/format', 'N/runtime', 'N/task', 'N/url', 'N/error'],
    function(https, record, search, log, format, runtime, task, url, error) {
        function execute(scriptContext) {
            var headers = new Array();
            headers["Content-Type"] = "application/json";

          //  url = 'https://softypetestdata.000webhostapp.com/customers.json';
		  
		  	var ckey = runtime.getCurrentScript().getParameter('custscript_customer_consumer_key');
			var csecret = runtime.getCurrentScript().getParameter('custscript_customer_consumer_secret');
			var host_url = runtime.getCurrentScript().getParameter('custscript_customer_host_url');
			
			var stdUrl = 'https://softypetestdata.000webhostapp.com/WooCommerce-REST-API/API/';
			var url = stdUrl + 'GetCustomers.php?url=' + host_url + '&ckey=' + ckey + '&csecret=' + csecret;

            var response = https.get({
                url: url,
                headers: headers
            });
            log.debug('response-->', response);

            var dataBody = JSON.parse(response.body);

			dataBody = dataBody.customers;
			            log.debug('dataBody',typeof(dataBody));
			            log.debug('dataBody',dataBody);

            for (var i = 0; i < dataBody.length; i++) {

                var entityId = dataBody[i].id;
                log.debug('entityId', entityId);
				
				var emailId = dataBody[i].email;
                log.debug('emailId', emailId);
				
                var searchId = search.create({
                    type: search.Type.CUSTOMER,
                    columns: [{
                        name: 'internalid'
                    }],
                    filters: [{
                        name: 'email',
                        operator: 'is',
                        values: emailId
                    }]

                }).run().getRange(0, 999);

                if (searchId.length != 0)
                    continue;

                var firstName = dataBody[i].first_name;
                log.debug('firstName', firstName);

                var lastName = dataBody[i].last_name;
                log.debug('lastName', lastName);

               

                var date_created = dataBody[i].created_at;
                log.debug('date_created', date_created);


                var phone = dataBody[i].billing_address.phone;
                log.debug('phone', phone);

                var newRecord = record.create({
                    type: record.Type.CUSTOMER,
                    isDynamic: false
                });

                // newRecord.setValue({
                    // fieldId: "customform",
                    // value: -2
                // });

                newRecord.setValue({
                    fieldId: "entityid",
                    value: entityId
                });

                newRecord.setValue({
                    fieldId: "subsidiary",
                    value: 3
                });

                newRecord.setValue({
                    fieldId: "firstname",
                    value: firstName
                });

                newRecord.setValue({
                    fieldId: "lastname",
                    value: lastName || ''
                });

                // newRecord.setValue({
                    // fieldId: "entitystatus",
                    // value: 30
                // });

                newRecord.setValue({
                    fieldId: "email",
                    value: emailId
                });

                var recordID = newRecord.save();
                log.emergency("record ID", recordID);

            }
        }
        return {
            execute: execute
        };
    });