function createRecord(data)
{
    var err = new Object();
   
    // Validate if mandatory record type is set in the request
    if (!data.recordtype)
    {
        err.status = "failed";
        err.message = "missing recordtype";
        var error = JSON.stringify(err);
        return error;
    }
   
    var record = nlapiCreateRecord(data.recordtype);
   
    for (var fieldname in data)
    {
        if (data.hasOwnProperty(fieldname))
        {
            if (fieldname != 'recordtype' && fieldname != 'id')
            {
                var value = data[fieldname];
                if (value && typeof value != 'object') // ignore other type of parameters
                {
                    record.setFieldValue(fieldname, value);
                }
            }
        }
    }
    var recordId = nlapiSubmitRecord(record);
    nlapiLogExecution('DEBUG','id='+recordId);
   
    var nlobj = nlapiLoadRecord(data.recordtype,recordId);
    //var nlobString = JSON.stringify(nlobj);
    return nlobj;

}
