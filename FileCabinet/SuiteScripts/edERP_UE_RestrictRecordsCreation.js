/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/error', 'N/record', 'N/runtime','./edERPLicenceCheck'],
/**
 * @param {error} error
 * @param {record} record
 * @param {runtime} runtime
 */
function(error, record, runtime,edERPLicenceCheck) {
   
    function beforeLoad(context) {
    		
    	//Execution Context of the Script
    	var exeContext=runtime.executionContext;
    	log.debug("exeContext",exeContext);
    	
    	var exeType=context.type;
    	log.debug("exeType",exeType);
    	
    	if(exeContext=='USERINTERFACE' && exeType=='create'){
    		
    		log.debug("exeContext",exeContext);
    		
    		var paymentStatus=edERPLicenceCheck.edERPLicencingCheck();
    		log.debug('paymentStatus',paymentStatus);

    		if(paymentStatus==3 || paymentStatus==""){
    			
    			var errorObj = error.create({
   	             name: 'UNAUTHORISED_ACCESS',
   	             message: 'You are not Authorised to access this record.Please contact your Softype Account Manager to update your Billing Status',
   	             notifyOff: true
   	         });
       		 
	       		throw errorObj.message;
	       		
	       		return false;
    		}
    		
			 
    	}
    }

    function beforeSubmit(scriptContext) {

    }

    function afterSubmit(scriptContext) {

    }

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
