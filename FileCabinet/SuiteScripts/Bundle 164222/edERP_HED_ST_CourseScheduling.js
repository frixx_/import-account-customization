/* **************************************************************************************  
 ** Copyright (c) 1998-2014 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of          
 ** Softype, Inc. ("Confidential Information"). You shall not               
 ** disclose such Confidential Information and shall use it only in          
 ** accordance with the terms of the license agreement you entered into    
 ** with Softype.                       
 ** @author: Sarah Akid
 ** @version: 1.0
 ** Description: Suitelet Script for Course Scheduling
 ************************************************************************************** */
var listBldngs;
var ajaxloader;
var arrowleft;
var arrowRight;
var checkIcon;
var information;
var AcadPlanCSS;
var jBoxCSS;
var jBoxLib;
var jqueryLib;
var calendarCSS;
var autoCompleteLib;
var oneWorld = nlapiGetContext().getFeature('SUBSIDIARIES');
var Customer_Employee='customer';

//EXTENSION OF ARRAY
Array.prototype.getParameter = function (key) {
	return this[key];
};

/**GLOBAL VARs**/
//if In last term of the year do not display current Acad Year in list, else Display
var isInLastTerm=false;
var stLoggerTitle = 'Course Scheduling Assistant';
//In case going back from Faculty & Classrooms Planning
var courseBack,termBack,yearBack;
var USAGE_LIMIT_THRESHOLD=200;

var listCourseComponents, listGroups;

/**----MAIN FUNCTION OF COURSE SCHEDULING----*/
function suitelet_CourseSchedulingMain(request, response)
{
	try
	{
		nlapiLogExecution('Debug',stLoggerTitle,'----START----');

		//CREATE FORM
		var formCP = nlapiCreateForm(stLoggerTitle, false);
		formCP.setScript('customscript_ederph_cs_courseschd');

		//VERFIY USER'S ROLE AGNST SETUP
		var roleUser=nlapiGetContext().getRole();
		var setup=getSetup();
		if(setup!=null && setup!='')
		{	
			var accessRole=setup[0].getValue('custrecord_ederp_setup_schedaccessrole');
                        if(accessRole.indexOf(',')>=0)	
			{
				accessRole=accessRole.split(',');
				if(accessRole.indexOf(roleUser.toString())<0 )
					throw nlapiCreateError('Notice','Your role is not authorized to access this page.',true);
			}
			else if (accessRole!=roleUser)
				throw nlapiCreateError('Notice','Your role is not authorized to access this page.',true);

		}
		else
		{
			throw nlapiCreateError('Notice','This subsidiary has no setup.',true);
		}

		//REQUEST VARs
		var resultOfSaving='';
		var headerMsg=request.getParameter('header');
		headerMsg=headerMsg==undefined?'':base64_decode(headerMsg);

		var dptm = getDepartment(); 
		
		
		//HTML Header
		var fldHtmlHeader=formCP.addField('custpage_header','inlinehtml','');

		//Fields on suitlet to get image url from client side call
		formCP.addField('custpage_img_ajaxloader','url','ajax-loader.gif').setDisplayType('hidden');
		formCP.addField('custpage_img_arroleft','url','arrow_left.png').setDisplayType('hidden');
		formCP.addField('custpage_img_arroright','url','arrow_right.png').setDisplayType('hidden');
		formCP.addField('custpage_img_checkicon','url','check-icon.png').setDisplayType('hidden');
		formCP.addField('custpage_img_information','url','information.png').setDisplayType('hidden');

		//Dynamic url search from file cabinet
		var searchResults = nlapiSearchRecord(null,'customsearch_ederp_documentsearch',null, null);
		if(searchResults)
		{
			nlapiLogExecution('Debug','searchResults length = ',searchResults.length);
			for(var i=0;i<searchResults.length;i++)
			{
				var searchResult = searchResults[i];
				var columns = searchResults[i].getAllColumns();
				var name = searchResult.getValue(columns[0]);
				var folderName = searchResult.getText(columns[1]);

				if(folderName == 'Images')
				{
					if(name == 'edERP_HED_ajax-loader.gif')
					{
						ajaxloader = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_arrow_left.png')
					{
						arrowleft = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_arrow_right.png')
					{
						arrowRight = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_check-icon.png')
					{
						checkIcon = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_information.png')
					{
						information = searchResult.getValue(columns[3]);
						nlapiLogExecution('Debug','information url = ',information);
					}
				}
				else if(folderName == 'CSS')
				{
					if(name == 'edERP_HED_AcademicPlanning.css')
					{
						AcadPlanCSS = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_jBox.css')
					{
						jBoxCSS = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_Calendar.css')
					{
						calendarCSS = searchResult.getValue(columns[3]);

					}

				}
				else if(folderName == 'Library')
				{
					if(name == 'edERP_HED_jBox.min.js')
					{
						jBoxLib = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_jquery.min.1.9.1.js')
					{
						jqueryLib = searchResult.getValue(columns[3]);

					}
					else if(name == 'edERP_HED_jquery.autocomplete.js')
					{
						autoCompleteLib = searchResult.getValue(columns[3]);

					}
				}

			}
		}

		//HTML HEADER
		var htmlHeader='<!DOCTYPE html> <html> <head> <meta charset="UTF-8">';
		htmlHeader+="<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css' />";	
		htmlHeader+="<link href="+AcadPlanCSS+" rel='stylesheet' type='text/css' />";
		htmlHeader+="<link href="+jBoxCSS+" rel='stylesheet' type='text/css' />";
		htmlHeader+="<link href="+calendarCSS+" rel='stylesheet' type='text/css' />";
		htmlHeader+='<script src='+jBoxLib+' type="text/javascript"></script>';
		htmlHeader+='<script src='+jqueryLib+' type="text/javascript"></script>';
		htmlHeader+='<script src='+autoCompleteLib+' type="text/javascript"></script>';
		htmlHeader+="</head>";

		formCP.setFieldValues({custpage_img_ajaxloader:ajaxloader,custpage_img_arroleft:arrowleft,custpage_img_arroright:arrowRight,custpage_img_checkicon:checkIcon,custpage_img_information:information});

		
		var goBackMethod='scratch';
		var action=request.getParameter('action');
		if(!isEmpty(action))
		{
			nlapiLogExecution('Debug',stLoggerTitle,'----In Action----'+action);
			if(action=="getprograms")
			{
				var result=getPrograms(request);
				response.write(result.toString());
				return;
			}
			if(action=="getsections")
			{
				var result=getScheduler(request);
				response.write(result.toString());
				return;
			}
			if(action=="getcourses")
			{
				var result=getCourses(getDepartment(),getSubsidiary(),request);
				response.write(result.toString());
				return;
			}
			if(action=="getdays")
			{
				var result=getDays();
				response.write(result.toString());
				return;
			}
			if(action=="gettimes")
			{
				var result=getTimes();
				response.write(result.toString());
				return;
			}
			if(action=="getsummary")
			{
				var result=getSummary(request);
				response.write(result.toString());
				return;
			}
			if(action=="saving")
			{

				var request=EncToDec('saving', request);
				resultOfSaving=saveAndDisplay(request, response , formCP);
				if(request.getParameter('mode')=='draft')
					headerMsg='Course schedule sucessfully Saved as draft';


			}
			if(action=='getroomdetails')
			{
				var result=getRoomDetails(request);
				response.write(result.toString());
				return;
			}
			if(action=='facclass_display')
			{
				var request=EncToDec('saving', request);
				var result=getHtmlFacClass(request.getParameter('method'), request);
				response.write(result.toString());
				return;
			}
			if(action=='goback')
			{
				if(base64_decode(request.getParameter('year')).indexOf(':')<0)
				{
					yearBack=base64_decode(request.getParameter('year'));
					courseBack=base64_decode(request.getParameter('course'));
					termBack=base64_decode(request.getParameter('term'));
				}
				else
					goBackMethod='historical';
			}
			if(action=='savefacclass')
			{
				headerMsg=saveFaculClass(request);
				if(headerMsg=='abort')
					return;
			}
			if(action=='checkfacav')
			{
				var result=checkFacAvail(request);
				nlapiLogExecution('Debug',stLoggerTitle,'----resp---'+result);
				response.write(result.toString());
				return;
			}
			if(action=='preview')
			{
				resultOfSaving=preview(request,response , formCP);
				if(resultOfSaving=='abort')
					return;
			}
			if(action=='submit')
			{
				headerMsg=submission(request);
			}
			if(action == 'getroomdetails1')
			{
				var result=getRoomDetails1(request);
				response.write(result.toString());
				return;
			}

		}




		/*var htmlHeader='<!DOCTYPE html> <html> <head> <meta charset="UTF-8">';
		htmlHeader+="<link href='/core/media/media.nl?id=403&c=TSTDRV1234890&h=79a29abd79f685fad8f3&_xt=.css' rel='stylesheet' type='text/css' />";
		htmlHeader+="<link href='/core/media/media.nl?id=608&c=TSTDRV1234890&h=78272180cbfcbdd717ee&_xt=.css' rel='stylesheet' type='text/css' />";
		htmlHeader+="<link href='/core/media/media.nl?id=926&c=TSTDRV1234890&h=5184ec611ac3f03c116d&_xt=.css' rel='stylesheet' type='text/css' />";
		htmlHeader+='<script src="/core/media/media.nl?id=303&c=TSTDRV1234890&h=5c12514396785e4f100d&_xt=.js" type="text/javascript"></script>';
		htmlHeader+='<script src="/core/media/media.nl?id=928&c=TSTDRV1234890&h=23ffc6d17c8b89a69988&_xt=.js" type="text/javascript"></script>';
		htmlHeader+='<script src="/core/media/media.nl?id=607&c=TSTDRV1234890&h=e41469d7c8f660c7a7f1&_xt=.js" type="text/javascript"></script>';
		htmlHeader+="</head>";*/

		if(dptm == '' || dptm == null)
		{
			throw nlapiCreateError('Notice','This user is not assigned to any department.',true);
		}
		else
		{
			if(resultOfSaving!='')
			{
				//contains Next Step === Faculties and Classrooms
				if(resultOfSaving!='abort')
				{
					fldHtmlHeader.setDefaultValue(htmlHeader+resultOfSaving);
					response.writePage(formCP);
				}
				return;
			}

			var subsText='';
			if(oneWorld)
				subsText=nlapiLookupField('subsidiary', getSubsidiary(), 'name');
			var campusTxt = getCampus();
			if(isEmpty(campusTxt))
				throw nlapiCreateError('Notice','This user is not assigned to any campus.',true);

			campusTxt = nlapiLookupField('customlist_ederp_campus',campusTxt,'name');

			//REDUCE DEPT AND SUBS NAME BY GETTING LAST PART
			var depts = getDepartment();
			var deptText = '';
			for(var dp=0; dp<depts.length;dp++)
			{
				var deptTxt=nlapiLookupField('department', depts[dp], 'name');
				if(deptTxt.indexOf(':')>=0)
				{
					deptTxt=deptTxt.split(":");
					deptText+=deptTxt[deptTxt.length-1]+'</br>';
				}
				
			}
			if(subsText.indexOf(':')>=0)
			{
				subsText=subsText.split(":");
				subsText=subsText[subsText.length-1];
			}  

			/*----Filter Academic Term----*/
			var optionsAcadTerm="<option></option>";
			optionsAcadTerm=getListAcadTerm(optionsAcadTerm); 
			/*----Filter Academic Year----*/
			var optionsAcadYear="<option></option>";
			optionsAcadYear=getListAcadYear(optionsAcadYear,'scratch');
			var optionsAcadYearH="<option></option>";
			optionsAcadYearH=getListAcadYear(optionsAcadYearH,'historical');  

			//HTML Body 
			var htmlBody="<body>";
			if(!isEmpty(headerMsg))
			{
				htmlBody+='<div id="SuccessMsg"><img src="'+checkIcon+'" style="float:left;margin-top:-7px"/><h2>Confirmation</h2><br/>';
				htmlBody+='<h5>'+headerMsg+'</h5></div>';
			}
			/*---Buttons---*/
			htmlBody+='<div style="margin-bottom:15px;"><input class="btnAP" type="button" value="Save & Next" onClick="onSave(this,\'next\');" disabled />';
			htmlBody+='<input class="btnAP" type="button" value="Save as Draft" onClick="onSave(this,\'draft\');" disabled />';
			htmlBody+='<input id="btnCancel" class="btnAP" type="button" value="Cancel" onClick="onCancel();"  />';
			htmlBody+='<input class="btnAP" type="button" value="Next" onClick="onSave(this,\'next\',\'next\');"  style="visibility:hidden;"/></div>';
			/*---End Buttons---*/

			/*---Step 1 : Select Method---*/
			htmlBody+="<h4 class='FldGrpHdr'> STEP 1: Select Course Scheduling Method </h4>";
			if(goBackMethod=='scratch')
			{
				htmlBody+='<table id= "tableMethod"><tr><td><input type="radio" name="method" value="scratch" checked>From Scratch</td></tr>';
				htmlBody+='<tr><td><input type="radio" name="method" value="historical">From Historical Schedule</td></tr>';
				htmlBody+='</table>';
			}
			else
			{
				htmlBody+='<table id= "tableMethod"><tr><td><input type="radio" name="method" value="scratch" >From Scratch</td></tr>';
				htmlBody+='<tr><td><input type="radio" name="method" value="historical" checked>From Historical Schedule</td></tr>';
				htmlBody+='</table>';
			}
			/*---End Step 1---*/

			/*---Step 2 : Primary Infos---*/
			htmlBody+="<h4 class='FldGrpHdr'> STEP 2: Select Schedule Primary Parameters </h4>";
			/*----Table in case of scratch -----*/
			if(goBackMethod=='scratch')
				htmlBody+='<table id="CrsSchedFields">';
			else
				htmlBody+='<table style="display:none;" id="CrsSchedFields">';
			/*----Subsidiary----*/
			htmlBody+='<tr><td>Subsidiary<br/><span class="deptFld" contenteditable="false" id="'+ getSubsidiary() +'" >'+subsText+'</span></td>';
			/*----Department----*/
			htmlBody+='<td>Department<br/><span class="deptFld" contenteditable="false">'+deptText+'</span></td>';
			/*----Campus----*/
			htmlBody+='<td>Campus<br/><span class="deptFld" contenteditable="false">'+campusTxt+'</span></td></tr>';
			/*----Academic Year----*/
			htmlBody+='<tr><td> Academic Year <font color="#c77f02">*</font><br/>';
			htmlBody+="<select id='listAcadYr' >"+ optionsAcadYear +"</select></td>";
			/*----Academic Term----*/
			htmlBody+='<td> Academic Term <font color="#c77f02">*</font><br/>';
			htmlBody+="<select id='listTerms' >"+optionsAcadTerm +"</select></td>";
			/*----Programs----*/
			htmlBody+='<td valign="top" rowspan="2"> Programs in Section Plan <br/><textarea id="listPrograms" rows="6" readonly> </textarea></td></tr>';
			/*----Course Code----*/
			htmlBody+='<tr><td> Course Code <font color="#c77f02">*</font><br/>';
			if(courseBack!=null && courseBack!='')
				htmlBody+='<div><input type="text" id="autocomplete" value="'+ courseBack +'"';
			else
				htmlBody+='<div><input type="text" id="autocomplete"';

			htmlBody+='/></div></td>';
			/*----Course Description----*/
			htmlBody+='<td> Course Description<br/><span class="deptFld" id="descFld" contenteditable="false"></span> </td></tr>';
			htmlBody+='</table>';
			/*----End Table in case of scratch -----*/
			/*----Table in case of historical -----*/
			if(goBackMethod=='scratch')
				htmlBody+='<table style="display:none;" id="CrsSchedFieldsHist">';
			else
				htmlBody+='<table id="CrsSchedFieldsHist">';
			if(goBackMethod=='historical')
			{
				yearBack=base64_decode(request.getParameter('year')).split(':')[0];
				courseBack=base64_decode(request.getParameter('course'));
				termBack=base64_decode(request.getParameter('term')).split(':')[0];
				/*----Filter Academic Term----*/
				optionsAcadTerm="<option></option>";
				optionsAcadTerm=getListAcadTerm(optionsAcadTerm); 
				/*----Filter Academic Year----*/
				optionsAcadYear="<option></option>";
				optionsAcadYear=getListAcadYear(optionsAcadYear,'scratch');
			}
			else
			{
				yearBack='';
				courseBack='';
				termBack='';
				/*----Filter Academic Term----*/
				optionsAcadTerm="<option></option>";
				optionsAcadTerm=getListAcadTerm(optionsAcadTerm); 
				/*----Filter Academic Year----*/
				optionsAcadYear="<option></option>";
				optionsAcadYear=getListAcadYear(optionsAcadYear,'scratch');
				/*----Filter Academic Year----*/
				optionsAcadYearH="<option></option>";
				optionsAcadYearH=getListAcadYear(optionsAcadYearH,'historical'); 
			}
			/*----Subsidiary----*/
			htmlBody+='<tr><td>Subsidiary<br/><span class="deptFld" contenteditable="false" id="'+ getSubsidiary() +'" >'+subsText+'</span></td>';
			/*----Department----*/
			htmlBody+='<td>Department<br/><span class="deptFld" contenteditable="false">'+deptText+'</span></td>';
			/*----Campus----*/
			htmlBody+='<td>Campus<br/><span class="deptFld" contenteditable="false">'+campusTxt+'</span></td></tr>';
			/*----To Plan Academic Year----*/
			htmlBody+='<tr><td>Academic Year to Schedule <font color="#c77f02">*</font><br/>';
			htmlBody+="<select id='listAcadYrP'>"+ optionsAcadYear +"</select></td>";
			/*----To Plan Academic Term----*/
			htmlBody+='<td>Academic Term to Schedule <font color="#c77f02">*</font><br/>';
			htmlBody+="<select id='listTermsP' >"+optionsAcadTerm +"</select></td>";
			/*----Programs----*/
			htmlBody+='<td valign="top" rowspan="3"> Programs in Section Plan <br/><textarea id="listProgramsH" rows="6" readonly> </textarea></td></tr>';

			if(goBackMethod=='historical')
			{
				yearBack=base64_decode(request.getParameter('year')).split(':')[1];
				termBack=base64_decode(request.getParameter('term')).split(':')[1];
				/*----Filter Academic Term----*/
				optionsAcadTerm="<option></option>";
				optionsAcadTerm=getListAcadTerm(optionsAcadTerm); 
				/*----Filter Academic Year----*/
				optionsAcadYearH="<option></option>";
				optionsAcadYearH=getListAcadYear(optionsAcadYearH,'historical');  
			}
			/*----Hist Academic Year----*/  
			htmlBody+='<tr><td>Historical Academic Year <font color="#c77f02">*</font><br/>';
			htmlBody+="<select id='listAcadYrH' >"+ optionsAcadYearH +"</select></td>";
			/*----Hist Academic Term----*/
			htmlBody+='<td>Historical Academic Term <font color="#c77f02">*</font><br/>';
			htmlBody+="<select id='listTermsH'>"+optionsAcadTerm +"</select></td></tr>";
			/*----Course Code----*/
			htmlBody+='<tr><td> Course Code <font color="#c77f02">*</font><br/>';
			if(courseBack!=null && courseBack!='')
				htmlBody+='<div><input type="text" id="autocompleteH" value="'+courseBack+'"/></div>';
			else
				htmlBody+='<div><input type="text" id="autocompleteH"/></div>';
			htmlBody+="</td>";
			/*----Course Description----*/
			htmlBody+='<td> Course Description<br/><span class="deptFld" id="descFldH" contenteditable="false"></span></td></tr>';
			htmlBody+='</table>';
			/*----End Table in case of historical-----*/

			/*----Schedule & Reset Button----*/
			htmlBody+='<input class="btnAP" type="button" value="Next" onClick="schedule();"  style="margin-left:15px;margin-bottom:15px"/>';
			htmlBody+='<input id="btnReset" class="btnAP" type="button" value="Reset" onClick="onReset(2);"  style="margin-bottom:15px"/></div>';
			/*----End STEP 2----*/

			/*---STEP 3: Calendar---*/
			htmlBody+="<h4 class='FldGrpHdr' style='display:none;'> STEP 3: Set Section Timings</h4>";
			htmlBody+='<div><img id="img-spinner" src="'+ajaxloader+'" alt="Loading"/></div>';

			/*----Summary Table in case of historical-----*/
			htmlBody+="<div id='summaryDiv' style='display:none;'></div>";
			/*----End Summary Table-----*/
			htmlBody+="<table id='scheduler'>";
			htmlBody+="</table>";
			/*---End STEP 3---*/

			/*---Buttons---*/ 
			htmlBody+='<div style="margin-left:15px;margin-top:15px;"><input id="btnBack" class="btnAP" type="button" value="Back" onClick="onBack();" style="visibility:hidden"/>';
			htmlBody+='<input id="btnClear" class="btnAP" type="button" value="Clear" onClick="onClear(3);" style="visibility:hidden"/></div>';
			htmlBody+='<div style="margin-top:15px;"><input class="btnAP" type="button" value="Save & Next" onClick="onSave(this,\'next\');" disabled style="visibility:hidden"/>';
			htmlBody+='<input class="btnAP" type="button" value="Save as Draft" onClick="onSave(this,\'draft\');" disabled style="visibility:hidden"/>';
			htmlBody+='<input id="btnCancel" class="btnAP" type="button" value="Cancel" onClick="onCancel();"  style="visibility:hidden"/>';
			htmlBody+='<input class="btnAP" type="button" value="Next" onClick="onSave(this,\'next\',\'next\');" style="visibility:hidden"/></div>';

			/*---End Buttons---*/

			htmlBody+='</body></html>';
			//End HTML Body 

			fldHtmlHeader.setDefaultValue(htmlHeader+htmlBody);
			response.writePage(formCP);
		}

		return;
	}
	catch(error)
	{
		if (error.getDetails != undefined)
		{
			nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
			throw error;
		}
		else
		{
			nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

/*----ROUTINE THAT SORT TERMS BASED ON START AND END MONTH FOR APC DISPLAY----*/
function sort_terms()
{
	nlapiLogExecution('debug', 'Loading' ,'-------Sorting-----');
	var array=new Array();
	var cols = new Array();
	cols[0] = new nlobjSearchColumn('custrecord_ederp_acadterm_start');
	cols[1] = new nlobjSearchColumn('custrecord_ederp_acadterm_first').setSort(true);
	cols[2] = new nlobjSearchColumn('name');
	var filter=new Array();
	filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	if(oneWorld)
		filter.push(new nlobjSearchFilter('custrecord_ederp_acadterm_subsid', null, 'anyof', getSubsidiary()));
	filter.push(new nlobjSearchFilter('custrecord_ederp_acadterm_campus', null, 'anyof', getCampus()));
	var allTerms = nlapiSearchRecord('customrecord_ederp_acadterm', null, filter, cols);

	if(allTerms!=null && allTerms!='')
	{
		array.push([allTerms[0].getId(),allTerms[0].getValue('custrecord_ederp_acadterm_start'),allTerms[0].getValue('custrecord_ederp_acadterm_first'),allTerms[0].getValue('name')]);
		var lenTermsRem=allTerms.length-1;
		do{
			nlapiLogExecution('debug', 'lenTermsRem' ,lenTermsRem);
			var termToCompare=array[array.length-1];
			var nextTermID=[[]];
			var nextTermIndex=0;
			var minMonthsDiff=12;
			nlapiLogExecution('debug', 'allTerms' ,allTerms.length);
			for(var term=1; term<allTerms.length; term++)
			{
				var termStartMnth=parseInt(allTerms[term].getValue('custrecord_ederp_acadterm_start'));
				var diff;
				if(termStartMnth <= parseInt(termToCompare[1]))
					diff=12-(parseInt(termToCompare[1])-termStartMnth);
				else
					diff=termStartMnth-parseInt(termToCompare[1]);
				nlapiLogExecution('debug', 'diff' ,diff +','+minMonthsDiff);
				if(minMonthsDiff>diff)
				{
					minMonthsDiff=diff;
					nextTermIndex=term;
					nextTermID=[allTerms[term].getId(),allTerms[term].getValue('custrecord_ederp_acadterm_start'),allTerms[term].getValue('custrecord_ederp_acadterm_first'),allTerms[term].getValue('name')];
				}
			}
			nlapiLogExecution('debug', 'nextTermID' ,nextTermID);
			nlapiLogExecution('debug', 'nextTermIndex' ,nextTermIndex);
			array.push(nextTermID);
			allTerms.splice(nextTermIndex);
			lenTermsRem--;

		}while(lenTermsRem>0);
	}

	nlapiLogExecution('debug', 'results' ,array);
	return array;
}

/**----GET OPTIONS LIST OF ACADEMIC TERMS WITH DEFAULT TERM----*/
function getListAcadTerm(optionsAcadTerm)
{
	var today=new Date();
	today=nlapiDateToString(today); 

	var current_month=new Date().getMonth()+1;
	var current_year=new Date().getFullYear();
	var nextTermID;
	var minMonthsDiff=12;

	var results = sort_terms();
	if(results ==null || results == '')
	{
		throw nlapiCreateError('Notice','No active academic term found.',true);
	}
	else
	{
		for(var i=0;i<results.length;i++)
		{
			var startMonthTerm=results[i][1];
			var yearTerm=current_year;
			//get current year and the start month of the term
			if(current_month>=startMonthTerm)
				yearTerm=parseInt(current_year)+1;

			var monthsDiff=(yearTerm-current_year)*12;
			monthsDiff+=startMonthTerm-current_month;

			if(monthsDiff>=minMonthsDiff)
			{
				minMonthsDiff=monthsDiff;
				nextTermID=results[i][0];
			}

		}

		for(var i=0;i<results.length;i++)
		{
			if(nextTermID==results[i][0])
			{
				if(termBack!=null && termBack !='' && results[i][0]!=termBack)
					optionsAcadTerm+=" <option value='"+results[i][0]+"' id='defaultTerm'";
				else
					optionsAcadTerm+=" <option value='"+results[i][0]+"' id='defaultTerm' selected";

				if(results[i][2]=='T')
					isInLastTerm=true;
			}
			else
			{
				if(termBack!=null && termBack !='' && results[i][0]==termBack)
					optionsAcadTerm+=" <option value='"+results[i][0]+"' selected";
				else
					optionsAcadTerm+=" <option value='"+results[i][0]+"'"; 
			}


			optionsAcadTerm+=">"+results[i][3]+"</option>";
		}

	}	
nlapiLogExecution('Debug', 'optionsAcadTerm =  ',optionsAcadTerm);
	return optionsAcadTerm;
}

/**----GET OPTIONS LIST OF ACADEMIC YEARS WITH DEFAULT TERM----*/
function getListAcadYear(optionsAcadYear,method)
{
	var today=new Date();
	today=nlapiDateToString(today); 

	var cols = new Array();
	cols[0] = new nlobjSearchColumn('name');
	cols[1] = new nlobjSearchColumn('custrecord_ederp_acadyear_enddate').setSort();

	var filters= new Array();
	filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	if(oneWorld)
		filters.push(new nlobjSearchFilter('custrecord_ederp_acadyear_subsid', null, 'anyof', getSubsidiary()));
	if(method=='scratch')
		filters.push( new nlobjSearchFilter('custrecord_ederp_acadyear_enddate', null, 'onorafter', today));
	else
		filters.push( new nlobjSearchFilter('custrecord_ederp_acadyear_startdate', null, 'onorbefore', today));
	filters.push(new nlobjSearchFilter('custrecord_ederp_acadyear_campus', null, 'anyof', getCampus()));
	results = nlapiSearchRecord('customrecord_ederp_acadyear', null, filters, cols);
	if(results !=null && results != '')
	{
		for(var i=0;i<results.length;i++)
		{
			var valid=false;
			if(method=='scratch')
			{
				//change back later
				if(i==0)
				{
					optionsAcadYear+="<option value='"+results[i].getId()+"' id='currentYear'";
					valid=true;
				}

				else 
				{
					optionsAcadYear+="<option value='"+results[i].getId()+"'";
					valid=true;
				}

				if(valid && yearBack!=null && yearBack !='' && results[i].getId()==yearBack)
					optionsAcadYear+="selected >"+results[i].getValue('name')+"</option>";
				else if(valid)
					optionsAcadYear+=">"+results[i].getValue('name')+"</option>";
			}
			else
			{

				if(yearBack!=null && yearBack !='' && results[i].getId()==yearBack)
					optionsAcadYear+="<option value='"+results[i].getId()+"' selected>"+results[i].getValue('name')+"</option>";
				else
					optionsAcadYear+="<option value='"+results[i].getId()+"'>"+results[i].getValue('name')+"</option>";


			}

		}
	}
	else
		throw nlapiCreateError('Notice', 'No active academic year found.',true);

	return optionsAcadYear;
}

/**----DANGER: NOT USE SETUP, FILTER BY ROOMS----**/
function getRoomsType(listRooms)
{
	var options='<option>--TBD--</option>';
	var roomTypesGot=new Array();
	for(var lr=0; lr<listRooms.length; lr++)
	{
		if(roomTypesGot.indexOf(listRooms[lr][3])<0)
		{
			options+='<option value="'+listRooms[lr][3]+'">'+listRooms[lr][4]+'</option>';
			roomTypesGot.push(listRooms[lr][3]);
		}
	}
nlapiLogExecution('Debug', 'roomtype option =  ',options);
	return options;
}

/**----SAVE COURSE SCHEDULE RECORDS IF NEEDED AND DISPLAY FACULTY & ROOMS STEP----*/
function saveAndDisplay(request, response ,formCP)
{
	var mode=request.getParameter('mode');
	var method=request.getParameter('method');
	nlapiLogExecution('Debug', 'mode saving',mode);

	var htmlBody='';

	if(mode!='nextwithoutsv')
	{
		if(saveData(request,response)=='abort')
		{
			return 'abort';
		}
		else
		{
			if ((nlapiGetContext().getRemainingUsage() < 950) && mode!='draft')
			{ 

				nlapiLogExecution('Debug', 'usage after saving',nlapiGetContext().getRemainingUsage());
				var param = new Array();
				param['action']='saving';
				param['mode']=base64_encode('nextwithoutsv');
				param['method']=base64_encode(method);
				param['course']= base64_encode(request.getParameter('course'));
				param['arr']=request.getParameter('arr');
				param['realmode']=mode;
				if(method=='scratch')
				{
					param['year']=base64_encode(request.getParameter('year'));
					param['term']=base64_encode(request.getParameter('term'));
				}
				else
				{
					param['yearP']=base64_encode(request.getParameter('yearP'));
					param['termP']=base64_encode(request.getParameter('termP'));
					param['yearH']=base64_encode(request.getParameter('yearH'));
					param['termH']=base64_encode(request.getParameter('termH'));
				}
				nlapiSetRedirectURL('SUITELET','customscript_ederph_st_coursesched', 'customdeploy_ederph_st_coursesched', null, param);
				return 'abort';

			}
		}
	}
	if(mode=='next' || mode=='nextwithoutsv')
	{

		htmlBody="<body>";
		/*---Get Data---*/
		var subsText='';
		if(oneWorld)
			subsText=nlapiLookupField('subsidiary', getSubsidiary(), 'name');
		var campusTxt = nlapiLookupField('customlist_ederp_campus',getCampus(),'name');
		//REDUCE DEPT AND SUBS NAME BY GETTING LAST PART
		var depts = getDepartment();
		var deptText = '';
		for(var dp=0; dp<depts.length;dp++)
		{
			var deptTxt=nlapiLookupField('department', depts[dp], 'name');
			if(deptTxt.indexOf(':')>=0)
			{
				deptTxt=deptTxt.split(":");
				deptText+=deptTxt[deptTxt.length-1]+'</br>';
			}
			
		}
		
		if(subsText.indexOf(':')>=0)
		{
			subsText=subsText.split(":");
			subsText=subsText[subsText.length-1];
		}  

		var course=nlapiLookupField('serviceitem', request.getParameter('course'), 'itemid');	
		var courseDesc=nlapiLookupField('serviceitem', request.getParameter('course'), 'salesdescription');	

		if(method=='scratch')
		{
			var term=nlapiLookupField('customrecord_ederp_acadterm', request.getParameter('term'), 'name');
			var year=nlapiLookupField('customrecord_ederp_acadyear', request.getParameter('year'), 'name');
			/*---Buttons---*/
			htmlBody+='<div style="margin-bottom:15px;"><input id="btnBack" class="btnAP" type="button" value="Back" onClick="goBack(\'' + request.getParameter('year') + '\',\''+course+'\',\''+request.getParameter('term')+'\');"  />';
			htmlBody+='<input class="btnAP" type="button" value="Save as Draft" onClick="onSaveFacClass(this,\'' + request.getParameter('year') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('term')+'\');" disabled />';
			htmlBody+='<input id="btnPrev" class="btnAP" type="button" value="Save & Next" onClick="preview(this,\'' + request.getParameter('year') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('term')+'\',\''+method+'\');" /></div>';
			/*---End Buttons---*/
		}
		else
		{
			var termP=nlapiLookupField('customrecord_ederp_acadterm', request.getParameter('termP'), 'name');
			var yearP=nlapiLookupField('customrecord_ederp_acadyear', request.getParameter('yearP'), 'name');
			var termH=nlapiLookupField('customrecord_ederp_acadterm', request.getParameter('termH'), 'name');
			var yearH=nlapiLookupField('customrecord_ederp_acadyear', request.getParameter('yearH'), 'name');
			/*---Buttons---*/
			htmlBody+='<div style="margin-bottom:15px;"><input id="btnBack" class="btnAP" type="button" value="Back" onClick="goBack(\'' + request.getParameter('yearP') +':'+request.getParameter('yearH') + '\',\''+course+'\',\''+request.getParameter('termP')+':'+request.getParameter('termH')+'\');" />';
			htmlBody+='<input class="btnAP" type="button" value="Save as Draft" onClick="onSaveFacClass(this,\'' + request.getParameter('yearP') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('termP')+'\');" disabled />';
			htmlBody+='<input id="btnPrev" class="btnAP" type="button" value="Save & Next" onClick="preview(this,\'' + request.getParameter('yearP') +':'+request.getParameter('yearH')+ '\',\''+request.getParameter('course')+'\',\''+request.getParameter('termP')+':'+request.getParameter('termH')+'\',\''+method+'\');" /></div>';
			/*---End Buttons---*/
		}

		/*---End Get Data---*/ 
		htmlBody+="<h4 class='FldGrpHdr'>Step 1: Primary Information </h4>";
		htmlBody+='<table id="CrsSchedFields">';
		/*----Subsidiary----*/
		htmlBody+='<tr><td>Subsidiary<br/><span class="depSubsFld" contenteditable="false" >'+subsText+'</span></td>';
		if(method=='scratch')
		{
			/*----Academic Year----*/
			htmlBody+='<td> Academic Year <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" id="year_'+request.getParameter('year')+'">'+year+'</span></td>';
		}
		else
		{
			/*----Academic Year to sched----*/
			htmlBody+='<td> Academic Year To Schedule <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" id="yearp_'+request.getParameter('yearP')+'">'+yearP+'</span></td>';
			/*----hist Academic Year----*/
			htmlBody+='<td> Historical Academic Year <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" id="yearh_'+request.getParameter('yearH')+'">'+yearH+'</span></td>';
		}

		/*----Course Code----*/
		htmlBody+='<td> Course Code <br/>';
		htmlBody+='<span class="depSubsFld" contenteditable="false" >'+course+'</span></td>';
		/*----Programs----*/
		var text='';
		var data=getPrograms(request).toString();
		data=data.split(',');
		for(var i=0;i<data.length;i++)
			text+='-'+ data[i] +'\n';


		htmlBody+='<td valign="top" rowspan="2"> Programs in Section Plan <br/><textarea id="listPrograms" rows="6" readonly>'+text+'</textarea></td></tr>';
		/*----Department----*/
		htmlBody+='<tr><td>Department<br/><span class="depSubsFld" contenteditable="false">'+deptText+'</span></td>';
		if(method=='scratch')
		{
			/*----Academic Term----*/
			htmlBody+='<td> Academic Term <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" id="term_'+request.getParameter('term')+'">'+term+'</span></td>';
		}
		else
		{
			/*----Academic Term to sched----*/
			htmlBody+='<td> Academic Term To Schedule <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" id="termp_'+request.getParameter('termP')+'">'+termP+'</span></td>';
			/*----hist Academic Year----*/
			htmlBody+='<td> Historical Academic Term <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" id="termh_'+request.getParameter('termH')+'">'+termH+'</span></td>';
		}

		/*----Course Description----*/
		htmlBody+='<td> Course Description<br/><span class="depSubsFld" id="descFld" contenteditable="false">'+courseDesc+'</span> </td></tr>';
		/*----Campus----*/
		htmlBody+='<tr><td>Campus<br/><span class="deptFld" contenteditable="false">'+campusTxt+'</span></td></tr>';
		htmlBody+='</table>';
		htmlBody+="<h4 class='FldGrpHdr' id='facultyStep' style='margin-top:-10px;'>Step 4: Assign Section Faculty & Rooms</h4>";
		htmlBody+='<img id="spinner" src="'+ajaxloader+'" style="display:block;margin-top:20px;" alt="Loading"/>';
		htmlBody+="<div id='slideshow' style='margin-left:auto; margin-right:auto;width:100%;'>";
		htmlBody+= getHtmlFacClass(method,request);
		htmlBody+='</div>';

		if(method=='scratch')
		{
			htmlBody+='<div style="margin-left:20px;margin-top:15px;"><input id="btnReset" class="btnAP" type="button" value="Reset" onClick="onReset(4);"  style="visibility:hidden"/>';
			htmlBody+='<input  id="btnClear" class="btnAP" type="button" value="Clear" onClick="onClear(4);"  style="visibility:hidden"/></div>';

			htmlBody+='<div style=" margin-top:15px;"><input id="btnBack" class="btnAP" type="button" value="Back" onClick="goBack(\'' + request.getParameter('year') + '\',\''+course+'\',\''+request.getParameter('term')+'\');"  style="visibility:hidden"/>';
			htmlBody+='<input class="btnAP" type="button" value="Save as Draft" onClick="onSaveFacClass(this,\'' + request.getParameter('year') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('term')+'\');" disabled style="visibility:hidden"/>';
			htmlBody+='<input id="btnPrev" class="btnAP" type="button" value="Save & Next" onClick="preview(this,\'' + request.getParameter('year') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('term')+'\',\''+method+'\');" style="visibility:hidden"/></div>';
		}
		else
		{
			htmlBody+='<div style="margin-left:20px;margin-top:15px;"><input id="btnReset" class="btnAP" type="button" value="Reset" onClick="onReset(4);"  style="visibility:hidden"/>';
			htmlBody+='<input  id="btnClear" class="btnAP" type="button" value="Clear" onClick="onClear(4);"  style="visibility:hidden"/></div>';

			htmlBody+='<div style=" margin-top:15px;"><input id="btnBack" class="btnAP" type="button" value="Back" onClick="goBack(\'' + request.getParameter('yearP') +':'+request.getParameter('yearH') + '\',\''+course+'\',\''+request.getParameter('termP')+':'+request.getParameter('termH')+'\');"  style="visibility:hidden"/>';
			htmlBody+='<input class="btnAP" type="button" value="Save as Draft" onClick="onSaveFacClass(this,\'' + request.getParameter('yearP') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('termP')+'\');" disabled style="visibility:hidden"/>';
			htmlBody+='<input id="btnPrev" class="btnAP" type="button" value="Save & Next" onClick="preview(this,\'' + request.getParameter('yearP') +':'+request.getParameter('yearH')+ '\',\''+request.getParameter('course')+'\',\''+request.getParameter('termP')+':'+request.getParameter('termH')+'\',\''+method+'\');"  style="visibility:hidden"/></div>';
		}

		htmlBody+='</body></html>';
	}
	var fieldHidden=formCP.addField('custpage_facclass_url','longtext','');
	fieldHidden.setDisplayType('hidden');

	nlapiLogExecution('Debug', 'realmode',request.getParameter('realmode') + mode);

	if((request.getParameter('realmode')==undefined && mode=='nextwithoutsv') || (request.getParameter('realmode')!=undefined && request.getParameter('realmode')=='nextwithoutsv'))
	{
		var updateSchedulesFalse=formCP.addField('custpage_update_sched','text','');
		updateSchedulesFalse.setDisplayType('hidden');
	}
	return htmlBody;
}

/**----GET ROOMS IN CURRENT SUBSIDIARY----*/
function getRoomsInBldgs()
{
	var arrAllRooms=new Array();
	var columns = new Array();
	var subs = getSubsidiary();
	nlapiLogExecution('Debug', 'subs@<= ',subs);
	
	var campus = getCampus();
	nlapiLogExecution('Debug', 'campus@<= ',campus);
	
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('name').setSort();
	columns[2] = new nlobjSearchColumn('custrecord_ederp_room_type');
	columns[3] = new nlobjSearchColumn('custrecord_ederp_room_building');
	var filter= new Array();
	filter[0]=new nlobjSearchFilter('isinactive',null,'is','F');
	filter[1]=new nlobjSearchFilter('isinactive','custrecord_ederp_room_building','is','F');
	filter[2]=new nlobjSearchFilter('custrecord_ederp_building_campus','custrecord_ederp_room_building','is',campus);
	if(oneWorld)
		filter[3]=new nlobjSearchFilter('custrecord_ederp_building_subsid','custrecord_ederp_room_building','is',subs);
	var srchRooms = nlapiSearchRecord('customrecord_ederp_room', null, filter, columns);
	nlapiLogExecution('Debug','$$$srchRooms#***>>> ',srchRooms);
	
	if(srchRooms !=null && srchRooms != '')
	{
		nlapiLogExecution('Debug','srchRooms==null');
		for(var i=0; i<srchRooms.length; i++)
		{
			nlapiLogExecution('Debug','srchRoomslength>1');
			arrAllRooms.push([srchRooms[i].getValue('custrecord_ederp_room_building'),srchRooms[i].getValue('internalid'),srchRooms[i].getValue('name'),srchRooms[i].getValue('custrecord_ederp_room_type'),srchRooms[i].getText('custrecord_ederp_room_type')]);
		}
	}
	nlapiLogExecution('Debug','$$$arrAllRooms#***>>> ',arrAllRooms);
	return arrAllRooms;
}

/**----GET LIST OF FACULTY AUTHORIZED TO TEACH A COURSE----*/
function getAuthFaculty(course)
{
	
	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custentity_ederp_authcourses', null, 'anyof',course);
	filter[1]= new nlobjSearchFilter('category', null, 'is',2);
	filter[2]= new nlobjSearchFilter('isinactive',null,'is','F');
	filter[3]= new nlobjSearchFilter('custentity_ederp_campus',null, 'is',getCampus());
	if(oneWorld)
		filter[4]= new nlobjSearchFilter('subsidiary',null, 'anyof',getSubsidiary());
	  //filter[5]= new nlobjSearchFilter('custentity_ederp_custom_inactive_cs',null,'is','F');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('lastname');
	columns[2] = new nlobjSearchColumn('firstname').setSort();
	var srchAuthFac = nlapiSearchRecord('customer', null, filter, columns);
	return srchAuthFac;
}

/**----GET SECTION PLAN----*/
function getSectionPlan_H(method,request)
{
	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_course', null, 'is', request.getParameter('course'));
	if(method=='scratch')
	{
		filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', request.getParameter('term'));
		filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', request.getParameter('year'));
	}
	else
	{
		filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', request.getParameter('termH'));
		filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', request.getParameter('yearH'));
	}

	filter[3]= new nlobjSearchFilter('isinactive',null,'is','F');
	filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',getCampus());
	if(oneWorld)
		filter[5]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',getSubsidiary());
	var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, null);
	return results;
}

/**----GET COURSE SCHEDULE----*/
function getCourseSched_P(method,request)
{
	nlapiLogExecution('Debug', 'with course ',request.getParameter('course'));
	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ederp_schedule_campus',null, 'is',getCampus());
	filter[1]= new nlobjSearchFilter('isinactive',null,'is','F');
	filter[2]= new nlobjSearchFilter('custrecord_ederp_schedule_course',null,'is',request.getParameter('course'));
	if(method=='scratch')
	{
		filter[3]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', request.getParameter('term'));
		filter[4]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', request.getParameter('year'));
	}
	else
	{   
		if(arguments.length==2)
		{
			filter[3]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', request.getParameter('termP'));
			filter[4]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', request.getParameter('yearP'));

		}
		else
		{
			filter[3]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm',null, 'is', request.getParameter('termH'));
			filter[4]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', request.getParameter('yearH'));
			filter[5]= new nlobjSearchFilter('custrecord_ederp_schedule_status', null, 'is',2);
		}
	}
	if(oneWorld)
		filter.push(new nlobjSearchFilter('custrecord_ederp_schedule_subsid',null, 'is',getSubsidiary()));
	
	var col=new Array();
	col[0]=new nlobjSearchColumn('custrecord_ederp_schedule_section');
	col[1]=new nlobjSearchColumn('custrecord_ederp_schedule_faculty');
	col[2]=new nlobjSearchColumn('custrecord_ederp_schedule_room');
	col[3]=new nlobjSearchColumn('custrecord_ederp_schedule_building');

	var srchResults = nlapiSearchRecord('customrecord_ederp_schedule', null, filter, col);
	return srchResults;
}

/**----RETURN HTML TABLE OF FACULTY AND CLASSROOM : STEP4----*/
function getHtmlFacClass(method,request)
{
	var htmlBody='';
	htmlBody+="<div id='slideshowWindow'  style='width:1100px;margin-left:auto; margin-right:auto;'>";

	//Get All rooms in Array
	var arrAllRooms=new Array();
	listBldngs=getBuildings_Subs();
	nlapiLogExecution('Debug','listBldngs=====>>> ',listBldngs);
	if(listBldngs!=null && listBldngs != '')
	{
		arrAllRooms=getRoomsInBldgs();
		nlapiLogExecution('Debug','arrAllRooms=====>>> ',arrAllRooms);
	}
	else
	{
		nlapiLogExecution('Debug','listBldngs==null');
		throw nlapiCreateError('Notice','There are no buildings found under your subsidiary.',true);
	}

	if(arrAllRooms.length==0)
		throw nlapiCreateError('Notice','There are no rooms found under your subsidiary.',true);

	//Done getting All rooms
	//All auth Faculties for this course
	var srchAuthFac = new Array();
	srchAuthFac=getAuthFaculty(request.getParameter('course'));
    nlapiLogExecution('Debug','srchAuthFac=====>>> ',srchAuthFac);
	//Done with auth faculties

	//Get Sec plan or Hist Sec Plan
	var results = getSectionPlan_H(method,request);
	if(results==null || results=='')
		throw nlapiCreateError('Notice','Section plan not found.',true);

	//Get Course Comp Global use
	//getCourseComponents(request.getParameter('course'));
	if(method=='scratch')
		getCourseComponents(request.getParameter('course'),request.getParameter('term'));
	else
		getCourseComponents(request.getParameter('course'),request.getParameter('termP'));

        var arrSections=getCourseSecfromSecP(results[0].getId(),request.getParameter('course'));
	if(arrSections==null || arrSections.length==0)
		throw nlapiCreateError('Notice', 'No sections found under the section plan.');

	var groupWise=false;
	var secGrp=arrSections[0][6];
	if(!isEmpty(secGrp))
		groupWise=true;
	nlapiLogExecution('Debug', 'groupWise',groupWise);
	if(groupWise && method=='scratch')
	{
		arrSections.sort(function(a, b)
				{
			if(parseInt(a[6]) == parseInt(b[6]))
			{
				if(a[7]==b[7])
				{
					if(parseInt(a[0]) == parseInt(b[0]))
					{
						var x = parseInt(a[1]), y = parseInt(b[1]);
						return x == y ? 0 : (x < y ? -1 : 1);
					}
					return parseInt(a[0]) < parseInt(b[0]) ? -1 : 1;
				}
				else
				{
					return a[7]=='T'? -1 : (b[7] == 'T'? 1 : 0);
				}
			}
			return parseInt(a[6]) < parseInt(b[6]) ? -1 : 1;
				});
	}
	else
	{
		arrSections.sort(function(a, b)
				{
			if(a[7]==b[7])
			{
				if(parseInt(a[0]) == parseInt(b[0]))
				{
					var x = parseInt(a[1]), y = parseInt(b[1]);
					return x == y ? 0 : (x < y ? -1 : 1);
				}
				return parseInt(a[0]) < parseInt(b[0]) ? -1 : 1;
			}
			else
			{
				return a[7]=='T'? -1 : (b[7] == 'T'? 1 : 0);
			}
				});
	}


	//Get all CLASSES IN current term and year through the Course Schedule   
	var arrRoomPerYearTerm=new Array();
	var srchResults = getCourseSched_P(method,request);
	if(srchResults !=null && srchResults != '')
	{
		for(var cs=0; cs<srchResults.length; cs++)
		{
			var room=srchResults[cs].getValue('custrecord_ederp_schedule_room');
			var building=srchResults[cs].getValue('custrecord_ederp_schedule_building');
			var faculty=srchResults[cs].getValue('custrecord_ederp_schedule_faculty');
			var section=srchResults[cs].getValue('custrecord_ederp_schedule_section');
			arrRoomPerYearTerm.push([section,room,building,faculty]);
		}
	}
	nlapiLogExecution('Debug', 'arrRoomPerYearTerm',arrRoomPerYearTerm);
	//Done: We now have all the rooms occupied in the current term and year  
	var newComp=0;
	var indexToCompare=0;
	var indexTitle=3;
	if(method=='scratch')
	{
		if(groupWise)
		{
			indexToCompare=6;
			indexTitle=8;
		}

		for(var i=0 ; i<arrSections.length; i++)
		{
			nlapiLogExecution('Debug', 'arrSection Length =',arrSections.length);
			nlapiLogExecution('Debug', 'usage i '+i,nlapiGetContext().getRemainingUsage());
			if(newComp==arrSections[i][indexToCompare])
			{
				nlapiLogExecution('Debug', '----newComp==arrSections-----');
				htmlBody+= '<tr><td id="'+ arrSections[i][1] +'"><span onclick="dispSched(this);">'+arrSections[i][2]+'</span></td>';
				htmlBody+= '<td><input tabindex="-1" type="checkbox" id="checkbx_'+arrSections[i][indexToCompare]+'_'+i+'"/></td>';
				htmlBody+= '<td><select tabindex="-1" id="listFac_'+arrSections[i][indexToCompare]+'_'+i+'">'+getFaculties(srchAuthFac,arrRoomPerYearTerm,arrSections[i][1])+'</select>';
				htmlBody+= '<span class="uir-field-widget"><a data-helperbuttontype="open" href="#" id="facavail_popup_link'+i+'" class="smalltextnolink field_widget i_options" tabindex="-1" title="Open" style="visibility: visible !important;" onclick="openFacAv(this);"></a></span></td>';
				htmlBody+= '<td><select tabindex="-1" id="listBld_'+arrSections[i][indexToCompare]+'_'+i+'">'+getBuildings(arrRoomPerYearTerm,arrSections[i][1])+'</select></td>';
				htmlBody+= '<td><select tabindex="-1" id="listRmType_'+arrSections[i][indexToCompare]+'_'+i+'">'+getRoomsType(arrAllRooms)+'</select></td>';
				htmlBody+= '<td><select tabindex="-1" id="listRm_'+arrSections[i][indexToCompare]+'_'+i+'">'+getRooms(arrAllRooms,arrRoomPerYearTerm,arrSections[i][1])+'</select></td>';
				htmlBody+= '<td></td><td></td></tr>';
			}
			else
			{
				if(newComp!=0)
					htmlBody+= '</tbody></table></div>';
				newComp=arrSections[i][indexToCompare];
				nlapiLogExecution('Debug', '<----newComp=arrSections----->');
				htmlBody+= '<div class="slide" style="width:1100px;">';
				htmlBody+= '<table id="facClass'+arrSections[i][indexTitle]+'" class="courses" style="width:1100px;">';
				htmlBody+= '<caption>'+arrSections[i][indexTitle]+'</caption><thead><tr>';
				htmlBody+= '<td style="min-width:120px;">Section</td><td width="110px">Show Available Faculty</td><td width="200px">Faculty</td><td>Building</td><td>Room Type</td><td>Room</td><td>Room Capacity</td><td>Max Student Count</td>';
				htmlBody+= '</tr></thead><tbody>';
				htmlBody+= '<tr><td id="'+ arrSections[i][1] +'"><span onclick="dispSched(this);">'+arrSections[i][2]+'</span></td>';
				htmlBody+= '<td><input tabindex="-1" type="checkbox" id="checkbx_'+arrSections[i][indexToCompare]+'_'+i+'"/></td>';
				htmlBody+= '<td><select tabindex="-1" id="listFac_'+arrSections[i][indexToCompare]+'_'+i+'">'+getFaculties(srchAuthFac,arrRoomPerYearTerm,arrSections[i][1])+'</select>';
				htmlBody+= '<span class="uir-field-widget"><a data-helperbuttontype="open" href="#" id="facavail_popup_link'+i+'" class="smalltextnolink field_widget i_options" tabindex="-1" title="Open" style="visibility: visible !important;" onclick="openFacAv(this);"></a></span></td>';
				htmlBody+= '<td><select tabindex="-1" id="listBld_'+arrSections[i][indexToCompare]+'_'+i+'">'+getBuildings(arrRoomPerYearTerm,arrSections[i][1])+'</select></td>';
				htmlBody+= '<td><select tabindex="-1" id="listRmType_'+arrSections[i][indexToCompare]+'_'+i+'">'+getRoomsType(arrAllRooms)+'</select></td>';
				htmlBody+= '<td><select tabindex="-1" id="listRm_'+arrSections[i][indexToCompare]+'_'+i+'">'+getRooms(arrAllRooms,arrRoomPerYearTerm,arrSections[i][1])+'</select></td>';
				htmlBody+= '<td></td><td></td></tr>';
			}


		}
	}
	else
	{

		//Get section Plan and course sections for the acad year and term to plan
		var filter=new Array();
		filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_course', null, 'is', request.getParameter('course'));
		filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', request.getParameter('termP'));
		filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', request.getParameter('yearP'));
		filter[3]= new nlobjSearchFilter('isinactive',null,'is','F');
		filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',getCampus());
		if(oneWorld)
			filter[5]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',getSubsidiary());
		var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, null);

		arrCourseSecP=getCourseSecfromSecP(results[0].getId(),request.getParameter('course'));
                if(arrCourseSecP==null || arrCourseSecP.length==0)
			throw nlapiCreateError('Notice', 'No sections found under the section plan.');

		arrCourseSecP.sort(function(a, b)
				{
			if(a[7]=='T')
			{
				return -1;
			}
			if(parseInt(a[0]) == parseInt(b[0]))
			{
				var x = parseInt(a[1]), y = parseInt(b[1]);
				return x == y ? 0 : (x < y ? -1 : 1);
			}
			return parseInt(a[0]) < parseInt(b[0]) ? -1 : 1;

				});
		var arrSectionsH=getComponentInSecP(request.getParameter('course'),request.getParameter('termH'),request.getParameter('yearH'));
		arrSectionsH.sort(); 
		var arrSectionsP=getComponentInSecP(request.getParameter('course'),request.getParameter('termP'),request.getParameter('yearP'));
		arrSectionsP.sort();
		var diff=0,compt=0,occurenceH=0,occurenceP=0;

		//Hist Course Scheduling
		var arrHistCS=new Array();
		var srchResults = getCourseSched_P(method,request,'hist');
		if(srchResults !=null && srchResults != '')
		{
			for(var cs=0; cs<srchResults.length; cs++)
			{
				var room=srchResults[cs].getValue('custrecord_ederp_schedule_room');
				var roomNm=srchResults[cs].getText('custrecord_ederp_schedule_room');
				var building=srchResults[cs].getValue('custrecord_ederp_schedule_building');
				var buildingNm=srchResults[cs].getText('custrecord_ederp_schedule_building');
				var faculty=srchResults[cs].getValue('custrecord_ederp_schedule_faculty');
				var facultyNm='';
				if(!isEmpty(faculty))
				{
					var fields=['firstname','lastname'];
					facultyNm=nlapiLookupField('customer', faculty,fields);
					facultyNm=facultyNm.firstname+' '+facultyNm.lastname;
				}
				var section=srchResults[cs].getValue('custrecord_ederp_schedule_section');
				var sectionNm=srchResults[cs].getText('custrecord_ederp_schedule_section');
				arrHistCS.push([section,sectionNm,room,building,faculty,roomNm,buildingNm,facultyNm]);
			}

			nlapiLogExecution('Debug','arrHistCS',arrHistCS);
		}
		else
		{
			throw nlapiCreateError('Notice','No approved historical schedule found.',true);
		}

		for(var i=0 ; i<arrSections.length; i++)
		{
			if(newComp==arrSections[i][0])
			{

				if(diff<0 && compt>=(occurenceH+diff))
				{
					var indexFacInHist=getfirstIndexOf(arrHistCS,arrSections[i][2],1);
					var roomDet=new Array();
					roomDet=getRoomDetails(arrHistCS[indexFacInHist][2],arrSections[i][1]);
					if(roomDet.length==0)
						roomDet[0]="   ";//In case bad practice of using a hist schedule having no room

					nlapiLogExecution('Debug','roomDet ',roomDet);
					htmlBody+= '<tr><td id="'+ arrSections[i][1] +'remove">'+arrSections[i][2]+'</td>';
					htmlBody+= '<td><input tabindex="-1" type="checkbox" disabled/></td>';
					htmlBody+= '<td>'+arrHistCS[indexFacInHist][7]+'</td>';
					htmlBody+= '<td>'+arrHistCS[indexFacInHist][6]+'</td>';
					htmlBody+= '<td>'+roomDet[0][0]+'</td>';
					htmlBody+= '<td>'+arrHistCS[indexFacInHist][5]+'</td>';
					htmlBody+= '<td>'+roomDet[0][1]+'</td><td>'+roomDet[0][2]+'</td></tr>';
				}
				else if(diff>0 && compt==(occurenceH-1))
				{
					var indexInCurrentPlan=getfirstIndexOf(arrCourseSecP,arrSections[i][2],2);
					var indexFacInHist=getfirstIndexOf(arrHistCS,arrSections[i][2],1);
					htmlBody+= '<tr><td id="'+ arrCourseSecP[indexInCurrentPlan][1] +'"><span onclick="dispSched(this);">'+arrSections[i][2]+'</span></td>';
					htmlBody+= '<td><input tabindex="-1" type="checkbox" id="checkbx_'+arrSections[i][3]+'_'+i+'"/></td>';
					htmlBody+= '<td><select tabindex="-1" id="listFac_'+arrSections[i][3]+'_'+i+'">'+getFaculties(srchAuthFac,arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][4])+'</select>';
					htmlBody+= '<span class="uir-field-widget"><a data-helperbuttontype="open" href="#" id="facavail_popup_link'+i+'" class="smalltextnolink field_widget i_options" tabindex="-1" title="Open" style="visibility: visible !important;" onclick="openFacAv(this);"></a></span></td>';
					htmlBody+= '<td><select tabindex="-1" id="listBld_'+arrSections[i][3]+'_'+i+'">'+getBuildings(arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][3])+'</select></td>';
					htmlBody+= '<td><select tabindex="-1" id="listRmType_'+arrSections[i][3]+'_'+i+'">'+getRoomsType(arrAllRooms)+'</select></td>';
					htmlBody+= '<td><select tabindex="-1" id="listRm_'+arrSections[i][3]+'_'+i+'">'+getRooms(arrAllRooms,arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][2])+'</select></td>';
					htmlBody+= '<td></td><td></td></tr>';

					var start=occurenceP+getfirstIndexOf(arrCourseSecP,arrSections[i][0],0);

					for(var w=start-diff; w<=start-1; w++)
					{

						htmlBody+= '<tr><td id="'+ arrCourseSecP[w][1] +'"><span onclick="dispSched(this);">'+arrCourseSecP[w][2]+'</span></td>';
						htmlBody+= '<td><input tabindex="-1" type="checkbox" id="checkbx_'+arrSections[i][3]+'_'+w+'"/></td>';
						htmlBody+= '<td><select tabindex="-1" id="listFac_'+arrSections[i][3]+'_'+w+'">'+getFaculties(srchAuthFac,arrRoomPerYearTerm,arrCourseSecP[w][1])+'</select>';
						htmlBody+= '<span class="uir-field-widget"><a data-helperbuttontype="open" href="#" id="facavail_popup_link'+w+'" class="smalltextnolink field_widget i_options" tabindex="-1" title="Open" style="visibility: visible !important;" onclick="openFacAv(this);"></a></span></td>';
						htmlBody+= '<td><select tabindex="-1" id="listBld_'+arrSections[i][3]+'_'+w+'">'+getBuildings(arrRoomPerYearTerm,arrCourseSecP[w][1])+'</select></td>';
						htmlBody+= '<td><select tabindex="-1" id="listRmType_'+arrSections[i][3]+'_'+w+'">'+getRoomsType(arrAllRooms)+'</select></td>';
						htmlBody+= '<td><select tabindex="-1" id="listRm_'+arrSections[i][3]+'_'+w+'">'+getRooms(arrAllRooms,arrRoomPerYearTerm,arrCourseSecP[w][1])+'</select></td>';
						htmlBody+= '<td></td><td></td></tr>';
					}
				}
				else
				{
					var indexInCurrentPlan=getfirstIndexOf(arrCourseSecP,arrSections[i][2],2);
					var indexFacInHist=getfirstIndexOf(arrHistCS,arrSections[i][2],1);
					htmlBody+= '<tr><td id="'+ arrCourseSecP[indexInCurrentPlan][1] +'"><span onclick="dispSched(this);">'+arrSections[i][2]+'</span></td>';
					htmlBody+= '<td><input tabindex="-1" type="checkbox" id="checkbx_'+arrSections[i][3]+'_'+i+'"/></td>';
					htmlBody+= '<td><select tabindex="-1" id="listFac_'+arrSections[i][3]+'_'+i+'">'+getFaculties(srchAuthFac,arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][4])+'</select>';
					htmlBody+= '<span class="uir-field-widget"><a data-helperbuttontype="open" href="#" id="facavail_popup_link'+i+'" class="smalltextnolink field_widget i_options" tabindex="-1" title="Open" style="visibility: visible !important;" onclick="openFacAv(this);"></a></span></td>';
					htmlBody+= '<td><select tabindex="-1" id="listBld_'+arrSections[i][3]+'_'+i+'">'+getBuildings(arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][3])+'</select></td>';
					htmlBody+= '<td><select tabindex="-1" id="listRmType_'+arrSections[i][3]+'_'+i+'">'+getRoomsType(arrAllRooms)+'</select></td>';
					htmlBody+= '<td><select tabindex="-1" id="listRm_'+arrSections[i][3]+'_'+i+'">'+getRooms(arrAllRooms,arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][2])+'</select></td>';
					htmlBody+= '<td></td><td></td></tr>';
					compt++;
				}
			}
			else
			{
				if(newComp!=0)
					htmlBody+= '</tbody></table></div>';
				newComp=arrSections[i][0];
				compt=0;
				occurenceH=getOccurrences(arrSectionsH,arrSections[i][0]);
				occurenceP=getOccurrences(arrSectionsP,arrSections[i][0]);
				diff=occurenceP-occurenceH;
				var indexInCurrentPlan=getfirstIndexOf(arrCourseSecP,arrSections[i][2],2);
				var indexFacInHist=getfirstIndexOf(arrHistCS,arrSections[i][2],1);
				htmlBody+= '<div class="slide" style="width:1100px;">';
				htmlBody+= '<table id="facClass'+arrSections[i][3]+'" class="courses" style="width:1100px;">';
				htmlBody+= '<caption>'+arrSections[i][3]+'</caption><thead><tr>';
				htmlBody+= '<td style="min-width:120px;">Section</td><td width="110px">Show Available Faculty</td><td width="200px">Faculty</td><td>Building</td><td>Room Type</td><td>Room</td><td>Room Capacity</td><td>Max Student Count</td>';
				htmlBody+= '</tr></thead><tbody>';
				htmlBody+= '<tr><td id="'+ arrCourseSecP[indexInCurrentPlan][1] +'"><span onclick="dispSched(this);">'+arrSections[i][2]+'</span></td>';
				htmlBody+= '<td><input tabindex="-1" type="checkbox" id="checkbx_'+arrSections[i][3]+'_'+i+'"/></td>';
				htmlBody+= '<td><select tabindex="-1" id="listFac_'+arrSections[i][3]+'_'+i+'">'+getFaculties(srchAuthFac,arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][4])+'</select>';
				htmlBody+= '<span class="uir-field-widget"><a data-helperbuttontype="open" href="#" id="facavail_popup_link'+i+'" class="smalltextnolink field_widget i_options" tabindex="-1" title="Open" style="visibility: visible !important;" onclick="openFacAv(this);"></a></span></td>';
				htmlBody+= '<td><select tabindex="-1" id="listBld_'+arrSections[i][3]+'_'+i+'">'+getBuildings(arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][3])+'</select></td>';
				htmlBody+= '<td><select tabindex="-1" id="listRmType_'+arrSections[i][3]+'_'+i+'">'+getRoomsType(arrAllRooms)+'</select></td>';
				htmlBody+= '<td><select tabindex="-1" id="listRm_'+arrSections[i][3]+'_'+i+'">'+getRooms(arrAllRooms,arrRoomPerYearTerm,arrCourseSecP[indexInCurrentPlan][1],arrHistCS[indexFacInHist][2])+'</select></td>';
				htmlBody+= '<td></td><td></td></tr>';
				compt++;
			}

		}

	}
	htmlBody+= '</tbody></table></div>';
	htmlBody+="</div>";
	nlapiLogExecution('Debug', 'done arrRoomPerYearTerm',arrRoomPerYearTerm);

	return htmlBody;
}

/**----RETURN LIST OPTIONS OF FACULTY----*/
function getFaculties()
{

	var histFac;
	var results=arguments[0];
	var arrayAllData=arguments[1];
	var section=arguments[2];
        nlapiLogExecution('Debug', 'faculty results =  ',results);
	nlapiLogExecution('Debug', 'faculty arrayAllData =  ',arrayAllData);
	nlapiLogExecution('Debug', 'faculty section =  ',section);
	if (arguments.length == 4)
		histFac=arguments[3];
	var posSecInBkdRooms=getSecPositions(arrayAllData,section);
	nlapiLogExecution('Debug', 'faculty posSecInBkdRooms =  ',posSecInBkdRooms);
	var options='<option value="">--TBD--</option>';
	if(results!=null)
	{
		for(var i=0; i<results.length; i++)
			if(arrayAllData[posSecInBkdRooms[0]][3]==results[i].getValue('internalid') || histFac==results[i].getId())
				options+='<option value="'+results[i].getValue('internalid')+'" selected>'+results[i].getValue('firstname')+' '+results[i].getValue('lastname')+'</option>'; 
			else
				options+='<option value="'+results[i].getValue('internalid')+'">'+results[i].getValue('firstname')+' '+results[i].getValue('lastname')+'</option>'; 

	}
	nlapiLogExecution('Debug', 'faculty option =  ',options);
	return options;
}

/**----RETURN LIST OF BLDNGS IN CURRENT SUBS----*/
function getBuildings_Subs()
{
	nlapiLogExecution('Debug', '----getBuildings_Subs----');
	var subsid = getSubsidiary();
	nlapiLogExecution('Debug', 'subsid get buildings',subsid);
	var campus = getCampus();
	nlapiLogExecution('Debug', 'getCampus',campus);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('name').setSort();
	var filters=new Array()	;
	filters[0]= new nlobjSearchFilter('isinactive',null,'is','F');
	filters[1]= new nlobjSearchFilter('custrecord_ederp_building_campus',null, 'is',campus);
	if(oneWorld)
		filters[2]= new nlobjSearchFilter('custrecord_ederp_building_subsid',null, 'is',subsid);
	var results = nlapiSearchRecord('customrecord_ederp_building', null, filters, columns);
	nlapiLogExecution('Debug', 'results_getbuildings',results);
	return results;
}

/**----RETURN LIST OPTIONS OF BLDNGS----*/
function getBuildings()
{
	nlapiLogExecution('Debug', '----getBuildings----');
	var histBldg;
	var arrayAllData=arguments[0];
	var section=arguments[1];
	if(arguments.length==3)
		histBldg=arguments[2];


	var posSecInBkdRooms=getSecPositions(arrayAllData,section);
	var options='<option value="">--TBD--</option>';
	var results = listBldngs;
	if(results ==null || results == '')
		options='<option>No Building found</option>';
	else
	{
		for(var i=0; i<results.length; i++)
			if(arrayAllData[posSecInBkdRooms[0]][2]==results[i].getValue('internalid') || histBldg==results[i].getValue('internalid'))
				options+='<option value="'+results[i].getValue('internalid')+'" selected>'+results[i].getValue('name')+'</option>'; 
			else
				options+='<option value="'+results[i].getValue('internalid')+'">'+results[i].getValue('name')+'</option>';
	}
	nlapiLogExecution('Debug', 'building option =  ',options);
	return options;
}

/**----RETURN POSITION OF SECTIONS----*/
function getSecPositions(arr,val)
{
	var positions=new Array();
	for(var i = 0; i < arr.length; i++) 
		if(val == arr[i][0]) 
			positions.push(i);

	return positions;
}

/**----RETURN LIST OPTIONS OF ROOMS----*/
function getRooms()
{
	var histRoom;
	var arrRooms=arguments[0];
	var arrBookedRooms=arguments[1];
	var section=arguments[2];
	if(arguments.length==4)
		histRoom=arguments[3];
	var posSecInBkdRooms=getSecPositions(arrBookedRooms,section);
	var options='<option value="" id="">--TBD--</option>';
	for(var r=0; r<arrRooms.length; r++)
		if(arrRooms[r][1]==arrBookedRooms[posSecInBkdRooms[0]][1] || arrRooms[r][1]==histRoom)
			options+='<option value="'+arrRooms[r][0]+'_'+arrRooms[r][3]+'" id="'+arrRooms[r][1]+'" selected>'+arrRooms[r][2]+'</option>'; 
		else
			options+='<option value="'+arrRooms[r][0]+'_'+arrRooms[r][3]+'" id="'+arrRooms[r][1]+'">'+arrRooms[r][2]+'</option>'; 
	nlapiLogExecution('Debug', 'rooms option =  ',options);
	return options;
}

/**----CHECK IF TIMINGS ARE OVERLAPPING----*/
function isOverlapping(myArray,day,stime,etime)
{    
	for(var i=0; i<myArray.length; i++)
	{  

		var dayGet=parseInt(myArray[i][0]);
		var stimeGet=parseInt(myArray[i][1]);
		var etimeGet=parseInt(myArray[i][2]);
		if(dayGet == parseInt(day) && ((stimeGet<=parseInt(stime) && parseInt(stime)<etimeGet) || (parseInt(stime)<stimeGet && parseInt(etime)>stimeGet) )) 
			return true;
	}
	return false;
}

/**----SAVE COURSE SCHEDULE RECORDS AND CHECK USAGE LIMIT----*/
function saveData(request,response)
{
	
	var arrProcessedSections=new Array();
	var arrRemove=new Array();

	var arrCourseSchedTime=request.getParameter('arr').split(',');
	var all_arr = request.getParameter('arr');
	var course=request.getParameter('course');	
	var method=request.getParameter('method');
	if(method=='scratch')
	{
		var term=request.getParameter('term');
		var year=request.getParameter('year');
	}
	else
	{
		var term=request.getParameter('termP');
		var year=request.getParameter('yearP');
	}


	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is',term);
	filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', year);
	filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_course', null, 'is',course);
	filter[3]= new nlobjSearchFilter('isinactive',null,'is','F');
	filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',getCampus());
	if(oneWorld)
		filter[5]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',getSubsidiary());
	var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, null);
	if(results ==null || results == '')
	{
		throw nlapiCreateError('Notice','Section Plan under the criteria selected has been removed or inactivated.',true);

	}
	else
	{
		if(request.getParameter('arrRemv')!=null)
		{
          nlapiLogExecution('Debug', 'course', request.getParameter('course'));
			var file = nlapiLoadFile(request.getParameter('f'));
				
			var content = JSON.parse(file.getValue());
			arrProcessedSections=content['arrRemv'].split(',');
			arrRemove=content['arrRemv'].split(',');
			arrCourseSchedTime=content['arr'].split(',');
			all_arr = content['arr'];
		}
		//nlapiLogExecution('Debug', 'in saving data',course);
		var objSecCourse=nlapiLoadRecord('customrecord_ederp_secplan', results[0].getId());
		//nlapiLogExecution('Debug', 'objSecCourse.getLineItemCount',objSecCourse.getLineItemCount('recmachcustrecord_ederp_sec_link'));
		for(var i=0;i<objSecCourse.getLineItemCount('recmachcustrecord_ederp_sec_link');i++)
		{
			var secId=objSecCourse.getLineItemValue('recmachcustrecord_ederp_sec_link', 'id', i+1);
			if(arrRemove.indexOf(secId)<0 && arrProcessedSections.indexOf(secId)<0)
			{
		          nlapiLogExecution('Debug', 'arrCourseSchedTime',arrCourseSchedTime);
			  nlapiLogExecution('Debug', 'secId',secId);
				
                            var arrCsTimings=getAllPos(arrCourseSchedTime,secId);
			    nlapiLogExecution('Debug', 'arrCsTimings',arrCsTimings);
				
				if(arrCsTimings.length!=0)
				{

					//First take sec id , search with all criteria for Course Schedule if exists
					filter[0]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is',term);
					filter[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', year);
					filter[2]= new nlobjSearchFilter('custrecord_ederp_schedule_course', null, 'is',course);  
					filter[3]= new nlobjSearchFilter('custrecord_ederp_schedule_section', null, 'is', secId);
					filter[4]= new nlobjSearchFilter('isinactive',null,'is','F');
					if(oneWorld)
						filter[5]= new nlobjSearchFilter('custrecord_ederp_schedule_subsid',null, 'is',getSubsidiary());
					filter.push(new nlobjSearchFilter('custrecord_ederp_schedule_campus',null, 'is',getCampus()));
					var srchCourseSched = nlapiSearchRecord('customrecord_ederp_schedule', null, filter, null);
					nlapiLogExecution('Debug', 'usage',nlapiGetContext().getRemainingUsage());
					if ((nlapiGetContext().getRemainingUsage() < USAGE_LIMIT_THRESHOLD))
					{ 

						nlapiLogExecution('Debug', 'usage',nlapiGetContext().getRemainingUsage());
						var param = new Array();
						param['action']='saving';
						param['mode']=base64_encode(request.getParameter('mode'));
						param['method']=base64_encode(method);
						param['course']= base64_encode(course);
						param['arr']=[];
						param['arrRemv']=[""];
						if(method=='scratch')
						{
							param['year']=base64_encode(year);
							param['term']=base64_encode(term);
						}
						else
						{
							param['yearP']=base64_encode(year);
							param['termP']=base64_encode(term);
							param['yearH']=base64_encode(request.getParameter('yearH'));
							param['termH']=base64_encode(request.getParameter('termH'));
						}
						//CREATE FILE in the folder
						var file = sftpiCreateScheduleFile(all_arr, arrProcessedSections.toString(), course);
                        //nlapiLogExecution('Debug', 'file to send', file.toString() );						
                        param['f']= file.toString();
                       //nlapiLogExecution('Debug', 'to send', param);	
						nlapiSetRedirectURL('SUITELET','customscript_ederph_st_coursesched', 'customdeploy_ederph_st_coursesched', null, param);
						return 'abort';

					}
					else
					{
						//update course schedule: status to draft + name convention again
						var name=objSecCourse.getLineItemValue('recmachcustrecord_ederp_sec_link', 'name', i+1)+'-';
						name+=nlapiLookupField('customrecord_ederp_acadyear', year,'custrecord_ederp_acadyear_abbr')+'-';
						name+=nlapiLookupField('customrecord_ederp_acadterm', term,'custrecord_ederp_acadterm_abbr');
						nlapiLogExecution('Debug', 'name',name);

						//if yes it means an update else means creation
						if(srchCourseSched !=null && srchCourseSched != '' && srchCourseSched != 'undefined' && srchCourseSched != undefined)
						{
							var obj=nlapiLoadRecord('customrecord_ederp_schedule',srchCourseSched[0].getId());
							obj.setFieldValue('custrecord_ederp_schedule_status',4);
							obj.setFieldValue('name', name);
							nlapiSubmitRecord(obj, true);

							//update status in sec plan also
							nlapiSubmitField('customrecord_ederp_secplan',parseInt(results[0].getId()), 'custrecord_ederp_secplan_schedstatus', 4);

							nlapiLogExecution('Debug', 'arrCsTimings length = ',arrCsTimings.length);
							var schTime = false;
							//if update srch for cstime with filter cs and day , stime and etime
							for(var j=0; j<arrCsTimings.length; j++)
							{
								var day=arrCourseSchedTime[arrCsTimings[j]].split(':')[1];
								var stime=arrCourseSchedTime[arrCsTimings[j]].split(':')[2];
								var etime=arrCourseSchedTime[arrCsTimings[j]].split(':')[3];

								day=day.charAt(0).toUpperCase()+day.substring(1,day.length);
								day=getIds('customrecord_ederp_classday','name',day);
								stime=getIds('customrecord_ederp_timeslot','custrecord_ederp_timeslot_value',''+stime.substring(0,2)+':'+stime.substring(2,4));
								etime=getIds('customrecord_ederp_timeslot','custrecord_ederp_timeslot_value',''+etime.substring(0,2)+':'+etime.substring(2,4));

								var cstFilter=new Array();
								cstFilter.push(new nlobjSearchFilter('custrecord_ederp_schedtime_schedlink', null, 'is', srchCourseSched[0].getId()));
								cstFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
								var srchCourseSchedTime=nlapiSearchRecord('customrecord_ederp_schedtime', null, cstFilter, null);
								nlapiLogExecution('Debug', 'j',j);
						
								nlapiLogExecution('Debug', 'schedule time id',srchCourseSchedTime);
						                
                                                                //load it, set values and submit the record
								if((srchCourseSchedTime!=null && srchCourseSchedTime!='' && srchCourseSchedTime!='undefined' && srchCourseSchedTime!=undefined && srchCourseSchedTime!='null' && srchCourseSchedTime!=' ') && schTime == false)
								{
									var obj=nlapiLoadRecord('customrecord_ederp_schedtime',srchCourseSchedTime[j].getId());
									obj.setFieldValue('custrecord_ederp_schedtime_day',day);
									obj.setFieldValue('custrecord_ederp_schedtime_start', stime);
									obj.setFieldValue('custrecord_ederp_schedtime_end',etime);
									nlapiSubmitRecord(obj, true);
								}
								else//Added on 20-5-16 by Tapan,if new class is added then Course Schedule time should be created for that rather update it
								{
									//create cs time record
									var cstRecord = nlapiCreateRecord('customrecord_ederp_schedtime');
									cstRecord.setFieldValue('custrecord_ederp_schedtime_day', day);
									cstRecord.setFieldValue('custrecord_ederp_schedtime_start', stime);
									cstRecord.setFieldValue('custrecord_ederp_schedtime_end', etime);	
									cstRecord.setFieldValue('custrecord_ederp_schedtime_schedlink',srchCourseSched[0].getId());
									nlapiSubmitRecord(cstRecord, true);
									schTime = true;
								}
							} 
							arrProcessedSections.push(secId);
						}
						else
						{
							//means that schedules were created --in case of draft--
							var compId=objSecCourse.getLineItemValue('recmachcustrecord_ederp_sec_link', 'custrecord_ederp_sec_comp', i+1);
							compId=nlapiLoadRecord('customrecord_ederp_coursecomp', compId);
							
							//Check Primary Component for Checking Primary checkbox in Course Schedule (updated on 21-3-16 as requirement in grading)
							var primary = compId.getFieldValue('custrecord_ederp_coursecomp_primary');
							nlapiLogExecution('DEBUG','primary',primary);
							
							compId=compId.getFieldValue('custrecord_ederp_coursecomp_name');
							var max_occup=nlapiLookupField('customrecord_ederp_sec', secId, 'custrecord_ederp_sec_maxoccup');
							max_occup = isEmpty(max_occup)?0:parseInt(max_occup);
							var overEnroll = nlapiLookupField('serviceitem', course, 'custitem_ederp_overenroll');
							overEnroll = isEmpty(overEnroll)?0:parseInt(overEnroll);
							overEnroll += max_occup;
							
							//Creation of Course Schedule
							var record = nlapiCreateRecord('customrecord_ederp_schedule');
							record.setFieldValue('name',name);
							record.setFieldValue('custrecord_ederp_schedule_acadyear', year);
							record.setFieldValue('custrecord_ederp_schedule_course', course);
							record.setFieldValue('custrecord_ederp_schedule_acadterm', term);
							if(oneWorld)
								record.setFieldValue('custrecord_ederp_schedule_subsid', getSubsidiary());
							record.setFieldValue('custrecord_ederp_schedule_campus', getCampus());
							record.setFieldValue('custrecord_ederp_schedule_coursecomp', compId);
							record.setFieldValue('custrecord_ederp_schedule_section',secId);	
							record.setFieldValue('custrecord_ederp_schedule_maxoccup',max_occup);
							record.setFieldValue('custrecord_ederp_schedule_availslots',overEnroll);
							record.setFieldValue('custrecord_ederp_schedule_status',4);
							record.setFieldValue('custrecord_ederp_schedule_secplan',results[0].getId());
							if(primary == 'T')
								record.setFieldValue('custrecord_ederp_schedule_primary','T');
							
							var	id = nlapiSubmitRecord(record, true);	
							//update status in sec plan also
							nlapiSubmitField('customrecord_ederp_secplan',parseInt(results[0].getId()), 'custrecord_ederp_secplan_schedstatus', 4);

							arrProcessedSections.push(secId);
							for(var j=0; j<arrCsTimings.length; j++)
							{
								var day=arrCourseSchedTime[arrCsTimings[j]].split(':')[1];
								var stime=arrCourseSchedTime[arrCsTimings[j]].split(':')[2];
								var etime=arrCourseSchedTime[arrCsTimings[j]].split(':')[3];

								day=day.charAt(0).toUpperCase()+day.substring(1,day.length);
								day=getIds('customrecord_ederp_classday','name',day);
								stime=getIds('customrecord_ederp_timeslot','custrecord_ederp_timeslot_value',''+stime.substring(0,2)+':'+stime.substring(2,4));
								etime=getIds('customrecord_ederp_timeslot','custrecord_ederp_timeslot_value',''+etime.substring(0,2)+':'+etime.substring(2,4));
								//create cs time record
								var record = nlapiCreateRecord('customrecord_ederp_schedtime');
								record.setFieldValue('custrecord_ederp_schedtime_day', day);
								record.setFieldValue('custrecord_ederp_schedtime_start', stime);
								record.setFieldValue('custrecord_ederp_schedtime_end', etime);	
								record.setFieldValue('custrecord_ederp_schedtime_schedlink',id);
								nlapiSubmitRecord(record, true);

							}
						}

					}
				}

			}


		}
	}

}

function sftpiCreateScheduleFile(arrData, arrProcessedData, course)
{
	var nameFile = 'Sch_'+course+'_'+nlapiGetContext().getUser()+'.txt';
         nlapiLogExecution('Debug', 'nameFile', nameFile );
	var JsonObject = {arr : arrData, arrRemv : arrProcessedData};
	var folder = nlapiGetContext().getSetting('SCRIPT', 'custscript_schedule_folder');
	var newFile = nlapiCreateFile(nameFile, 'PLAINTEXT', JSON.stringify(JsonObject));
	    newFile.setFolder(folder);
	nlapiLogExecution('Debug', 'folder ',folder );	
        var submitFile = nlapiSubmitFile(newFile);
        nlapiLogExecution('Debug', 'submitFile',submitFile);	
	return submitFile;
}

/**----RETURNS LIST OF PROGRAMS UNDER CURRENT SUBS----*/
function getPrograms(request)
{
	var listPrograms=new Array();
	var course=request.getParameter('course');	
	var term=request.getParameter('term');
	if(isEmpty(term))
		term=request.getParameter('termP');
	else if(term.indexOf(':')>=0)
		term=request.getParameter('term').split(':')[0];
	nlapiLogExecution('Debug', 'term',term);
	var year=request.getParameter('year');
	if(isEmpty(year))
		year=request.getParameter('yearP');
	else if(year.indexOf(':')>=0)
		year=request.getParameter('year').split(':')[0];

	var col= new nlobjSearchColumn('custrecord_ederp_secplan_proglink');
	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_course', null, 'is', course);
	
	filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', year);
	filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_status', null, 'is', 2);
	filter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',getCampus());
	if(oneWorld)
		filter[5]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',getSubsidiary());
	if(term!=null && term!='' && term!='null')
		filter.push(new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', term));

	var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, col);
	if(results!=null && results!='')
	{
		for(var i=0; i<results.length; i++)
			listPrograms.push(isEmpty(results[i].getText('custrecord_ederp_secplan_proglink'))?'Created From Scratch':results[i].getText('custrecord_ederp_secplan_proglink'));
	}
	return listPrograms;
}

/**----RETURNS COURSES FROM PROGRAM COURSES----*/
function getCourses(dep,subs,request)
{

var arrCourses=new Array();	
	//var searchCourses = sftpiSrchAllReference(-999,dep.split(','),subs,getCampus(),parseInt(request.getParameter('term')));
	var searchCourses = getSectionPlanCourses(-999,dep,subs,getCampus(),request.getParameter('term'));
	searchCourses.sort(function(a, b)
			{
				var x = a[1], y = b[1];
				return x == y ? 0 : (x < y ? -1 : 1);
		
			});
	
	
	var savedcourses = [];
	for(var i=0;i<searchCourses.length;i++)
	{   
		if(savedcourses.indexOf(searchCourses[i][1])<0)
		{
			arrCourses.push(searchCourses[i][1]+":"+searchCourses[i][0]);
			savedcourses.push(searchCourses[i][1]);
		}

	}
	
	//nlapiLogExecution('Debug', 'arrCourses',arrCourses);
	return arrCourses;

}
var courses = [];
function getSectionPlanCourses(index, dept, subs, campus,term)
{
	var col= new Array();
	col.push(new nlobjSearchColumn('custrecord_ederp_secplan_course',null,'group'));
	col.push(new nlobjSearchColumn('internalid',null,'min'));
	var filter=new Array();
	filter[0]= new nlobjSearchFilter('department', 'custrecord_ederp_secplan_course', 'anyof', dept);
	filter[1]= new nlobjSearchFilter('subsidiary', 'custrecord_ederp_secplan_course', 'is', subs);
	filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_status', null, 'is', 2);
	filter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',campus);
	filter[5]= new nlobjSearchFilter('isinactive', 'custrecord_ederp_secplan_course', 'is', 'F');
	filter[6]= new nlobjSearchFilter('internalidnumber', null, 'greaterthan', index);
	if(oneWorld)
		filter[7]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',subs);
	if(term!=null && term!='' && term!=undefined && term!=NaN && term!='NaN' && term!='null')
		filter.push(new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', parseInt(term)));
	var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, col);
	if(results!=null && results!='')
	{
		for(var i=0; i<results.length; i++)
		{
			courses.push([results[i].getValue('custrecord_ederp_secplan_course',null,'group'),results[i].getText('custrecord_ederp_secplan_course',null,'group')]);
			//nlapiLogExecution('Debug', 'results in loop',results[i].getValue('internalid',null,'min'));
		}
			
		//nlapiLogExecution('Debug', 'results',courses);
		
		getSectionPlanCourses(results[i-1].getValue('internalid',null,'min'), dept, subs, campus,term);
	}
	return courses;
}
/**----RETURNS HTML COMPARISON BTWN HIST AND TO SCHEDULE DATA----*/
function getSummary(request)
{
	
	var html='';
	var course=request.getParameter('course');	
	var termP=request.getParameter('termP');
	var yearP=request.getParameter('yearP');
	var termH=request.getParameter('termH');
	var yearH=request.getParameter('yearH');

	//First check Existence of Section Plan
	nlapiLogExecution('Debug', 'sec plan',course+':'+termP+':'+yearP+':'+termH+':'+yearH);
	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_course', null, 'is', course);
	filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', termP);
	filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', yearP);
	filter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_status', null, 'is', 2);
	filter[5]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',getCampus());
	if(oneWorld)
		filter[6]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',getSubsidiary());
	
	var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, null);
	if(results ==null || results == '')
	{
		html='There is no approved course section plan found under selected academic year and term to schedule. Select different parameters to proceed.';
		return html;
	}
	else
	{
		//Check if Course Schedule already created 
		var srchfilterP=new Array();
		srchfilterP[0]= new nlobjSearchFilter('custrecord_ederp_schedule_course', null, 'is', course);
		srchfilterP[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', termP);
		srchfilterP[2]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', yearP);
		srchfilterP[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
		if(oneWorld)
			srchfilterP[4]= new nlobjSearchFilter('custrecord_ederp_schedule_subsid',null, 'is',getSubsidiary());
		srchfilterP.push(new nlobjSearchFilter('custrecord_ederp_schedule_campus',null, 'is',getCampus()));
		var srchCrseSchedP = nlapiSearchRecord('customrecord_ederp_schedule', null, srchfilterP, null);
		if(srchCrseSchedP !=null && srchCrseSchedP != '' && request.getParameter('goingbck')=='false')
		{
			html='There is already a course schedule created for selected academic year and term.';
			html+='\nTo update it, select "From Scratch" option in Step 1 of the process.';
			return html;
		}

		//Check Existence of Historical Course Schedule
		var srchfilterH=new Array();
		srchfilterH[0]= new nlobjSearchFilter('custrecord_ederp_schedule_course', null, 'is', course);
		srchfilterH[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', termH);
		srchfilterH[2]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', yearH);
		srchfilterH[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
		srchfilterH[4]= new nlobjSearchFilter('custrecord_ederp_schedule_status', null, 'is', 2);
		if(oneWorld)
			srchfilterH[5]= new nlobjSearchFilter('custrecord_ederp_schedule_subsid',null, 'is',getSubsidiary());
		srchfilterH.push(new nlobjSearchFilter('custrecord_ederp_schedule_campus',null, 'is',getCampus()));
		
		var srchCrseSchedH = nlapiSearchRecord('customrecord_ederp_schedule', null, srchfilterH, null);
		if(srchCrseSchedH ==null || srchCrseSchedH == '')
		{
			html='There is no approved course schedule found under selected historical academic year and term. Select different parameters to proceed.';
			return html;
		}

		//Otherwise
		//Get Components + idComponent from Courses
		var arrComp=[];
		var objCourse=nlapiLoadRecord('serviceitem',course);
		for(var i=0; i<objCourse.getLineItemCount('recmachcustrecord_ederp_coursecomp_courselink'); i++)
		{
			var compId=objCourse.getLineItemValue('recmachcustrecord_ederp_coursecomp_courselink', 'id', i+1);
			if(!isEmpty(compId))
			{
				var compName=objCourse.getLineItemText('recmachcustrecord_ederp_coursecomp_courselink', 'custrecord_ederp_coursecomp_name', i+1);
				var classesPerWeek=objCourse.getLineItemValue('recmachcustrecord_ederp_coursecomp_courselink', 'custrecord_ederp_coursecomp_classweek', i+1);
				arrComp.push([compId,compName,classesPerWeek]);
			}

		}

		//Get Number of Historical Sections from Section Plan
		var arrSectionsH=getComponentInSecP(course,termH,yearH);
		arrSectionsH.sort();

		//Get Number of Current Sections from Section Plan
		var arrSectionsP=getComponentInSecP(course,termP,yearP);
		arrSectionsP.sort();


		html="<table id='summary'><caption>Summary Table</caption><thead>";
		html+="<tr><td>Course Component</td><td>Classes per Week</td><td>Historical Schedule Sections</td>";
		html+="<td>Sections to Schedule</td><td>Sections to Add/Remove</td></tr></thead>";
		html+="<tbody>";
		for(var arr=0; arr<arrComp.length; arr++)	
		{
			var occurenceH=getOccurrences(arrSectionsH,arrComp[arr][0]);
			var occurenceP=getOccurrences(arrSectionsP,arrComp[arr][0]);
			var diff=occurenceP-occurenceH;
			html+="<tr><td>"+arrComp[arr][1]+"</td><td>"+arrComp[arr][2]+"</td><td>"+occurenceH+"</td><td>"+occurenceP+"</td>";
			if(diff<0)
				html+="<td class='neg'>"+diff+"</td></tr>";
			else if(diff>0)
				html+="<td class='pos'>"+diff+"</td></tr>";
			else
				html+="<td>"+diff+"</td></tr>";
		}
		html+="</tbody></table>";
	}

	return html;
}

/**----RETURNS ALL COURSE COMPONENT OF COURSE----*/
function getComponentInSecP(course,term,year)
{
        var col=new nlobjSearchColumn('custrecord_ederp_sec_comp');
	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_course', 'custrecord_ederp_sec_link', 'is', course);
	filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_term', 'custrecord_ederp_sec_link', 'is', term);
	filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_year', 'custrecord_ederp_sec_link', 'is', year);
	filter[3]= new nlobjSearchFilter('isinactive', 'custrecord_ederp_sec_link', 'is', 'F');
	filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_status', 'custrecord_ederp_sec_link', 'is',2);
	filter[5]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filter[6]= new nlobjSearchFilter('custrecord_ederp_secplan_campus','custrecord_ederp_sec_link', 'is',getCampus());
	if(oneWorld)
		filter[7]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary','custrecord_ederp_sec_link', 'is',getSubsidiary());
	
	var results = nlapiSearchRecord('customrecord_ederp_sec', null, filter, col);
	var arr=new Array();
	if(results!=null && results!='')
		for(var i=0;i<results.length;i++)
		{
			var compId=results[i].getValue('custrecord_ederp_sec_comp');
                        if(!isEmpty(compId))
				arr.push(compId);
		}
	return arr;
}

/**----RETURNS COURSE COMPONENT DETAILS----*/
function getCourseComponents(course,term)
{
    
	var arr=new Array();
	var cols=new Array();
	cols[0] = new nlobjSearchColumn('custrecord_ederp_coursecomp_name');
	cols[1] = new nlobjSearchColumn('custrecord_ederp_coursecomp_classweek');
	cols[2] = new nlobjSearchColumn('custrecord_ederp_coursecomp_duration');
	cols[3] = new nlobjSearchColumn('custrecord_ederp_coursecomp_primary');
	var filter=new Array();
	filter[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filter[1] = new nlobjSearchFilter('custrecord_ederp_coursecomp_courselink', null, 'is', course);
	if(term)
	filter[2] = new nlobjSearchFilter('custrecord_ederp_coursecomp_acdterm', null, 'anyof', term);
	var objCourseComp=nlapiSearchRecord('customrecord_ederp_coursecomp', null, filter, cols);
	
	if(objCourseComp!=null && objCourseComp!='')
	{
		for(var i=0; i<objCourseComp.length; i++)
		{
			var name=objCourseComp[i].getText('custrecord_ederp_coursecomp_name');
                        var classweek=objCourseComp[i].getValue('custrecord_ederp_coursecomp_classweek');
                        var duration=objCourseComp[i].getValue('custrecord_ederp_coursecomp_duration');
                        var primary=objCourseComp[i].getValue('custrecord_ederp_coursecomp_primary');
                        arr.push([objCourseComp[i].getId(),name,classweek,duration,primary]);
		}
	}
	else
	{
		throw nlapiCreateError('Notice', 'No course components for this course.',true);
	}
        nlapiLogExecution('Debug', 'listCourseComponents = ',arr);
	listCourseComponents=arr;	
}

/**----RETURNS SECTION PLAN DETAILS----*/
function getCourseSecfromSecP(secPlan,courseId)
{
	nlapiLogExecution('Debug', 'in get sec plan',''+secPlan);
	if(listCourseComponents==null)
		throw nlapiCreateError('Notice', 'List of course components empty.',true);

	var arr=new Array();
	var cols=new Array();
	cols[0] = new nlobjSearchColumn('internalid').setSort(true);
	cols[1] = new nlobjSearchColumn('custrecord_ederp_sec_comp');
	cols[2] = new nlobjSearchColumn('name');
	cols[3] = new nlobjSearchColumn('custrecord_ederp_sec_group');
	var filter=new Array();
	filter[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filter[1] = new nlobjSearchFilter('custrecord_ederp_sec_link', null, 'is', secPlan);
	filter[2] = new nlobjSearchFilter('custrecord_ederp_coursecomp_courselink','custrecord_ederp_sec_comp', 'is', courseId);
	filter[3] = new nlobjSearchFilter('isinactive','custrecord_ederp_sec_comp', 'is', 'F');
	var objSecCourse=nlapiSearchRecord('customrecord_ederp_sec', null, filter, cols);

	if(objSecCourse!=null && objSecCourse!='')
	{
		
		for(var i=0;i<objSecCourse.length;i++)
		{
			var secId=objSecCourse[i].getId();
			var compId=objSecCourse[i].getValue('custrecord_ederp_sec_comp');
			var sec=objSecCourse[i].getValue('name');
			if(!isEmpty(compId))
			{
				var objComp=getfirstIndexOf(listCourseComponents,compId,0);
				var comp=listCourseComponents[objComp][1];
				var classes=listCourseComponents[objComp][2];
				var duration=listCourseComponents[objComp][3];
				var primary=listCourseComponents[objComp][4];

				var secGrp=objSecCourse[i].getValue('custrecord_ederp_sec_group');
				var namesecGrp=objSecCourse[i].getText('custrecord_ederp_sec_group');

				arr.push([compId,secId,sec,comp,classes,duration,secGrp,primary,namesecGrp]);
			}

		}
	}

	if(arr.length==0)
		throw nlapiCreateError('Notice','No sections found for the section plan: '+secPlan,true);
	return arr;
}

/**----CHECKS AVAILABILITY OF FACULTY AND RETURNS BOOKED DETAILS----*/
function checkFacAvail(request)
{
	var section=request.getParameter('section');
	var arrFac=request.getParameter('faculties').split(',');
	var notAvailFaculties=new Array();
	var arrAvailButBooked=new Array();
	var sectionValues= new Array();

	//1.load course schedule + year n term + timings 
	var filter=new Array();
	filter[0]=new nlobjSearchFilter('custrecord_ederp_schedule_section', 'custrecord_ederp_schedtime_schedlink', 'is', section);
	filter[1]=new nlobjSearchFilter('isinactive', 'custrecord_ederp_schedtime_schedlink', 'is', 'F');
	filter[2]=new nlobjSearchFilter('custrecord_ederp_schedule_campus', 'custrecord_ederp_schedtime_schedlink', 'is', getCampus());
        var col=new Array();
	col[0]=new nlobjSearchColumn('custrecord_ederp_schedtime_day');
	col[1]=new nlobjSearchColumn('custrecord_ederp_schedtime_start');
	col[2]=new nlobjSearchColumn('custrecord_ederp_schedtime_end');
	col[3]=new nlobjSearchColumn('custrecord_ederp_schedule_acadyear','custrecord_ederp_schedtime_schedlink');
	col[4]=new nlobjSearchColumn('custrecord_ederp_schedule_acadterm','custrecord_ederp_schedtime_schedlink');
	col[5]=new nlobjSearchColumn('custrecord_ederp_schedtime_schedlink').setSort();
	var resultsCSched = nlapiSearchRecord('customrecord_ederp_schedtime', null, filter, col);
	var mySecTimings=new Array();
	var year, term;
	if(resultsCSched)
	{
		year=resultsCSched[0].getValue('custrecord_ederp_schedule_acadyear','custrecord_ederp_schedtime_schedlink');
		term=resultsCSched[0].getValue('custrecord_ederp_schedule_acadterm','custrecord_ederp_schedtime_schedlink');
		for(var t=0; t<resultsCSched.length; t++)
		{
			var d=resultsCSched[t].getValue('custrecord_ederp_schedtime_day');
			var st=resultsCSched[t].getValue('custrecord_ederp_schedtime_start');
			var et=resultsCSched[t].getValue('custrecord_ederp_schedtime_end');
			mySecTimings.push([d,st,et]);
		}
	}
	nlapiLogExecution('DEBUG','arrFac',arrFac.length);
	nlapiLogExecution('DEBUG','arrFac',arrFac);
	//nlapiLogExecution('DEBUG','mySecTimings',mySecTimings.length);
	for(var fac=0; fac<arrFac.length; fac++)
	{
		var objFaculty=nlapiLoadRecord('customer',arrFac[fac]);
		var usage = nlapiGetContext().getRemainingUsage();
		if(usage<50)
			nlapiLogExecution('DEBUG','usage',usage+'@@'+fac);	
                for(var i=0; i<mySecTimings.length; i++)
		{
			//See if the faculty is available in the university during this time
			//if Yes check if he is not teaching other course
			//If teaching go to another Faculty if Not add
			//if No look for another faculty
			var notAvDay=0;
			var totalDays=objFaculty.getLineItemCount('recmachcustrecord_ederp_facultyavail_faculty');
			for(var av=0; av<objFaculty.getLineItemCount('recmachcustrecord_ederp_facultyavail_faculty'); av++)
			{
				var day=objFaculty.getLineItemValues('recmachcustrecord_ederp_facultyavail_faculty', 'custrecord_ederp_facultyavail_days', av+1);
				var stime=objFaculty.getLineItemValue('recmachcustrecord_ederp_facultyavail_faculty', 'custrecord_ederp_facultyavail_fromtime', av+1);
				var etime=objFaculty.getLineItemValue('recmachcustrecord_ederp_facultyavail_faculty', 'custrecord_ederp_facultyavail_tilltime', av+1);
				//nlapiLogExecution('DEBUG','before',arrFac[fac]);
				if((day=='' || day==null) || (stime=='' || stime==null) || (etime=='' || etime==null))
					continue;
				for(var j=0; j<day.length;j++)
				{ 
			//nlapiLogExecution('DEBUG','after',arrFac[fac]);
					totalDays++;
					if(mySecTimings[i][0]==day[j] && !(parseInt(mySecTimings[i][1])>=parseInt(stime) && parseInt(mySecTimings[i][2])<=parseInt(etime)))
					{
						notAvDay++;
						//exit and go to another faculty
					}
					else if(mySecTimings[i][0]!=day[j])
						notAvDay++;
				}
				totalDays--;

			}
			if(notAvDay==totalDays)
			{
				notAvailFaculties.push(arrFac[fac]);
				i=mySecTimings.length;
			}
		}

		if(notAvailFaculties.indexOf(arrFac[fac])<0)
		{
			for(var cs=0; cs<objFaculty.getLineItemCount('recmachcustrecord_ederp_schedule_faculty'); cs++)
			{
				var lineSection=objFaculty.getLineItemValue('recmachcustrecord_ederp_schedule_faculty', 'custrecord_ederp_schedule_section', cs+1);
				var lineYear=objFaculty.getLineItemValue('recmachcustrecord_ederp_schedule_faculty', 'custrecord_ederp_schedule_acadyear', cs+1);
				var lineTerm=objFaculty.getLineItemValue('recmachcustrecord_ederp_schedule_faculty', 'custrecord_ederp_schedule_acadterm', cs+1);

				if(section!=lineSection && year==lineYear && term==lineTerm)
				{
					var idCSched=objFaculty.getLineItemValue('recmachcustrecord_ederp_schedule_faculty', 'id', cs+1);
					var objCSched=nlapiLoadRecord('customrecord_ederp_schedule',idCSched);
					if(objCSched.getFieldValue('isinactive') == 'T')
					{
						notAvailFaculties.splice(notAvailFaculties.indexOf(arrFac[fac]),1);
					}
					else
					{
						for(var t=0; t<objCSched.getLineItemCount('recmachcustrecord_ederp_schedtime_schedlink'); t++)
						{
							var d=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_day', t+1);
							var st=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_start', t+1);
							var et=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_end', t+1);
							if(isOverlapping(mySecTimings,d,st,et))
							{
								arrAvailButBooked.push(arrFac[fac]);
								sectionValues.push(lineSection+'_'+d+'_'+st+'_'+et);
								t=objCSched.getLineItemCount('recmachcustrecord_ederp_schedtime_schedlink');
								cs=objFaculty.getLineItemCount('recmachcustrecord_ederp_schedule_faculty');
							}
						}
					}
					
				}
			}


		}


	}

	if(arrFac.length > 0)
	{
		//nlapiLogExecution('DEBUG','arrFac jj',arrFac);
		var names = [];
		var filterFac = [];
		filterFac[0] =  new nlobjSearchFilter('isinactive', null, 'is', 'F');
		filterFac[1] =  new nlobjSearchFilter('internalid', null, 'anyof', arrFac);
                var colFac = [];
		colFac[0]=new nlobjSearchColumn('firstname');
		colFac[1]=new nlobjSearchColumn('lastname');
		var faculty = nlapiSearchRecord('customer', null, filterFac, colFac);
		if(faculty)
		{
			//nlapiLogExecution('DEBUG','faculty',faculty);
			for(var f = 0; f < faculty.length; f++)
				names.push(faculty[f].getId()+':'+faculty[f].getValue('firstname')+' '+faculty[f].getValue('lastname'));
			//nlapiLogExecution('DEBUG','faculty',faculty.length);
		}
		var options='<option>--TBD--</option>';
		for(var fac=0; fac<arrFac.length; fac++)
		{
			if(notAvailFaculties.indexOf(arrFac[fac])<0)
			{
				var name = '';
				if(arrFac[fac])
				{
					name = getFacName(arrFac[fac],names);
				}
							
				if(arrAvailButBooked.indexOf(arrFac[fac])<0)
					options+='<option value="'+arrFac[fac]+'">'+name+'</option>';
				else
					options+='<option class="Booked" id="'+sectionValues[arrAvailButBooked.indexOf(arrFac[fac])]+'" value="'+arrFac[fac]+'">'+name+'</option>'; 
			}
		}

	}
	

	return options;
}
function getFacName(fac,names)
{
	for(var n = 0; n <names.length; n++)
	{
		var temp = names[n].split(':');
		if(fac == parseInt(temp[0]))
			return temp[1];
	}
	
	return '';
}

/**----RETURNS ROOM DETAILS AND BOOKED TIMINGS IF NOT AVAILABLE----*/
function getRoomDetails()
{
	var arrRes=[];
	var room,section;

	if(arguments.length==2 || arguments[0].getParameter('skip')!=undefined)
	{
		if(arguments.length==2)
		{
			room=arguments[0];
			section=arguments[1];
		}
		else
		{
			room=arguments[0].getParameter('room');
			section=arguments[0].getParameter('section');
		}
		if(!isEmpty(room) && !isEmpty(section))
		{
			var objRoom = nlapiLoadRecord('customrecord_ederp_room', room);
			var maxOccup=nlapiLookupField('customrecord_ederp_sec', section, 'custrecord_ederp_sec_maxoccup');
			arrRes.push([objRoom.getFieldText('custrecord_ederp_room_type'),objRoom.getFieldValue('custrecord_ederp_room_capacity'),maxOccup]);
		}
	}
	else
	{
		var request=arguments[0];
		room=request.getParameter('room');
		section=request.getParameter('section');
		var term=request.getParameter('term');
		var year=request.getParameter('year');

		//check availability
		//1.load course schedule + year n term + timings 

		if(!isEmpty(room) && !isEmpty(section) && !isEmpty(term) && !isEmpty(year))
		{
			var filter=new Array();
			filter[0]= new nlobjSearchFilter('custrecord_ederp_schedule_section', null, 'is', section);
			filter[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', year);
			filter[2]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', term);
			filter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
			filter[4]= new nlobjSearchFilter('custrecord_ederp_schedule_campus', null, 'is', getCampus());
                        var resultsCSched = nlapiSearchRecord('customrecord_ederp_schedule', null, filter, null);

			var objCSched=nlapiLoadRecord('customrecord_ederp_schedule',resultsCSched[0].getId());
			var mySecTimings=new Array();
			for(var t=0; t<objCSched.getLineItemCount('recmachcustrecord_ederp_schedtime_schedlink'); t++)
			{
				var d=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_day', t+1);
				var st=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_start', t+1);
				var et=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_end', t+1);
				mySecTimings.push([d,st,et]);
			}
			//2.search all course scheduled Time filtered by selected room year n term n != mycourse sched
			var arrFil = new Array();
			var arrCol = new Array();
			arrFil.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			arrFil.push(new nlobjSearchFilter('isinactive', 'custrecord_ederp_schedtime_schedlink', 'is', 'F'));
			arrFil.push(new nlobjSearchFilter('custrecord_ederp_schedule_room', 'custrecord_ederp_schedtime_schedlink', 'is', room));
			arrFil.push(new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', 'custrecord_ederp_schedtime_schedlink', 'is', term));
			arrFil.push(new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', 'custrecord_ederp_schedtime_schedlink', 'is', year));

			arrCol.push(new nlobjSearchColumn('internalid').setSort());
			arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedtime_day'));
			arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedtime_start'));
			arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedtime_end'));
			arrCol.push(new nlobjSearchColumn('internalid','custrecord_ederp_schedtime_schedlink'));
			arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedule_section','custrecord_ederp_schedtime_schedlink'));
			var searchScTime = nlapiSearchRecord('customrecord_ederp_schedtime',null,arrFil,arrCol);
			if(searchScTime!=null && searchScTime!='')
			{
				for(var t=0;t<searchScTime.length;t++)
				{
					var idCs = searchScTime[t].getValue('internalid','custrecord_ederp_schedtime_schedlink');
					if(idCs!=resultsCSched[0].getId())
					{
						var d=searchScTime[t].getValue('custrecord_ederp_schedtime_day');
						var st=searchScTime[t].getValue('custrecord_ederp_schedtime_start');
						var et=searchScTime[t].getValue('custrecord_ederp_schedtime_end');
						nlapiLogExecution('Debug', 'schedule time details = ',d+'##'+st+'##'+et);
						nlapiLogExecution('Debug', 'isOverlapping',isOverlapping(mySecTimings,d,st,et));
						if(isOverlapping(mySecTimings,d,st,et))
						{
							var note='<div>This room is already booked for <b class="'+searchScTime[t].getValue('custrecord_ederp_schedule_section','custrecord_ederp_schedtime_schedlink')+'" >'+searchScTime[t].getText('custrecord_ederp_schedule_section','custrecord_ederp_schedtime_schedlink')+'</b>';
							note+=' on <b>'+ getNameDay(searchScTime[t].getText('custrecord_ederp_schedtime_day').toLowerCase())+'</b>';
							note+=' from <b>'+ searchScTime[t].getText('custrecord_ederp_schedtime_start')+'</b>';
							note+=' to <b>'+ searchScTime[t].getText('custrecord_ederp_schedtime_end')+'</b>.';
							note+='</div>';
							return note;
						}
					}
				}
			}
			var objRoom = nlapiLoadRecord('customrecord_ederp_room', room);
						// for(var cs=0; cs<objRoom.getLineItemCount('recmachcustrecord_ederp_schedule_room'); cs++)
			// {
				// var idCs=objRoom.getLineItemValue('recmachcustrecord_ederp_schedule_room', 'id', cs+1);
				// var tm=objRoom.getLineItemValue('recmachcustrecord_ederp_schedule_room', 'custrecord_ederp_schedule_acadterm', cs+1);
				// var yr=objRoom.getLineItemValue('recmachcustrecord_ederp_schedule_room', 'custrecord_ederp_schedule_acadyear', cs+1);
				// if(idCs!=resultsCSched[0].getId() && tm==term && year==yr)
				// {
					////Load timings and see if overlapping or not
					// var cSched=nlapiLoadRecord('customrecord_ederp_schedule',idCs);
					// for(var t=0; t<cSched.getLineItemCount('recmachcustrecord_ederp_schedtime_schedlink'); t++)
					// {
						// var d=cSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_day', t+1);
						// var st=cSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_start', t+1);
						// var et=cSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_end', t+1);
						// nlapiLogExecution('Debug', 'isOverlapping',isOverlapping(mySecTimings,d,st,et));
						// if(isOverlapping(mySecTimings,d,st,et))
						// {
							// var note='<div>This room is already booked for <b class="'+cSched.getFieldValue('custrecord_ederp_schedule_section')+'" >'+cSched.getFieldText('custrecord_ederp_schedule_section')+'</b>';
							// note+=' on <b>'+ getNameDay(cSched.getLineItemText('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_day', t+1).toLowerCase())+'</b>';
							// note+=' from <b>'+ cSched.getLineItemText('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_start', t+1)+'</b>';
							// note+=' to <b>'+ cSched.getLineItemText('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_end', t+1)+'</b>.';
							// note+='</div>';
							// return note;
						// }
					// }
				// }
			// }
			var maxOccup=nlapiLookupField('customrecord_ederp_sec', section, 'custrecord_ederp_sec_maxoccup');
			arrRes.push([objRoom.getFieldText('custrecord_ederp_room_type'),objRoom.getFieldValue('custrecord_ederp_room_capacity'),maxOccup,objRoom.getFieldValue('custrecord_ederp_room_type')]);


		}
	}
	return arrRes;
}


/**--Check Rooms which are Already Booked--**/
function getRoomDetails1()
{
	var request=arguments[0];
	var room,section;	
	var arrRooms=[];
	var arrDays = new Array();
	var arrSTime = new Array();
	var arrEtime = new Array();
	var totalTimeDetails =new Array();
	var arrBookedRoom = '';
	room=JSON.parse(request.getParameter('room'));
	for(var i=0;i<room.length;i++)
	{
		arrRooms.push(room[i].room);
	}
	nlapiLogExecution('DEBUG','room',arrRooms);
	section=request.getParameter('section');
	var term=request.getParameter('term');
	var year=request.getParameter('year');

	//check availability
	//1.load course schedule + year n term + timings 
	var maxOccup=nlapiLookupField('customrecord_ederp_sec', section, 'custrecord_ederp_sec_maxoccup');
	if(!isEmpty(section) && !isEmpty(term) && !isEmpty(year) && (arrRooms.length>0))
	{
		var filter=new Array();
		filter[0]= new nlobjSearchFilter('custrecord_ederp_schedule_section', null, 'is', section);
		filter[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', year);
		filter[2]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', term);
		filter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
		filter[4]= new nlobjSearchFilter('custrecord_ederp_schedule_campus', null, 'is', getCampus());
                var resultsCSched = nlapiSearchRecord('customrecord_ederp_schedule', null, filter, null);

		var objCSched=nlapiLoadRecord('customrecord_ederp_schedule',resultsCSched[0].getId());
		var mySecTimings=new Array();
		for(var t=0; t<objCSched.getLineItemCount('recmachcustrecord_ederp_schedtime_schedlink'); t++)
		{
			var d=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_day', t+1);
			var st=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_start', t+1);
			var et=objCSched.getLineItemValue('recmachcustrecord_ederp_schedtime_schedlink', 'custrecord_ederp_schedtime_end', t+1);
			mySecTimings.push([d,st,et]);
			arrDays.push(d);
			arrSTime.push(st);
			arrEtime.push(et);
		}
		nlapiLogExecution('DEBUG','mySecTimings',mySecTimings);
		//2.get all course sched filtered by rooms year n term n != mycourse sched
		//var objRoom = nlapiLoadRecord('customrecord_ederp_room', room);
		var arrFil = new Array();
		var arrCol = new Array();
		arrFil.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
                arrFil.push(new nlobjSearchFilter('custrecord_ederp_schedtime_day',null, 'anyof', arrDays));
		arrFil.push(new nlobjSearchFilter('custrecord_ederp_schedule_room', 'custrecord_ederp_schedtime_schedlink', 'anyof', arrRooms));
		arrFil.push(new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', 'custrecord_ederp_schedtime_schedlink', 'is', term));
		arrFil.push(new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', 'custrecord_ederp_schedtime_schedlink', 'is', year));

		arrCol.push(new nlobjSearchColumn('internalid').setSort());
		arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedtime_day'));
		arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedtime_start'));
		arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedtime_end'));
		arrCol.push(new nlobjSearchColumn('custrecord_ederp_schedule_room','custrecord_ederp_schedtime_schedlink'));
		var search = nlapiSearchRecord('customrecord_ederp_schedtime',null,arrFil,arrCol);
		var objScheduleTime = search;
		if(search)
		{
			//Run when Search Result is More than 1000
			 while(search.length == 1000)
			 {
				 var lastId = search[999].getId();
				 arrFil.push( new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId));
				 search = nlapiSearchRecord('customrecord_ederp_schedtime',null, arrFil, arrCol);
				 objScheduleTime = objScheduleTime.concat(search);
			 }
		}
		if(objScheduleTime)
		{
			for(var index=0;index<objScheduleTime.length;index++)
			{
				var day = objScheduleTime[index].getValue('custrecord_ederp_schedtime_day');
				var stime = objScheduleTime[index].getValue('custrecord_ederp_schedtime_start');
				var etime = objScheduleTime[index].getValue('custrecord_ederp_schedtime_end');
				var roomId = objScheduleTime[index].getValue('custrecord_ederp_schedule_room','custrecord_ederp_schedtime_schedlink');
				totalTimeDetails.push([day+'#'+stime+'#'+etime+'#'+roomId]);
			}
		}
		nlapiLogExecution('DEBUG','totalTimeDetails',totalTimeDetails);
		nlapiLogExecution('DEBUG','totalTimeDetails length = ',totalTimeDetails.length);
		if(totalTimeDetails.length>0)
		{
			var newArrayTime = totalTimeDetails.toString().split(',');
			for(var i = 0;i<newArrayTime.length;i++)
			{
				var singleTime = newArrayTime[i].split('#');
				//nlapiLogExecution('Debug', 'singleTime',singleTime);
				
				var d = singleTime[0];
				var st = singleTime[1];
				var et = singleTime[2];
				var rmId = singleTime[3];
				if(isOverlapping(mySecTimings,d,st,et))
				{
					if(arrBookedRoom!='')
						arrBookedRoom+='##'+rmId;
					else
						arrBookedRoom+=rmId;
				}
				else
				{
					continue;
				}
			}
		}
	}
	nlapiLogExecution('DEBUG','arrBookedRoom',arrBookedRoom);
	return arrBookedRoom;
}


/**----RETURNS HTML LIST OF SECTIONS----*/
function getScheduler(request)
{
	var method=request.getParameter('method');
	var term,year,html='';
	var arrCourseSecP;

	var course=request.getParameter('course');	
	if(method=='scratch')
	{
		term=request.getParameter('term');
		year=request.getParameter('year');
	}
	else
	{
		term=request.getParameter('termH');
		year=request.getParameter('yearH');
	}

	var filter=new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_course', null, 'is', course);
	filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', term);
	filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', year);
	filter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',getCampus());
	if(oneWorld)
		filter[5]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',getSubsidiary());
		
	var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, null);
	if(results ==null || results == '')
	{
		html='There are no course section plans found under the selected criteria.';
		return html;
	}
	else
	{

		var col=new Array();
		col[0]=new nlobjSearchColumn('custrecord_ederp_schedtime_day');
		col[1]=new nlobjSearchColumn('custrecord_ederp_schedtime_start');
		col[2]=new nlobjSearchColumn('custrecord_ederp_schedtime_end');
		var srchfilter=new Array();
		srchfilter[0]= new nlobjSearchFilter('custrecord_ederp_schedule_course', null, 'is', course);
		srchfilter[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', term);
		srchfilter[2]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', year);
		srchfilter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
		srchfilter.push(new nlobjSearchFilter('custrecord_ederp_schedule_campus',null, 'is',getCampus()));
		if(oneWorld)
			srchfilter.push(new nlobjSearchFilter('custrecord_ederp_schedule_subsid',null, 'is',getSubsidiary()));
		
		var srchCol=new Array();
		srchCol[0]=new nlobjSearchColumn('custrecord_ederp_schedule_section');
		srchCol[1]=new nlobjSearchColumn('custrecord_ederp_schedule_status');
		srchCol[2]=new nlobjSearchColumn('custrecord_ederp_schedule_batch');
		var srchCrseSched = nlapiSearchRecord('customrecord_ederp_schedule', null, srchfilter, srchCol);

		if(method=='scratch' && request.getParameter('checkdraft')=='true')
		{
			nlapiLogExecution('Debug', 'check drat true',method);
			//find any course sched
			if(srchCrseSched !=null && srchCrseSched != '')
			{
				var statusCS=srchCrseSched[0].getValue('custrecord_ederp_schedule_status');
				nlapiLogExecution('Debug', 'statusCS',statusCS);
				if(statusCS==4)
				{
					html='There is a draft schedule created for selected academic year and term.';
					html+='<br/><br/>Click OK to update it or Cancel to select different parameters.<br/>';
					return html;
				}
				else if(statusCS==1)
				{
					html='There is already a course schedule created for the selected academic year and term, but not yet approved.';
					html+='<br/><br/>Click OK to update it or Cancel to select different parameters.<br/>';
					return html;
				}	
				else if(statusCS==3)
				{
					html='You are attempting to retrieve a schedule which has been rejected.';
					html+='<br/><br/>Click OK to update it or Cancel to select different parameters.<br/>';
					return html;
				}	
				else if(statusCS==2)
				{
					var roleUser=nlapiGetContext().getRole();
					var setup=getSetup();
					if(setup!=null && setup!='')
					{
						var editRole=nlapiLookupField('customrecord_ederp_setup', setup[0].getId(), 'custrecord_ederp_setup_schededitrole');
						if(editRole.indexOf(',')>=0)	
						{
							editRole=editRole.split(',');
							if(editRole.indexOf(roleUser.toString())<0)
							{
								html='There is already a course schedule created for the selected academic year and term, and approved.';  
								html+='<br/><br/>Only an authorized role can edit the schedule. Contact your administrator for more information.<br/>';
							}
							else 
							{
								nlapiLogExecution('Debug', 'batch',srchCrseSched[0].getId()+','+srchCrseSched[0].getValue('custrecord_ederp_schedule_batch'));
								if(!isEmpty(srchCrseSched[0].getValue('custrecord_ederp_schedule_batch')))
								{
									html='Scheduled sections were already assigned to student batches. Editing the course schedule may cause batch schedule conflicts.';
									html+='<br/><br/>Click OK to proceed regardless, or Cancel to leave the page<br/>';
								}
								else
								{
									html='There is already a course schedule created for the selected academic year and term, and approved.';
									html+='<br/><br/>Click OK to update it or Cancel to select different parameters.<br/>';
								}
							}
						}
						else if(editRole==roleUser)
						{
							nlapiLogExecution('Debug', 'batch',srchCrseSched[0].getId()+','+srchCrseSched[0].getValue('custrecord_ederp_schedule_batch'));

							if(!isEmpty(srchCrseSched[0].getValue('custrecord_ederp_schedule_batch')))
							{
								html='Scheduled sections were already assigned to student batches. Editing the course schedule may cause batch schedule conflicts.';
								html+='<br/><br/>Click OK to proceed regardless, or Cancel to leave the page<br/>';
							}
							else
							{
								html='There is already a course schedule created for the selected academic year and term, and approved.';
								html+='<br/><br/>Click OK to update it or Cancel to select different parameters.<br/>';
							}
						}
						else if(editRole!=roleUser) 
						{
							html='There is already a course schedule created for the selected academic year and term, and approved.';  
							html+='<br/><br/>Only an authorized role can edit the schedule. Contact your administrator for more information.<br/>';
						}
						return html;
					}
				}	
			}
			//check status
			//if approved check role of user

		}
		html+="<tr><td width='480px' height='12px'>";
		html+="<div>";
		html+='<input class="btnAP" type="button" value="Schedule" onClick="openPopup();"  style="float:left;margin-left: 30px; font-size:12px;"/>';
		html+='<input class="button_example" type="button" value="Mark All" onClick="un_markAll(1);"  style="float:right;margin-right: 32px;font-size:12px;"/>';
		html+='<input class="button_example" type="button" value="Unmark All" onClick="un_markAll(0);"  style="margin-right: 10px;float:right;font-size:12px;"/>';
		html+="</div></td>";

		var arrDay=getDays();
		var arrTimes=getTimes();

		//Calendar
		html+="<td rowspan='2' style='vertical-align:top;'>";
		html+= '<table id="calendar"><caption>My Calendar</caption><thead> <tr> ';
		html+= '<td></td>';
		for(var d=0;d<arrDay.length; d++)
		{
			var temp=arrDay[d].split(':');
			html+= '<td class="days" id="'+ temp[1] +'">'+ temp[0] +'</td>';
		}
		html+= '</tr></thead><tbody>';
		for(var t=0; t<arrTimes.length; t++)
		{
			var temp=arrTimes[t].split(';');
			var time=temp[0].split(':');
			if(time[1].substring(0,2)=="00")
			{
				html+= '<tr><td class="timings" id="'+ temp[1] +'">'+ temp[0] +'</td>';
				for(var d=0; d<arrDay.length; d++)
					html+= '<td><hr/></td>';
			}
		}
		html+="</tbody></table></td></tr>";

		html+="<tr><td width='480px' style='vertical-align:top;'><div id='slideshower'>";
		getCourseComponents(request.getParameter('course'),term);
		//List of sections
		var arrSections=getCourseSecfromSecP(results[0].getId(),request.getParameter('course'));
		if(arrSections==null || arrSections.length==0)
			throw nlapiCreateError('Notice', 'No sections found under the section plan.');

		var groupWise=false;
		var secGrp=arrSections[0][6];
		if(!isEmpty(secGrp))
			groupWise=true;
		nlapiLogExecution('Debug', 'groupWise',groupWise);



		html+='<div id="slideshowWindow">';
		var newComp=0;
		if(!groupWise || method=='historical')
		{
			
			arrSections.sort(function(a, b)
			{
				if(a[7]==b[7])
				{
					if(parseInt(a[0]) == parseInt(b[0]))
					{
						var x = parseInt(a[1]), y = parseInt(b[1]);
						return x == y ? 0 : (x < y ? -1 : 1);
					}
					return parseInt(a[0]) < parseInt(b[0]) ? -1 : 1;
				}
				else
				{
					return a[7]=='T'? -1 : (b[7] == 'T'? 1 : 0);
				}
			});

			nlapiLogExecution('Debug', 'arrSections',arrSections);
			
			//group per component
			if(method=='scratch')
			{
				for(var i=0 ; i<arrSections.length; i++)
				{
					if(newComp==arrSections[i][0])
					{
						html+= '<tr><td id="'+ arrSections[i][1] +'">'+arrSections[i][2]+'</td><td>'+arrSections[i][4]+'</td>';
						html+= '<td><input type="checkbox"></td></tr>';
					}
					else
					{
						if(newComp!=0)
							html+= '</tbody></table></div>';
						newComp=arrSections[i][0];
						html+= '<div class="slide">';
						html+= '<table id="course'+arrSections[i][3]+'_'+arrSections[i][5]+'" class="courses">';
						html+= '<caption>'+arrSections[i][3]+'</caption><thead><tr>';
						html+= '<td> Section </td><td> Classes per Week </td><td> Schedule </td>';
						html+= '</tr></thead><tbody>';
						html+= '<tr><td id="'+ arrSections[i][1] +'">'+arrSections[i][2]+'</td><td>'+arrSections[i][4]+'</td>';
						html+= '<td><input type="checkbox"></td></tr>';
					}

				}

			}
			else
			{
				var termP=request.getParameter('termP');
				var yearP=request.getParameter('yearP');

				//Get section Plan and course sections for the acad year and term to plan
				var filter=new Array();
				filter[0]= new nlobjSearchFilter('custrecord_ederp_secplan_course', null, 'is', course);
				filter[1]= new nlobjSearchFilter('custrecord_ederp_secplan_term', null, 'is', termP);
				filter[2]= new nlobjSearchFilter('custrecord_ederp_secplan_year', null, 'is', yearP);
				filter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
				filter[4]= new nlobjSearchFilter('custrecord_ederp_secplan_campus',null, 'is',getCampus());
				if(oneWorld)
					filter[5]= new nlobjSearchFilter('custrecord_ederp_secplan_subsidiary',null, 'is',getSubsidiary());
					
				var results = nlapiSearchRecord('customrecord_ederp_secplan', null, filter, null);
				if(results==null || results=='')
					throw nlapiCreateError('Notice','No section plan found under criteria to schedule.',true);

				arrCourseSecP=getCourseSecfromSecP(results[0].getId(),course);
				if(arrCourseSecP==null || arrCourseSecP.length==0)
					throw nlapiCreateError('Notice', 'No sections found under the section plan.');

				var secGrp=arrCourseSecP[0][6];
				nlapiLogExecution('Debug', 'make sure to schedule has sc grpe',secGrp);

				if(!isEmpty(secGrp))
					groupWise=true;
				else if(!groupWise && isEmpty(secGrp))
					groupWise=false;

				arrCourseSecP.sort(function(a, b)
						{
					if(a[7]==b[7])
					{
						if(parseInt(a[0]) == parseInt(b[0]))
						{
							var x = parseInt(a[1]), y = parseInt(b[1]);
							return x == y ? 0 : (x < y ? -1 : 1);
						}
						return parseInt(a[0]) < parseInt(b[0]) ? -1 : 1;
					}
					else
					{
						return a[7]=='T'? -1 : (b[7] == 'T'? 1 : 0);
					}
						});

				var arrSectionsH=getComponentInSecP(course,term,year);
				arrSectionsH.sort(); 
				var arrSectionsP=getComponentInSecP(course,termP,yearP);
				arrSectionsP.sort();
				
				var diff=0,compt=0,occurenceH=0,occurenceP=0;

				for(var i=0 ; i<arrSections.length; i++)
				{
					if(newComp==arrSections[i][0])
					{

						if(diff<0 && compt>=(occurenceH+diff))
						{
							html+= '<tr><td id="'+ arrSections[i][1] +'remove">'+arrSections[i][2]+'</td><td>'+arrSections[i][4]+'</td><td></td>';
							if(groupWise)
								html+= '<td>N/A</td><td>'+arrSections[i][8]+'</td>';
							html+='</tr>';
						}
						else if(diff>0 && compt==(occurenceH-1))
						{
							
							nlapiLogExecution('Debug', '2653 test','test');
							var indexInCurrentPlan=getfirstIndexOf(arrCourseSecP,arrSections[i][2],2);
							nlapiLogExecution('Debug', '2655 indexInCurrentPlan',indexInCurrentPlan);
							html+= '<tr><td id="'+ arrCourseSecP[indexInCurrentPlan][1] +'">'+arrSections[i][2]+'</td><td>'+arrSections[i][4]+'</td>';
							html+= '<td><input type="checkbox"></td>';
							if(groupWise)
								html+= '<td>'+arrCourseSecP[indexInCurrentPlan][8]+'</td><td>'+arrSections[i][8]+'</td>';
							html+= '</tr>';
							var start=occurenceP+getfirstIndexOf(arrCourseSecP,arrSections[i][0],0);
							for(var w=start-diff; w<=start-1; w++)
							{
								html+= '<tr><td id="'+ arrCourseSecP[w][1] +'">'+arrCourseSecP[w][2]+'</td><td>'+arrCourseSecP[w][4]+'</td>';
								html+= '<td><input type="checkbox">';
								if(groupWise)
									html+='<td>'+arrCourseSecP[w][8]+'</td><td>N/A</td></td>';
								html+='</tr>';
							}
						}
						else
						{
							nlapiLogExecution('Debug', '2673 test','test');
							var indexInCurrentPlan=getfirstIndexOf(arrCourseSecP,arrSections[i][2],2);
							nlapiLogExecution('Debug', '2674 indexInCurrentPlan',indexInCurrentPlan);
							html+= '<tr><td id="'+ arrCourseSecP[indexInCurrentPlan][1] +'">'+arrSections[i][2]+'</td><td>'+arrSections[i][4]+'</td>';
							html+= '<td><input type="checkbox">';
							if(groupWise)
								html+= '<td>'+arrCourseSecP[indexInCurrentPlan][8]+'</td><td>'+arrSections[i][8]+'</td></td>';
							html+= '</tr>';
							compt++;
						}
					}
					else
					{
						if(newComp!=0)
							html+= '</tbody></table></div>';
						newComp=arrSections[i][0];
						compt=0;
						occurenceH=getOccurrences(arrSectionsH,arrSections[i][0]);
						occurenceP=getOccurrences(arrSectionsP,arrSections[i][0]);
						diff=occurenceP-occurenceH;
						nlapiLogExecution('Debug', 'arrCourseSecP',arrCourseSecP);
						nlapiLogExecution('Debug', 'arrSections[i][2]',arrSections[i][2]);
						var indexInCurrentPlan=getfirstIndexOf(arrCourseSecP,arrSections[i][2],2);
						nlapiLogExecution('Debug', '2692 indexInCurrentPlan',indexInCurrentPlan);
						html+='<div class="slide">';
						html+= '<table id="course'+arrSections[i][3]+'_'+arrSections[i][5]+'" class="courses">';
						html+= '<caption>'+arrSections[i][3]+'</caption><thead><tr>';
						html+= '<td> Section </td><td> Classes/ Week </td><td> Schedule</td>';
						if(groupWise)
							html+= '<td>Current Group</td><td>Hist. Group</td>';
						html+= '</tr></thead><tbody>';
						html+= '<tr><td style="min-width:120px;" id="'+ arrCourseSecP[indexInCurrentPlan][1] +'">'+arrSections[i][2]+'</td><td>'+arrSections[i][4]+'</td>';
						html+= '<td><input type="checkbox"></td>';
						if(groupWise)
							html+= '<td>'+arrCourseSecP[indexInCurrentPlan][8]+'</td><td>'+arrSections[i][8]+'</td>';
						html+='</tr>';
						compt++;

					}

				}

			}

		}
		else
		{
			arrSections.sort(function(a, b)
			{
				if(parseInt(a[6]) == parseInt(b[6]))
				{
					if(a[7]==b[7])
					{
						if(parseInt(a[0]) == parseInt(b[0]))
						{
							var x = parseInt(a[1]), y = parseInt(b[1]);
							return x == y ? 0 : (x < y ? -1 : 1);
						}
						return parseInt(a[0]) < parseInt(b[0]) ? -1 : 1;
					}
					else
					{
						return a[7]=='T'? -1 : (b[7] == 'T'? 1 : 0);
					}
			}
			return parseInt(a[6]) < parseInt(b[6]) ? -1 : 1;
					});

			nlapiLogExecution('Debug', 'done with sort',groupWise);
			//group per group
			for(var i=0 ; i<arrSections.length; i++)
			{
				if(!isEmpty(arrSections[i][6]))
				{
					if(newComp==arrSections[i][6])
					{
						html+= '<tr><td id="'+ arrSections[i][1] +'">'+arrSections[i][2]+'</td>';
						html+= '<td id="'+ arrSections[i][0] +'_'+arrSections[i][5]+'">'+arrSections[i][3]+'</td><td>'+arrSections[i][4]+'</td>';
						html+= '<td><input type="checkbox"></td></tr>';
					}
					else
					{
						if(newComp!=0)
							html+= '</tbody></table></div>';
						newComp=arrSections[i][6];
						var secGrp=arrSections[i][8];
						nlapiLogExecution('Debug', 'secGrp name',secGrp);
						html+='<div class="slide">';
						html+= '<table id="course'+secGrp+'" class="courses">';
						html+= '<caption>'+secGrp+'</caption><thead><tr>';
						html+= '<td> Section </td><td> Component </td><td> Classes per Week </td><td> Schedule </td>';
						html+= '</tr></thead><tbody>';
						if(arrSections[i][7]=='T')
							html+= '<tr class="primary">';
						else
							html+= '<tr >';	
						html+= '<td id="'+ arrSections[i][1] +'">'+arrSections[i][2]+'</td>';
						html+= '<td id="'+ arrSections[i][0] +'_'+arrSections[i][5]+'">'+arrSections[i][3]+'</td><td>'+arrSections[i][4]+'</td>';
						html+= '<td><input type="checkbox"></td></tr>';
					}
				}

			}
		}
		html+= '</tbody></table>';
		html+="</div></td>";//end slideshow
		html+="</tr>";


		//if cs already existing , create array of CS Times
		var results=[];
		nlapiLogExecution('Debug', 'cs exists',srchCrseSched);
		if(srchCrseSched !=null && srchCrseSched != '')
		{
			for(var s=0 ; s<srchCrseSched.length; s++)
			{
				var filter= new nlobjSearchFilter('custrecord_ederp_schedtime_schedlink', null, 'is', srchCrseSched[s].getId());
				var srchRes=nlapiSearchRecord('customrecord_ederp_schedtime', null, filter, col);
				if(srchRes !=null && srchRes != '')
				{
					for(var r=0 ; r<srchRes.length; r++)
					{
						if(method=='scratch')
							results.push(srchCrseSched[s].getText('custrecord_ederp_schedule_section')+';'+srchCrseSched[s].getValue('custrecord_ederp_schedule_section')+';'+srchRes[r].getText('custrecord_ederp_schedtime_day')+';'+srchRes[r].getText('custrecord_ederp_schedtime_start') +';'+srchRes[r].getText('custrecord_ederp_schedtime_end'));
						else
						{
							var indexInCurrentPlan=getfirstIndexOf(arrCourseSecP,srchCrseSched[s].getText('custrecord_ederp_schedule_section'),2);
							var sectionId= indexInCurrentPlan==-1?srchCrseSched[s].getValue('custrecord_ederp_schedule_section')+'remove':arrCourseSecP[indexInCurrentPlan][1];
							results.push(srchCrseSched[s].getText('custrecord_ederp_schedule_section')+';'+sectionId+';'+srchRes[r].getText('custrecord_ederp_schedtime_day')+';'+srchRes[r].getText('custrecord_ederp_schedtime_start') +';'+srchRes[r].getText('custrecord_ederp_schedtime_end'));
						}
					}
				}
			}
			results.push('SolutionToUIIssue'); 
			html+='!!??'+results;
		}

	}
	nlapiLogExecution('Debug', 'cs done','');	
	return html;
}

/**----RETURNS ALL POSITIONS OF A VALUE IN AN ARRAY----*/
function getAllPos(myArray,value)
{

	var positions=new Array();
	for(var i = 0; i < myArray.length; i++) 
	{
		var temp=myArray[i].split(':');
		var paramGet=temp[0];
		if(paramGet == value) 
		{
			positions.push(i);
		}
	}

	return positions;
}

/**----GET ID OF RECORD FILTERED BY ONE PARAM----*/
function getIds(recordName,field,value)
{
	var filter = new nlobjSearchFilter(field, null, 'is', value);
	var srch = nlapiSearchRecord(recordName, null, filter, null);
	if(srch)
		return srch[0].getId();
}

/**----GET TIMES FOR THE CALENDAR----*/
function getTimes()
{
	nlapiLogExecution('Debug', 'internalid get times','');
	var jsonArr=[];
	var filter=new nlobjSearchFilter('isinactive', null, 'is', 'F');
	var arrcolumns = new Array();
	arrcolumns[0] = new nlobjSearchColumn('internalid').setSort();
	arrcolumns[1] = new nlobjSearchColumn('custrecord_ederp_timeslot_value');
	var results = nlapiSearchRecord('customrecord_ederp_timeslot', null, filter, arrcolumns);
	if(results ==null || results == '')
	{

	}
	else
	{
		for (var i = 0; i < results.length; i++) 
		{
			jsonArr.push(results[i].getValue('custrecord_ederp_timeslot_value')+";"+results[i].getId());
		}
	}
	nlapiLogExecution('Debug', 'internalid done','');
	return jsonArr;
}

/**----GET DAYS OF THE WEEK FOR THE CALENDAR----*/
function getDays()
{
	var jsonArr=[];
	var lastDaysofWeek=[];
	var fistDayFound=false;

	var arrFilters = new Array();
	arrFilters[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	arrFilters[1] = new nlobjSearchFilter('custrecord_ederp_classday_off', null, 'is', 'F');
	var arrcolumns = new Array();
	arrcolumns[0] = new nlobjSearchColumn('internalid').setSort();
	arrcolumns[1] = new nlobjSearchColumn('name');
	arrcolumns[2] = new nlobjSearchColumn('custrecord_ederp_classday_first');

	var srchDays = nlapiSearchRecord('customrecord_ederp_classday', null, arrFilters, arrcolumns);

	if(srchDays ==null || srchDays == '')
	{

	}
	else
	{
		for (var i = 0; i < srchDays.length; i++) 
		{
			if(fistDayFound)
				jsonArr.push(srchDays[i].getValue('name')+":"+srchDays[i].getId());
			else
			{
				if(srchDays[i].getValue('custrecord_ederp_classday_first')=='T')
				{
					fistDayFound=true;
					jsonArr.push(srchDays[i].getValue('name')+":"+srchDays[i].getId());
				}
				else
					lastDaysofWeek.push(srchDays[i].getValue('name')+":"+srchDays[i].getId()); 
			}
		}

		for(var i=0; i<lastDaysofWeek.length; i++) 
		{
			jsonArr.push(lastDaysofWeek[i]);
		}
	}	
	return jsonArr;
}

/**----RETURNS WHOLE DAY NAME FROM ABBREV NAME----*/
function getNameDay(day)
{
	switch (day)
	{
	case 'mon' :
		day='Monday';
		break;
	case 'm' :
		day='Monday';
		break;
	case  'tue' :
		day= 'Tuesday';
		break;
	case  't' :
		day= 'Tuesday';
		break;
	case  'wed' :
		day= 'Wednesday';
		break;
	case  'w' :
		day= 'Wednesday';
		break;
	case  'thu' :
		day= 'Thursday';
		break;
	case  'th' :
		day= 'Thursday';
		break;
	case  'fri' :
		day= 'Friday';
		break;
	case  'f' :
		day= 'Friday';
		break;
	case  'sat' :
		day= 'Saturday';
		break;
	case  's' :
		day= 'Saturday';
		break;
	case  'sun' :
		day= 'Sunday';
		break;
	case  'd' :
		day= 'Sunday';
		break;
	}	
	return day;
}

/**----FUNCTION THAT SAVES FACULTY AND ROOMS IN COURSE SCHEDULE RECORD----*/
function saveFaculClass(request)
{
	var initRequest=request;
	var notProcessedData=new Array();

	//convert from encrypted to decrypted
	var request=EncToDec('savefacclass',request);
	nlapiLogExecution('Debug', 'in save fac after dec',request.getParameter('year'));

	var year=request.getParameter('year');
	var course=request.getParameter('course');
	var term=request.getParameter('term');
	var arrFacClass=request.getParameter('arr').split(',');
	notProcessedData=arrFacClass.slice(0);//shallow copy
	nlapiLogExecution('Debug', 'arrFac + notProcessedData',arrFacClass.length+':'+notProcessedData.length);
	for(var i=0; i<arrFacClass.length; i++)
	{
		if ((nlapiGetContext().getRemainingUsage() < USAGE_LIMIT_THRESHOLD))
		{ 

			nlapiLogExecution('Debug', 'usage in save fac class',nlapiGetContext().getRemainingUsage());
			var param = new Array();
			param['year']=initRequest.getParameter('year');
			param['course']=initRequest.getParameter('course');
			param['term']=initRequest.getParameter('term');
			param['action']=initRequest.getParameter('action');
			param['arr']=notProcessedData.toString();
			if(initRequest.getParameter('method')!=undefined)
				param['method']=initRequest.getParameter('method');
			nlapiSetRedirectURL('SUITELET','customscript_ederph_st_coursesched', 'customdeploy_ederph_st_coursesched', null, param);
			return 'abort';

		}
		else
		{
			var temp=arrFacClass[i].split(':');
			temp[0]=base64_decode(temp[0]);
			temp[1]=base64_decode(temp[1]);
			temp[2]=base64_decode(temp[2]);
			temp[3]=base64_decode(temp[3]);
			nlapiLogExecution('Debug', 'arrFacClasses Details = ',temp[0]+'@@'+temp[1]+'@@'+temp[2]+'@@'+temp[3]);
		        var arrFilters = new Array();
			arrFilters[0] = new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', year);
			arrFilters[1] = new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', term);
			arrFilters[2] = new nlobjSearchFilter('custrecord_ederp_schedule_course', null, 'is', course);
			arrFilters[3] = new nlobjSearchFilter('custrecord_ederp_schedule_section', null, 'is', temp[0]);
			arrFilters[4] = new nlobjSearchFilter('isinactive', null, 'is','F');
			arrFilters[5] = new nlobjSearchFilter('custrecord_ederp_schedule_campus', null, 'is', getCampus());
                        var srchCourseSched = nlapiSearchRecord('customrecord_ederp_schedule', null, arrFilters, null);
			nlapiLogExecution('Debug', 'recCourseSched found',srchCourseSched[0].getId());
			if(srchCourseSched)
			{
				var recCourseSched=nlapiLoadRecord('customrecord_ederp_schedule', srchCourseSched[0].getId());
				var secPlan=recCourseSched.getFieldValue('custrecord_ederp_schedule_secplan');

				if(temp[1]=='--TBD--')
					temp[1]='';
				if(temp[2]=='--TBD--')
					temp[2]='';
				if(temp[3]=='--TBD--' || temp[3]=='undefined')
					temp[3]='';

				recCourseSched.setFieldValue('custrecord_ederp_schedule_faculty', temp[1]);
				recCourseSched.setFieldValue('custrecord_ederp_schedule_building', temp[2]);
				recCourseSched.setFieldValue('custrecord_ederp_schedule_room', temp[3]);
				recCourseSched.setFieldValue('custrecord_ederp_schedule_status', 4);
				nlapiSubmitRecord(recCourseSched, true);
				//update status in sec plan also
				nlapiSubmitField('customrecord_ederp_secplan',secPlan, 'custrecord_ederp_secplan_schedstatus', 4);
				notProcessedData.splice(0,1);
				nlapiLogExecution('Debug', 'splice',notProcessedData.length);

			}

		}
	}
	nlapiLogExecution('Debug', 'end',notProcessedData.length);
	return 'Course schedule sucessfully Saved as Draft';
}

/**----FUNCTION THAT GETS HTTP REQUEST WITH ENCODED PARAMS AND RETURN ARRAY WITH SAME DECODED PARAMS----*/
function EncToDec(action,request)
{
	var result=new Array();
	switch(action)
	{
	case 'preview':
		nlapiLogExecution('Debug', 'in encrypt dec',request.getParameter('year'));
		var year=base64_decode(request.getParameter('year'));
		var course=base64_decode(request.getParameter('course'));
		var term=base64_decode(request.getParameter('term'));
		var method=base64_decode(request.getParameter('method'));
		var arr=request.getParameter('arr');
		result['year']=year;
		result['course']=course;
		result['term']=term;
		result['method']=method;
		result['arr']=arr;
		result['action']=action;
		break;
	case 'submit':
		nlapiLogExecution('Debug', 'in encrypt submit',request.getParameter('year'));
		var year=base64_decode(request.getParameter('year'));
		var course=base64_decode(request.getParameter('course'));
		var term=base64_decode(request.getParameter('term'));
		result['year']=year;
		result['course']=course;
		result['term']=term;
		break;
	case 'savefacclass':
		nlapiLogExecution('Debug', 'in encrypt savefac class',request.getParameter('year'));
		var year=base64_decode(request.getParameter('year'));
		var course=base64_decode(request.getParameter('course'));
		var term=base64_decode(request.getParameter('term'));
		result['year']=year;
		result['course']=course;
		result['term']=term;
		result['action']=action;
		result['arr']=request.getParameter('arr');
		break;
	case 'saving':
		nlapiLogExecution('Debug', 'in encrypt saving',request.getParameter('mode'));
		var mode=base64_decode(request.getParameter('mode'));
		var method=base64_decode(request.getParameter('method'));
		var course=base64_decode(request.getParameter('course'));
		if(method=='scratch')
		{
			var year=base64_decode(request.getParameter('year'));
			var term=base64_decode(request.getParameter('term'));
			result['term']=term;
			result['year']=year;
		}
		else
		{
			var yearP=base64_decode(request.getParameter('yearP'));
			var termP=base64_decode(request.getParameter('termP'));
			var yearH=base64_decode(request.getParameter('yearH'));
			var termH=base64_decode(request.getParameter('termH'));
			result['termP']=termP;
			result['yearP']=yearP;
			result['termH']=termH;
			result['yearH']=yearH;
		}
		if(mode!='nextwithoutsv')
			result['arr']=request.getParameter('arr');
		if(request.getParameter('arrRemv')!=undefined)
			result['arrRemv']=request.getParameter('arrRemv');
		if(request.getParameter('realmode')!=undefined)
			result['realmode']=request.getParameter('realmode');
		result['course']=course;
		result['method']=method;
		result['mode']=mode;
		result['f']=request.getParameter('f');
		break;
	}

	return result;
}

/**----SAVE FACULTY & ROOMS IN CS RECORDS AND DISPLAY PREVIEW PAGE----*/
function preview(request,response, formCP)
{
	//saving fac and clasrooms
	if(request.getParameter('onlyprev')==undefined)
		if(saveFaculClass(request) == 'abort')
			return 'abort';

	if ((nlapiGetContext().getRemainingUsage() < USAGE_LIMIT_THRESHOLD))
	{ 

		nlapiLogExecution('Debug', 'usage in prev',nlapiGetContext().getRemainingUsage());
		var param = new Array();
		param['action']='preview';
		param['year']=request.getParameter('year');
		param['course']=request.getParameter('course');
		param['term']=request.getParameter('term');
		param['method']=request.getParameter('method');
		param['arr']=request.getParameter('arr');
		param['onlyprev']='true';
		nlapiSetRedirectURL('SUITELET','customscript_ederph_st_coursesched', 'customdeploy_ederph_st_coursesched', null, param);
		return 'abort';

	}
	else
	{

		//convert from encrypted to decrypted
		var request=EncToDec('preview',request);
		var method=request.getParameter('method');
		nlapiLogExecution('Debug', 'in prev after dec',request.getParameter('year'));


		htmlBody="<body>";
		nlapiLogExecution('Debug', 'in preview','-----');
		/*---Get Data---*/
		var subsText='';
		if(oneWorld)
			subsText=nlapiLookupField('subsidiary', getSubsidiary(), 'name');
		var campusTxt = nlapiLookupField('customlist_ederp_campus',getCampus(),'name');
		var depts = getDepartment();
		var deptText = '';
		for(var dp=0; dp<depts.length;dp++)
		{
			var deptTxt=nlapiLookupField('department', depts[dp], 'name');
			if(deptTxt.indexOf(':')>=0)
			{
				deptTxt=deptTxt.split(":");
				deptText+=deptTxt[deptTxt.length-1]+'</br>';
			}
			
		}
		
		if(subsText.indexOf(':')>=0)
		{
			subsText=subsText.split(":");
			subsText=subsText[subsText.length-1];
		}  

		if(method=='scratch')
		{
			var idYear=request.getParameter('year');
			var idTerm=request.getParameter('term');
		}
		else
		{
			var idYear=request.getParameter('year').split(':')[0];
			var idTerm=request.getParameter('term').split(':')[0];
		}

		//getting sections
		var options='<option>--Show All--</option>';
		var arrOptions=[];
		var results='';

		var col=new Array();
		col[0]=new nlobjSearchColumn('custrecord_ederp_schedtime_day');
		col[1]=new nlobjSearchColumn('custrecord_ederp_schedtime_start');
		col[2]=new nlobjSearchColumn('custrecord_ederp_schedtime_end');
		col[3]=new nlobjSearchColumn('custrecord_ederp_schedule_section','custrecord_ederp_schedtime_schedlink');
		col[4]=new nlobjSearchColumn('custrecord_ederp_schedule_faculty','custrecord_ederp_schedtime_schedlink');
		col[5]=new nlobjSearchColumn('custrecord_ederp_schedule_room','custrecord_ederp_schedtime_schedlink');
		col[6]=new nlobjSearchColumn('custrecord_ederp_schedtime_schedlink').setSort();
		col[7]=new nlobjSearchColumn('custrecord_ederp_schedule_faculty_disp','custrecord_ederp_schedtime_schedlink');
		var srchfilter=new Array();
		srchfilter[0]= new nlobjSearchFilter('custrecord_ederp_schedule_course', 'custrecord_ederp_schedtime_schedlink', 'is', request.getParameter('course'));
		srchfilter[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', 'custrecord_ederp_schedtime_schedlink', 'is', idTerm);
		srchfilter[2]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', 'custrecord_ederp_schedtime_schedlink', 'is', idYear);
		
		if(oneWorld)
			srchfilter.push(new nlobjSearchFilter('custrecord_ederp_schedule_subsid','custrecord_ederp_schedtime_schedlink', 'is',getSubsidiary()));
		srchfilter.push(new nlobjSearchFilter('custrecord_ederp_schedule_campus','custrecord_ederp_schedtime_schedlink', 'is',getCampus()));
		srchfilter.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		srchfilter.push(new nlobjSearchFilter('isinactive','custrecord_ederp_schedtime_schedlink', 'is','F'));
		
		var srchCrseSched = nlapiSearchRecord('customrecord_ederp_schedtime', null, srchfilter, col);
		nlapiLogExecution('Debug', 'srchCrseSched',srchCrseSched.length);
		var arrTemp=new Array();
		if(srchCrseSched !=null && srchCrseSched != '')
		{
			for(var s=0 ; s<srchCrseSched.length; s++)
			{  
				var section=srchCrseSched[s].getValue('custrecord_ederp_schedule_section','custrecord_ederp_schedtime_schedlink');
				if(arrTemp.indexOf(section)<0)
				{
					arrOptions.push([section,srchCrseSched[s].getText('custrecord_ederp_schedule_section','custrecord_ederp_schedtime_schedlink')]);  
					arrTemp.push(section);
				}
				var nameFac=srchCrseSched[s].getValue('custrecord_ederp_schedule_faculty','custrecord_ederp_schedtime_schedlink');
				if(!isEmpty(nameFac))
				{
					nameFac=srchCrseSched[s].getValue('custrecord_ederp_schedule_faculty_disp','custrecord_ederp_schedtime_schedlink');
				}

				var text=srchCrseSched[s].getText('custrecord_ederp_schedule_section','custrecord_ederp_schedtime_schedlink')+'/'+nameFac+'/'+srchCrseSched[s].getText('custrecord_ederp_schedule_room','custrecord_ederp_schedtime_schedlink');
				results+=text+';'+section+';'+srchCrseSched[s].getText('custrecord_ederp_schedtime_day')+';'+srchCrseSched[s].getText('custrecord_ederp_schedtime_start') +';'+srchCrseSched[s].getText('custrecord_ederp_schedtime_end')+']';


			}

		}

		arrOptions.sort(function(a, b)
				{
			return parseInt(a[0]) < parseInt(b[0]) ? -1 : 1;
			return 0;
				});
		for(var o=0; o<arrOptions.length; o++)
			options+='<option value="'+arrOptions[o][0]+'">'+arrOptions[o][1]+'</option>';

		//end getting sections
		var method=request.getParameter('method');
		nlapiLogExecution('Debug', 'subsText','-----');
		var course=nlapiLookupField('serviceitem', request.getParameter('course'), 'itemid');
		nlapiLogExecution('Debug', 'course','-----');
		var courseDesc=nlapiLookupField('serviceitem', request.getParameter('course'), 'salesdescription');	
		nlapiLogExecution('Debug', 'courseDesc','-----');
		if(method=='scratch')
		{
			var term=nlapiLookupField('customrecord_ederp_acadterm', request.getParameter('term'), 'name');
			var year=nlapiLookupField('customrecord_ederp_acadyear', request.getParameter('year'), 'name');
		}
		else
		{
			var termP=nlapiLookupField('customrecord_ederp_acadterm', request.getParameter('term').split(':')[0], 'name');
			var yearP=nlapiLookupField('customrecord_ederp_acadyear', request.getParameter('year').split(':')[0], 'name');
			var termH=nlapiLookupField('customrecord_ederp_acadterm', request.getParameter('term').split(':')[1], 'name');
			var yearH=nlapiLookupField('customrecord_ederp_acadyear', request.getParameter('year').split(':')[1], 'name');
		}
		nlapiLogExecution('Debug', 'Param',courseDesc);


		htmlBody+='<div style="margin-bottom:15px;"><input id="btnBack" class="btnAP" type="button" value="Back" onClick="goBack(\'' + request.getParameter('year') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('term')+'\',\''+method+'\');" />';
		htmlBody+='<input  class="btnAP" type="button" value="Save as Draft" onClick="onSubmit(this);"/>';
		htmlBody+='<input id="btnSubmit" class="btnAP" type="button" value="Submit" onClick="onSubmit(this,\'' + idYear + '\',\''+request.getParameter('course')+'\',\''+idTerm+'\');" /></div>';
		//htmlBody+='<input id="btnCancel" class="btnAP" type="button" value="Cancel" onClick="onCancel();"  style="margin-left:10px;margin-bottom:14px;"/></div>';
		/*---End Get Data---*/ 
		htmlBody+="<h4 class='FldGrpHdr'>Step 1: Primary Information </h4>";
		htmlBody+='<table id="CrsSchedFields">';
		/*----Subsidiary----*/
		htmlBody+='<tr><td>Subsidiary<br/><span class="depSubsFld" contenteditable="false" >'+subsText+'</span></td>';
		if(method=='scratch')
		{
			/*----Academic Year----*/
			htmlBody+='<td> Academic Year <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" >'+year+'</span></td>';
		}
		else
		{
			/*----Academic Year to sched----*/
			htmlBody+='<td> Academic Year To Schedule<br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" >'+yearP+'</span></td>';
			/*----hist Academic Year----*/
			htmlBody+='<td> Historical Academic Year <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" >'+yearH+'</span></td>';
		}
		/*----Course Code----*/
		htmlBody+='<td> Course Code <br/>';
		htmlBody+='<span class="depSubsFld" contenteditable="false" >'+course+'</span></td>';
		/*----Programs----*/
		var text='';
		var data=getPrograms(request).toString();
		data=data.split(',');
		nlapiLogExecution('Debug', 'get programs length',data.length);
		for(var i=0;i<data.length;i++)
			text+='-'+ data[i] +'\n';
		htmlBody+='<td valign="top" rowspan="2"> Programs in Section Plan <br/><textarea id="listPrograms" rows="6" readonly>'+text+'</textarea></td></tr>';
		/*----Department----*/
		htmlBody+='<tr><td>Department<br/><span class="depSubsFld" contenteditable="false">'+deptText+'</span></td>';
		if(method=='scratch')
		{
			/*----Academic Term----*/
			htmlBody+='<td> Academic Term <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" >'+term+'</span></td>';
		}
		else
		{
			/*----Academic Term to sched----*/
			htmlBody+='<td> Academic Term To Schedule <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" >'+termP+'</span></td>';
			/*----hist Academic Year----*/
			htmlBody+='<td> Historical Academic Term <br/>';
			htmlBody+='<span class="depSubsFld" contenteditable="false" >'+termH+'</span></td>';
		}
		/*----Course Description----*/
		htmlBody+='<td> Course Description<br/><span class="deptFld" id="descFld" contenteditable="false">'+courseDesc+'</span> </td></tr>';
		/*----Campus----*/
		htmlBody+='<tr><td>Campus<br/><span class="deptFld" contenteditable="false">'+campusTxt+'</span></td></tr>';
		htmlBody+='</table>';

		htmlBody+="<h4 class='FldGrpHdr' style='margin-top:-10px;'>Step 5: Schedule Preview</h4>";
		htmlBody+='<div style="margin-top:15px;margin-left:11px;margin-bottom:20px"><span class="title">Display by section: </span>'; 
		htmlBody+='<select id="dispBySec">'+options+'</select></div>';


		nlapiLogExecution('Debug', 'Calendar begins',courseDesc);
		//Calendar
		var arrDay=getDays();
		var arrTimes=getTimes();
		htmlBody+= '<table id="calendar" class="center"><caption>My Calendar</caption><thead> <tr> ';
		htmlBody+= '<td></td>';
		for(var d=0;d<arrDay.length; d++)
		{
			var temp=arrDay[d].split(':');
			htmlBody+= '<td class="days" id="'+ temp[1] +'">'+ temp[0] +'</td>';
		}
		htmlBody+= '</tr></thead><tbody>';
		for(var t=0; t<arrTimes.length; t++)
		{
			var temp=arrTimes[t].split(';');
			var time=temp[0].split(':');
			if(time[1].substring(0,2)=="00")
			{
				htmlBody+= '<tr><td class="timings" id="'+ temp[1] +'">'+ temp[0] +'</td>';
				for(var d=0; d<arrDay.length; d++)
					htmlBody+= '<td><hr/></td>';
			}
		}
		htmlBody+="</tbody></table>";

		nlapiLogExecution('Debug', 'Calendar end',courseDesc);

		var fieldHidden=formCP.addField('custpage_canvas_array','longtext','');
		fieldHidden.setDisplayType('hidden');
		fieldHidden.setDefaultValue(results);

		htmlBody+='<div style="margin-top:15px;"><input id="btnBack" class="btnAP" type="button" value="Back" onClick="goBack(\'' + request.getParameter('year') + '\',\''+request.getParameter('course')+'\',\''+request.getParameter('term')+'\',\''+method+'\');" />';
		htmlBody+='<input  class="btnAP" type="button" value="Save as Draft" onClick="onSubmit(this);" />';
		htmlBody+='<input id="btnSubmit" class="btnAP" type="button" value="Submit" onClick="onSubmit(this,\'' + idYear + '\',\''+request.getParameter('course')+'\',\''+idTerm+'\');" /></div>';
		htmlBody+='</body></html>';
	}

	return htmlBody;
}

/**----SUBMIT COURSE SCHEDULES AND SEND EMAILS IF REQUIRED----*/
function submission(request)
{
	//convert from encrypted to decrypted
	var request=EncToDec('submit',request);
	nlapiLogExecution('Debug', 'in submit after dec',request.getParameter('year'));

	//Get setup
	var setup=getSetup();
	var defaultStatus='';
	if(setup!=null && setup!='')
	{
		defaultStatus=setup[0].getValue('custrecord_ederp_setup_defschedulestatus');
		var col= new nlobjSearchColumn('custrecord_ederp_schedule_secplan');
		var srchfilter=new Array();
		srchfilter[0]= new nlobjSearchFilter('custrecord_ederp_schedule_course', null, 'is', request.getParameter('course'));
		srchfilter[1]= new nlobjSearchFilter('custrecord_ederp_schedule_acadterm', null, 'is', request.getParameter('term'));
		srchfilter[2]= new nlobjSearchFilter('custrecord_ederp_schedule_acadyear', null, 'is', request.getParameter('year'));
		srchfilter[3]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
		if(oneWorld)
			srchfilter.push(new nlobjSearchFilter('custrecord_ederp_schedule_subsid',null, 'is',getSubsidiary()));
		srchfilter.push(new nlobjSearchFilter('custrecord_ederp_schedule_campus',null, 'is',getCampus()));
		
		var srchCrseSched = nlapiSearchRecord('customrecord_ederp_schedule', null, srchfilter, col);

		if(srchCrseSched !=null && srchCrseSched != '')
		{
			for(var s=0 ; s<srchCrseSched.length; s++)
				nlapiSubmitField('customrecord_ederp_schedule', srchCrseSched[s].getId(), 'custrecord_ederp_schedule_status', defaultStatus);

			//update status in sec plan also
			var secPlan=srchCrseSched[0].getValue('custrecord_ederp_schedule_secplan');
			nlapiSubmitField('customrecord_ederp_secplan',secPlan, 'custrecord_ederp_secplan_schedstatus', defaultStatus);

		}


		//Now if pending approval look for every role allowed to approve in this department and subsid
		//Then send email with link to the person responsible
		var sendEmailCheckbox=setup[0].getValue('custrecord_ederp_setup_courseappremail');
		if(defaultStatus==1 && sendEmailCheckbox=='T')
		{
			nlapiLogExecution('debug', 'status',defaultStatus);
			var sendEmailCheckbox=setup[0].getValue('custrecord_ederp_setup_courseappremail');
			nlapiLogExecution('debug', 'sendEmailCheckbox',sendEmailCheckbox);
			if(sendEmailCheckbox=='T')
			{  
				var URL=getNetsuiteURL();
				URL = nlapiResolveURL('SUITELET','customscript_ederph_st_corseldisplay','customdeploy_ederph_st_corseldisplay');
				URL += '&action=approval&acdyear='+base64_encode(request.getParameter('year'));
				URL += '&acdcourse='+base64_encode(request.getParameter('course'));
				URL += '&acdterm='+base64_encode(request.getParameter('term'));
				URL += '&acdcomp=';
				URL += '&acdsec=';
				URL+='&compid='+ nlapiGetContext().getCompany();
				nlapiSubmitField('customrecord_ederp_schedule', srchCrseSched[0].getId(), 'custrecord_ederp_schedule_hiddenlink', URL);

				var deptApproval = nlapiLookupField('customrecord_ederp_schedule', srchCrseSched[0].getId(), 'custrecord_ederp_schedule_course.department');
				var emailappr_role = setup[0].getValue('custrecord_ederp_setup_schedapprovalrole').split(',');
				nlapiLogExecution('debug', 'emailappr_role length',emailappr_role.length);
				var sender = setup[0].getValue('custrecord_ederp_setup_fromemail');
				var template = setup[0].getValue('custrecord_ederp_setup_schedpending');
				if(!isEmpty(template))
				{
					var arrFilter=new Array();
					arrFilter[0]=new nlobjSearchFilter('department', null, 'is', deptApproval);
					arrFilter[1]=new nlobjSearchFilter('isinactive', null, 'is', 'F');
					arrFilter[2]=new nlobjSearchFilter('role', null, 'anyof', emailappr_role);
					arrFilter[3]=new nlobjSearchFilter('custentity_ederp_campus', null, 'is', getCampus());
					if(oneWorld)
						arrFilter[4]=new nlobjSearchFilter('subsidiary', null, 'is', getSubsidiary());
                                        var srchEmployees = nlapiSearchRecord('employee', null, arrFilter, [new nlobjSearchColumn('email')]);
				
					var emailMerger = nlapiCreateEmailMerger(template);
					emailMerger.setCustomRecord('customrecord_ederp_schedule', srchCrseSched[0].getId());
					var mergeResult = emailMerger.merge(); 
				
					
					if (srchEmployees) {
						nlapiLogExecution('debug', 'emp length',srchEmployees.length);
						for(var i=0; i < srchEmployees.length;i++ ){
							sftpiSendEmail(sender, srchEmployees[i].getValue('email'), mergeResult);//Send Mail
						}
					}
					nlapiLogExecution('debug', 'roles ',emailappr_role);
					arrFilter[0]=new nlobjSearchFilter('custentity_ederp_department', null, 'anyof', deptApproval);
					arrFilter[2]=new nlobjSearchFilter('custentity_ederp_role', null, 'anyof', emailappr_role);
					arrFilter.push(new nlobjSearchFilter('custentity_ederp_role', null, 'noneof', '@NONE@'));
                                        var srchCustomers = nlapiSearchRecord('customer', null, arrFilter, [new nlobjSearchColumn('email')]);
					if (srchCustomers) {
						nlapiLogExecution('debug', 'custom length',srchCustomers.length);
						for(var i=0; i < srchCustomers.length;i++ ){
							sftpiSendEmail(sender, srchCustomers[i].getValue('email'), mergeResult);//Send Mail
						}
					}
				}
				else
					return 'Course schedule sucessfully Saved but no emails sent. Please provide a template.';

			}
		}
	}
	else
		throw nlapiCreateError('Notice','This subsidiary has no setup.',true);

	return 'Course schedule sucessfully Saved';
}

/**----GET SETUP WITH ALL NECESSARY FIELDS----*/
function getSetup() {
	var subsidiary = getSubsidiary();
	var filters = new Array();
	if (!isEmpty(subsidiary)) {
		filters.push(new nlobjSearchFilter('custrecord_ederp_setup_subsidiary',null,'anyof',subsidiary));
	}
	filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_secgroups'));//if true use section groups
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_defschedulestatus'));//default status upon submit
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_courseappremail'));//if true inform approver
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_courseowneremail'));//if true inform owner
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_schedapprovalrole'));//approval role
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_schedaccessrole'));//access role
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_schededitrole'));//edit role
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_fromemail'));//sender
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_schedpending'));//pending approval template
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_schedapproved'));//approved template
	columns.push(new nlobjSearchColumn('custrecord_ederp_setup_schedrejected'));//rejected template

	var setup = nlapiSearchRecord('customrecord_ederp_setup', null, filters, columns);
	return setup;
}

/**----GET USER'S SUBSIDIARY----*/
function getSubsidiary()
{
	var subsidiary;
	if(oneWorld)
		subsidiary = (nlapiLookupField(Customer_Employee, nlapiGetContext().getUser(), 'subsidiary'));
	return subsidiary;
}

/**----GET USER'S DEPARTMENT----*/
function getDepartment() {

	var userType = (nlapiLookupField('entity', nlapiGetContext().getUser(), 'type')).toLowerCase();
	var dept = '';
	if (userType == 'employee') {
		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid' , null, 'is', nlapiGetContext().getUser()));
		var col = new nlobjSearchColumn('custentity_ederp_department');
		var search = nlapiSearchRecord('customer',null ,filters,col);
		if(search)
		{
			dept = search[0].getValue('custentity_ederp_department');
		}
		else
		{
			dept = nlapiLookupField(userType, nlapiGetContext().getUser(), 'department');
			Customer_Employee = 'employee';
		}
		dept = dept.split(',');
	}else{
		if (userType == 'custjob') {
			userType = 'customer';
		}
		dept = nlapiLookupField(userType, nlapiGetContext().getUser(), 'custentity_ederp_department');
		dept = dept.split(',');
	}
	return dept;	
}

/**----GET CURRENT NS DOMAIN----*/
function getNetsuiteURL()
{
	var linkUrl;
	switch (nlapiGetContext().getEnvironment()) 
	{
	case "PRODUCTION":
		linkUrl = 'https://system.netsuite.com';
		break;

	case "SANDBOX":
		linkUrl = 'https://system.sandbox.netsuite.com';
		break;

	case "BETA":
		linkUrl = 'https://system.beta.netsuite.com';
		break;
	}

	return linkUrl;
}
function getCampus()
{
	return nlapiLookupField(Customer_Employee, nlapiGetContext().getUser(),'custentity_ederp_campus');
}


