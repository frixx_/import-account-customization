function suiteletGetForm(request, response){
	if (request.getMethod() == 'GET' ){

		var form = nlapiCreateForm("GET call" );
		form.addField('field_name', 'text', 'Text Field' ).setDefaultValue('');
    	form.addField('field_age', 'integer', 'Integer Field' ).setDefaultValue(0);
    	form.addField('field_address', 'text', 'Address');

    	form.addSubmitButton('Submit');
    	
    	response.writePage(form);

    	}else{

    		//get the field values
    		//create the custom record
    		//submit custom record
    		//create the POST form
    		//write the form
    		//**************************
    		//
    		var record = nlapiCreateRecord('customrecord_submit_record');

    		var recordName = request.getParameter('field_name');
    		record.setFieldValue('name', recordName);
    		var recordAge = request.getParameter('field_age');
    		record.setFieldValue('custrecord_age', recordAge);
    		var recordAddress = request.getParameter('field_address');
    		record.setFieldValue('custrecord_address', recordAddress);
    		nlapiSubmitRecord(record, true, true);


    		var form = nlapiCreateForm("POST call" );

   			var resName = form.addField('res_name', 'text', 'Your name: ' );
    		resName.setDefaultValue(request.getParameter('field_name' ));
    		resName.setDisplayType('inline');

    		var resAge = form.addField('res_age', 'integer', 'Age:' );
    		resAge.setDefaultValue(request.getParameter('field_age' ));
    		resAge.setDisplayType('inline');

    		var resAddress = form.addField('res_address', 'text', 'Address' );
    		resAddress.setDefaultValue(request.getParameter('field_address' ));
    		resAddress.setDisplayType('inline');
    		
    		response.writePage(form);
    	}
}

