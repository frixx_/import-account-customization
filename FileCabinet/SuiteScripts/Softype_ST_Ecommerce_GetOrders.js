/**
* @NApiVersion 2.x
* @NScriptType Suitelet
*/

define(['N/runtime', 'N/task', 'N/ui/serverWidget'],
	function callbackFunction(runtime, task, serverWidget) {
		function getFunction(context) {

			var website = context.request.parameters.web;

			var scriptObj = runtime.getCurrentScript();
			var scriptID = scriptObj.getParameter({ name: 'custscript_getorders_scriptid' });
			var imageURL = scriptObj.getParameter({ name: 'custscript_getorders_imageurl' });
			var deploymentID = scriptObj.getParameter({ name: 'custscript_' + website + '_orders_deploymentid' });

			var scriptTask = task.create({
				taskType: task.TaskType.SCHEDULED_SCRIPT,
				scriptId: scriptID,
				deploymentId: deploymentID
			});

			var scriptTaskId = scriptTask.submit();

			log.debug("Script Task ID", scriptTaskId);

			var backFunction =
				"<script>" +
				'var x = document.createElement("IMG");' +
				'x.setAttribute("src", "'+imageURL+'");' +
				'x.setAttribute("width", "300");' +
				'x.setAttribute("alt", "Loading");' +
				'x.setAttribute("style", "margin: 20% 0% 0% 40%");' +
				'setTimeout(() => { document.body.appendChild(x) }, 1000);' +
				"</script>" +
				"<script>" +
				"function goBack() {" +
				"window.history.back();" +
				"}" +
				"setTimeout(() => {  goBack(); }, 5000);" +
				"</script>";
			context.response.write(backFunction);
		}

		function onRequestFxn(context) {
			if (context.request.method === "GET") {
				getFunction(context)
			}
		}
		return {
			onRequest: onRequestFxn
		};
	});