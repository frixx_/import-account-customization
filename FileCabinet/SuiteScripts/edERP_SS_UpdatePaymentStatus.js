/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                
 **                       
 **@Author      :  Anand Yadav
 **@Dated       :  23 Sep 2019
 **@Version     :  2.0
 **@Description :  To update the payment status of the current account as provided in softype account on a scheduled basis.
 ***************************************************************************************/

define(['N/https','N/record','N/runtime','N/search'],

function(https,record,runtime,search) {
   
    function execute(context) {
    	
	    try{
	    	
	    	log.debug("Scheduled script called");
	    	var url="https://tstdrv1234890.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=1581&deploy=1&compid=TSTDRV1234890&h=e1aa9697323f3c44d351";
	    	
	    	var accountID=runtime.accountId;
	    	log.debug("accountID",accountID);
	    	
	    	url+="&accountID="+accountID;
	    	
	    	var response = https.get({
			    url: url
			 });
	    	log.debug("response",response.body);
	    	
	    	if(response.body){
	    		
	    		var resBody=JSON.parse(response.body);
		    	
		    	var paymentStatus=resBody['PaymentStatus'];
		    	
		    	var searchResult = search.create({
		            type: 'customrecord_payment_status'
		            
		        }).run().getRange({start:0,end:1000});
		    	
		    	log.debug("searchResult",searchResult);
		    	
				if(searchResult.length>0){
					
					var recId=searchResult[0].id;
					log.debug("recId",recId);
					
					var paymentStatusRec = record.load({
					    type: 'customrecord_payment_status',
					    id: recId                              
					});
					
				}else{
					
					var paymentStatusRec=record.create({
					       type: 'customrecord_payment_status'
					});
					
				}
				
				paymentStatusRec.setValue({fieldId:"custrecord_payment_status",value:paymentStatus});
				
				var paymentStatusRecRecId=paymentStatusRec.save({
					enableSourcing: true,
					ignoreMandatoryFields: true
				});
				
				log.debug("paymentStatusRecRecId",paymentStatusRecRecId);
	    	}
	    	
	    }catch(e){
	
			if (e instanceof nlobjError) {
	
					 log.debug("Error Code= "+e.getCode(),"Error Details= "+e.getDetails());
					
			 } else {
	
		    	log.debug("Error Message= ",e.toString());
		        
		    }
	    }
    }

    return {
        execute: execute
    };
    
});
