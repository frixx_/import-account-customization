/**
*@NApiVersion 2.x
*@NScriptType Suitelet
*/
	
define(['N/ui/serverWidget','N/runtime','N/record','N/log'], 
	function (ui, runtime, record, log){

		function onRequest(context) {
			if(context.request.method === 'GET') {

				//Create a form
				var form = ui.createForm({
					title: 'Simple Test Form'
				});

				//Create name field
				var fieldname = form.addField({
					id: 'fieldname',
					type: ui.FieldType.TEXT,
					label: 'Input your Name'
				});
				fieldname.layoutType = ui.FieldLayoutType.NORMAL;
                fieldname.breakType = ui.FieldBreakType.STARTCOL;
                fieldname.isMandatory = true;

                //Create address field
                var fieldaddress = form.addField({
                	id: 'fieldaddress',
                	type: ui.FieldType.TEXT,
                	label: 'Input your Address'
                });
                fieldaddress.isMandatory = true;

                //Create age field
                var fieldage = form.addField({
                	id: 'fieldage',
                	type: ui.FieldType.INTEGER,
                	label: 'Input your Age'
                });
                fieldage.isMandatory = true;

                //Create submit button
                form.addSubmitButton({
                	label: 'Save Record'
                });

				context.response.writePage(form);

			} else {

				var request = context.request;

				var nameValue = request.parameters.fieldname;
				log.debug('Name', ' '+ nameValue);
				var addressValue = request.parameters.fieldaddress
				log.debug('Address', ' '+ addressValue);
				var ageValue = request.parameters.fieldage;
				log.debug('Name', ' '+ ageValue);

				var objRecord = record.create({
					type: 'customrecord_submit_record'
				});

				objRecord.setValue({
					fieldId: 'name', 
					value: nameValue
				});
				objRecord.setValue({
					fieldId: 'custrecord_address', 
					value: addressValue
				});

				objRecord.setValue({
					fieldId: 'custrecord_age', 
					value: ageValue
				});
				log.debug('debug', 'second object'+ objRecord);
				objRecord.save({});

			}
	    }

return {
            onRequest: onRequest
        };


});