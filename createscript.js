const fs = require('fs');
 

fs.readFile('listfiles.txt', 'utf8', function(err, contents) {
   
    //Remove the authenticated description
    let remove = contents.replace('Using token based authentication. ','');
    remove = "\""+remove.split(".js ").join(".js\" \"");
    
    //Remove the word "Done." in the last index
    let str = remove;
    let lastIndex = str.lastIndexOf(`"Done.`);
    str = str.substring(0, lastIndex);
    
//script file to be generated and run in terminal
let scriptCommand = `#!/usr/bin/expect -f

set timeout 20

spawn ./sdfcli importfiles -paths ${str} -account TSTDRV1393316 -email renatoc@softype.com -p /opt/atlassian/pipelines/agent/build -role 1162 -url system.netsuite.com

expect "Existing files will be overwritten, do you want to continue? Type YES to continue.\r"

send -- "Yes\\r"

expect eof`;

    fs.writeFile('scriptfile', `${scriptCommand}`, function (err) {
        if (err) throw err;
        console.log('Saved!');
      });



});


